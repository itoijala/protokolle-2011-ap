#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

alpha = e_0**2 / 2 / epsilon_0 / h / c
E_R = m_e * e_0**4 / 8 / epsilon_0**2 / h**2
d = loadtxt('data/d') * 1e-9
Delta_theta = 0.1

table = r'\sisetup{table-format=1.3e+2,round-mode=figures,round-precision=4}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $h$ & \joule\second & ' + '{:.6e}'.format(h) + r' \\' + '\n'
table += r'  $c$ & \meter\per\second & ' + '{:.6e}'.format(c) + r' \\' + '\n'
table += r'  $e$ & \coulomb & ' + '{:.6e}'.format(e_0) + r' \\' + '\n'
table += r'  $m_\t{e}$ & \kilo\gram & ' + '{:.6e}'.format(m_e) + r' \\' + '\n'
table += r'  $\varepsilon_0$ & \farad\per\meter & ' + '{:.6e}'.format(epsilon_0) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $E_\t{R}$ & \joule & ' + '{:.6e}'.format(E_R) + r' \\' + '\n'
table += r'  $\alpha$ & & ' + '{:.6e}'.format(alpha) + r' \\' + '\n'
table += r'  $d$ & \angstrom & \multicolumn{1}{S[round-mode=off]}{' + str(d * 1e10) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/data.tex', 'w').write(table)

theta, I = loadtxt('data/Delta_theta', unpack=True)
theta /= 2

theta_1 = theta[0:len(theta) / 3]
theta_2 = theta[len(theta) / 3:len(theta) * 2 / 3]
theta_3 = theta[len(theta) * 2 / 3:]
I_1 = I[0:len(I) / 3]
I_2 = I[len(I) / 3:len(I) * 2 / 3]
I_3 = I[len(I) * 2 / 3:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=3.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Delta_theta-data.tex', 'w').write(table)

z = loadtxt('data/z_Nb')
M = loadtxt('data/M_Nb') * m_u
E_M = E_R * M / (M + m_e)

theta, I = loadtxt('data/Nb', unpack=True)
i_1, i_2 = loadtxt('data/i_Nb', unpack=True)
I_K = (I[i_2] + I[i_1]) / 2
theta_K = ufloat((theta[i_1] + (I_K - I[i_1]) / (I[i_2] - I[i_1]) * (theta[i_2] - theta[i_1]), Delta_theta))
E_K = h * c / 2 / d / sin(radians(theta_K))

sigma_10 = z - sqrt(E_K / E_M - alpha**2 / 4 * z**4)

theta_1 = theta[0:len(theta) / 3 + 1]
theta_2 = theta[len(theta) / 3 + 1:len(theta) * 2 / 3 + 2]
theta_3 = theta[len(theta) * 2 / 3 + 2:]
I_1 = I[0:len(I) / 3 + 1]
I_2 = I[len(I) / 3 + 1:len(I) * 2 / 3 + 2]
I_3 = I[len(I) * 2 / 3 + 2:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=3.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$z$} & \multicolumn{1}{c}{} & \multicolumn{2}{S}{' + no_point(z) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$M$} & \multicolumn{1}{s}{\atomicmassunit} & \multicolumn{2}{S}{' + str(M / m_u) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Nb-data.tex', 'w').write(table)

table = r'\sisetup{table-format=5.2,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\theta_\t{K}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_K.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{K}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_K.std_dev()) + r'} \\' + '\n'
table += r'  $E_\t{K,abs}$ & \electronvolt & \multicolumn{1}{S[round-precision=3]}{' + str(E_K.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_\t{K,abs}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_K.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $\sigma_{1,0}$ & & \multicolumn{1}{S[round-precision=2]}{' + str(sigma_10.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\sigma_{1,0}}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(sigma_10.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Nb-calc.tex', 'w').write(table)

z = loadtxt('data/z_Bi')
M = loadtxt('data/M_Bi') * m_u
E_M = E_R * M / (M + m_e)

theta, I = loadtxt('data/Bi', unpack=True)
i_II_1, i_II_2, i_III_1, i_III_2 = loadtxt('data/i_Bi', unpack=True)
I_L_II = (I[i_II_2] + I[i_II_1]) / 2
theta_L_II = ufloat((theta[i_II_1] + (I_L_II - I[i_II_1]) / (I[i_II_2] - I[i_II_1]) * (theta[i_II_2] - theta[i_II_1]), Delta_theta))
E_L_II = h * c / 2 / d / sin(radians(theta_L_II))

I_L_III = (I[i_III_2] + I[i_III_1]) / 2
theta_L_III = ufloat((theta[i_III_1] + (I_L_III - I[i_III_1]) / (I[i_III_2] - I[i_III_1]) * (theta[i_III_2] - theta[i_III_1]), Delta_theta))
E_L_III = h * c / 2 / d / sin(radians(theta_L_III))

Delta_E = E_L_II - E_L_III
s_21 = z - sqrt((4 / alpha * sqrt(Delta_E / E_M) - 5 * Delta_E / E_M) * (1 + 19 / 32 * alpha**2 * Delta_E / E_M))

theta_1 = theta[0:len(theta) / 3 + 1]
theta_2 = theta[len(theta) / 3 + 1:len(theta) * 2 / 3 + 2]
theta_3 = theta[len(theta) * 2 / 3 + 2:]
I_1 = I[0:len(I) / 3 + 1]
I_2 = I[len(I) / 3 + 1:len(I) * 2 / 3 + 2]
I_3 = I[len(I) * 2 / 3 + 2:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=2.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$z$} & \multicolumn{1}{c}{} & \multicolumn{2}{S}{' + no_point(z) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$M$} & \multicolumn{1}{s}{\atomicmassunit} & \multicolumn{2}{S}{' + str(M / m_u) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Bi-data.tex', 'w').write(table)

table = r'\sisetup{table-format=5.1,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\theta_\t{L,II}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_II.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{L,II}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_II.std_dev()) + r'} \\' + '\n'
table += r'  $\theta_\t{L,III}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_III.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{L,III}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_III.std_dev()) + r'} \\' + '\n'
table += r'  $E_\t{L,II,abs}$ & \electronvolt & \multicolumn{1}{S[round-precision=4]}{' + str(E_L_II.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_\t{L,II,abs}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_L_II.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $E_\t{L,III,abs}$ & \electronvolt & \multicolumn{1}{S[round-precision=4]}{' + str(E_L_III.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_\t{L,III,abs}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_L_III.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $s_{2,1}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(s_21.nominal_value) + r'} \\' + '\n'
table += r'  $\error{s_{2,1}}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(s_21.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Bi-calc.tex', 'w').write(table)

z = loadtxt('data/z_Hg')
M = loadtxt('data/M_Hg') * m_u
E_M = E_R * M / (M + m_e)

theta, I = loadtxt('data/Hg', unpack=True)
i_II_1, i_II_2, i_III_1, i_III_2 = loadtxt('data/i_Hg', unpack=True)
I_L_II = (I[i_II_2] + I[i_II_1]) / 2
theta_L_II = ufloat((theta[i_II_1] + (I_L_II - I[i_II_1]) / (I[i_II_2] - I[i_II_1]) * (theta[i_II_2] - theta[i_II_1]), Delta_theta))
E_L_II = h * c / 2 / d / sin(radians(theta_L_II))

I_L_III = (I[i_III_2] + I[i_III_1]) / 2
theta_L_III = ufloat((theta[i_III_1] + (I_L_III - I[i_III_1]) / (I[i_III_2] - I[i_III_1]) * (theta[i_III_2] - theta[i_III_1]), Delta_theta))
E_L_III = h * c / 2 / d / sin(radians(theta_L_III))

Delta_E = E_L_II - E_L_III
s_21 = z - sqrt((4 / alpha * sqrt(Delta_E / E_M) - 5 * Delta_E / E_M) * (1 + 19 / 32 * alpha**2 * Delta_E / E_M))

theta_1 = theta[0:len(theta) / 3 + 1]
theta_2 = theta[len(theta) / 3 + 1:len(theta) * 2 / 3 + 2]
theta_3 = theta[len(theta) * 2 / 3 + 2:]
I_1 = I[0:len(I) / 3 + 1]
I_2 = I[len(I) / 3 + 1:len(I) * 2 / 3 + 2]
I_3 = I[len(I) * 2 / 3 + 2:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=2.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$z$} & \multicolumn{1}{c}{} & \multicolumn{2}{S}{' + no_point(z) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$M$} & \multicolumn{1}{s}{\atomicmassunit} & \multicolumn{2}{S}{' + str(M / m_u) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Hg-data.tex', 'w').write(table)

table = r'\sisetup{table-format=5.1,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\theta_\t{L,II}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_II.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{L,II}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_II.std_dev()) + r'} \\' + '\n'
table += r'  $\theta_\t{L,III}$ & \degree & \multicolumn{1}{S[round-precision=3]}{' + str(theta_L_III.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{L,III}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_L_III.std_dev()) + r'} \\' + '\n'
table += r'  $E_\t{L,II,abs}$ & \electronvolt & \multicolumn{1}{S[round-precision=3]}{' + str(E_L_II.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_\t{L,II,abs}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_L_II.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $E_\t{L,III,abs}$ & \electronvolt & \multicolumn{1}{S[round-precision=4]}{' + str(E_L_III.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_\t{L,III,abs}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_L_III.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $s_{2,1}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(s_21.nominal_value) + r'} \\' + '\n'
table += r'  $\error{s_{2,1}}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(s_21.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Hg-calc.tex', 'w').write(table)

z = loadtxt('data/z_Cu')
M = loadtxt('data/M_Cu') * m_u
E_M = E_R * M / (M + m_e)

theta, I = loadtxt('data/Cu', unpack=True)
theta /= 2
i_alpha, i_beta = loadtxt('data/i_Cu', unpack=True)

theta_K_alpha = ufloat((theta[i_alpha], Delta_theta))
theta_K_beta = ufloat((theta[i_beta], Delta_theta))
E_K_alpha = h * c / 2 / d / sin(radians(theta_K_alpha))
E_K_beta = h * c / 2 / d / sin(radians(theta_K_beta))

sigma_1 = z - sqrt(E_K_alpha / E_M)
sigma_2 = z - sqrt((E_K_beta - E_K_alpha) / E_M)

a = i_alpha - 1
b = i_alpha
f = i_alpha + 1
theta_12 = abs(theta[b] + (((I[f] - I[a]) / (theta[f] - theta[a]) * (theta[b] - theta[a]) + I[a] + (I[b] - I[f]) / 2) - I[b]) / (I[f] - I[b]) * (theta[f] - theta[b]) - theta[b]) + abs(theta[a] + (((I[f] - I[a]) / (theta[f] - theta[a]) * (theta[b] - theta[a]) + I[a] + (I[b] - I[f]) / 2) - I[a]) / (I[b] - I[a]) * (theta[b] - theta[a]) - theta[b])
E_12 = h * c * (1 / 2 / d / sin(radians(theta[i_alpha] - theta_12 / 2)) - 1 / 2 / d / sin(radians(theta[i_alpha] + theta_12 / 2)))

theta_1 = theta[0:len(theta) / 3 + 1]
theta_2 = theta[len(theta) / 3 + 1:len(theta) * 2 / 3 + 1]
theta_3 = theta[len(theta) * 2 / 3 + 1:]
I_1 = I[0:len(I) / 3 + 1]
I_2 = I[len(I) / 3 + 1:len(I) * 2 / 3 + 1]
I_3 = I[len(I) * 2 / 3 + 1:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=4.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$z$} & \multicolumn{1}{c}{} & \multicolumn{2}{S}{' + no_point(z) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$M$} & \multicolumn{1}{s}{\atomicmassunit} & \multicolumn{2}{S}{' + str(M / m_u) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Cu-data.tex', 'w').write(table)

table = r'\sisetup{table-format=4.2,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\theta_{\t{K},\alpha}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_K_alpha.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_{\t{K},\alpha}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_K_alpha.std_dev()) + r'} \\' + '\n'
table += r'  $E_{\t{K},\alpha}$ & \electronvolt & \multicolumn{1}{S[round-precision=3]}{' + str(E_K_alpha.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_{\t{K},\alpha}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_K_alpha.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $\sigma_1$ & & \multicolumn{1}{S[round-precision=3]}{' + str(sigma_1.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\sigma_1}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(sigma_1.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\theta_{\t{K},\beta}$ & \degree & \multicolumn{1}{S[round-precision=3]}{' + str(theta_K_beta.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_{\t{K},\beta}}$ & \degree & \multicolumn{1}{S[round-mode=off]}{' + str(theta_K_beta.std_dev()) + r'} \\' + '\n'
table += r'  $E_{\t{K},\beta}$ & \electronvolt & \multicolumn{1}{S[round-precision=3]}{' + str(E_K_beta.nominal_value / e_0) + r'} \\' + '\n'
table += r'  $\error{E_{\t{K},\beta}}$ & \electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_K_beta.std_dev() / e_0) + r'} \\' + '\n'
table += r'  $\sigma_2$ & & \multicolumn{1}{S[round-precision=3]}{' + str(sigma_2.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\sigma_2}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(sigma_2.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\theta_\frac{1}{2}$ & \degree & \multicolumn{1}{S[round-precision=2]}{' + str(theta_12) + r'} \\' + '\n'
table += r'  $E_\frac{1}{2}$ & \electronvolt & \multicolumn{1}{S[round-precision=2]}{' + str(E_12 / e_0) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Cu-calc.tex', 'w').write(table)

theta, I = loadtxt('data/Cu2', unpack=True)

A, B = ulinregress(theta[:11], I[:11])
theta_gr = - B / A
E_gr = h * c / 2 / d / sin(radians(theta_gr))

theta_1 = theta[0:len(theta) / 3]
theta_2 = theta[len(theta) / 3:len(theta) * 2 / 3]
theta_3 = theta[len(theta) * 2 / 3:]
I_1 = I[0:len(I) / 3]
I_2 = I[len(I) / 3:len(I) * 2 / 3]
I_3 = I[len(I) * 2 / 3:]

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S[table-format=3.0]'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += '  '
for i in range(3):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$\theta / \si{\degree}$} & \multicolumn{1}{c'
  if i < 2:
    table += '|'
  table += r'}{$I / \si{\per\second}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(theta_1)):
  table += '  ' + str(theta_1[i]) + ' & ' + no_point(I_1[i]) + ' &'
  if i < len(theta_2):
    table += str(theta_2[i]) + ' & ' + no_point(I_2[i]) + ' &'
  else:
    table += ' & '
  if i < len(theta_3):
    table += str(theta_3[i]) + ' & ' + no_point(I_3[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Cu2-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.1,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\degree\per\second & \multicolumn{1}{S[round-precision=2]}{' + str(A.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\degree\per\second & \multicolumn{1}{S[round-precision=1]}{' + str(A.std_dev()) + r'} \\' + '\n'
table += r'  $B$ & \per\second & \multicolumn{1}{S[round-precision=1]}{' + str(B.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \per\second & \multicolumn{1}{S[round-precision=1]}{' + str(B.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\theta_\t{gr}$ & \degree & \multicolumn{1}{S[round-precision=1]}{' + str(theta_gr.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\theta_\t{gr}}$ & \degree & \multicolumn{1}{S[round-precision=2]}{' + str(theta_gr.std_dev()) + r'} \\' + '\n'
table += r'  $E_\t{gr}$ & \kilo\electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_gr.nominal_value / e_0 * 1e-3) + r'} \\' + '\n'
table += r'  $\error{E_\t{gr}}$ & \kilo\electronvolt & \multicolumn{1}{S[round-precision=1]}{' + str(E_gr.std_dev() / e_0 * 1e-3) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Cu2-calc.tex', 'w').write(table)
