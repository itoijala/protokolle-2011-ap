\input{header.tex}

\renewcommand{\ExperimentNumber}{602}

\begin{document}
  \maketitlepage{Röntgen-Emmisions- und Absorbtionsspektren}{29.11.2011}{06.12.2011}
  \section{Ziel}
    Ziel des Versuchs ist die Bestimmung der Abschirmungszahlen $s$ und $\sigma$ von Elementen mit verschiedener Ordnungszahl und die Messung des Auflösungsvermögens des Versuchsaufbaus.
  \section{Theorie}
    Der Aufbau der inneren Elektronenhüllen lässt sich nur mit Röntgenstrahlung, die mit höherer Energie auf die UV-Strahlung folgt, untersuchen.
    Das Energieintervall, dass die Röntgenstrahlung beinhaltet, erstreckt sich von \SI{0.01}{\kilo\electronvolt} bis \SI{100}{\kilo\electronvolt}.
    Wegen der hohen Energie lassen sich die meisten Phänomene nur mit der Quantenmechanik beschreiben; in einigen Fällen verhält sich die Strahlung aber auch wellenartig.
    \subsection{Entstehung von Röntgenstrahlung}
      \label{entstehung_röntgen}
      Wenn Elektronen mit hinreichend großer kinetischer Energie in Materie eindringen, so können sie mit geringer Wahrscheinlichkeit bis zu den innersten Elektronenhüllen eintreten und durch das Coulombpotential des Atomkerns abgelenkt werden.
      Dadurch beschreiben die Elektronen eine Kreisbahn, werden also beschleunigt, und emittieren eine so genannte Bremsstrahlung, die vornehmlich senkrecht zum Geschwindigkeitsvektor ausgestrahlt wird.
      Die Wahrscheinlichkeit für das Auftreten dieser Strahlung ist proportional zum Quadrat der Kernladungszahl $z$ des Materials.
      Des Weiteren reicht das kontinuierliche Energiespektrum der Strahlung von Null bis maximal zum Betrag der kinetischen Energie der Elektronen.

      Wenn die kinetische Energie der Elektronen mindestens so groß wie die Bindungsenergie der Elektronenschalen des Atoms ist, können diese die Atome ionisieren.
      Da der ionisierte Zustand der Atome sehr instabil ist, rutschen innerhalb von $\SI{e-8}{\second}$ die Elektronen sequentiell aus den äußeren Schalen in die entstandenen Elektronenlöcher, also Stufen niedrigerer Energie.
      Die freiwerdende Energie wird in Form von Lichtquanten abgegeben, deren Frequenzbereich mit der Bindungsenergie zusammenhängt.
      Meistens wird Infrarotstrahlung ausgesendet, in seltenen Fällen Röntgenstrahlung.
      Das Spektrum ist im Gegensatz zur Bremsstrahlung diskret und materialabhängig; man bezeichnet die Röntgenstrahlung als charakteristische Röntgenstrahlung.

      In der Praxis wird die Röntgenstrahlung in einer Röntgenröhre erzeugt.
      Dies ist eine evakuierte Glasröhre mit einem Glühdraht als Kathode, der durch den Thomsonschen Effekt freie Elektronen erzeugt, und einer schräg stehenden Platte als Anode, auf die die Elektronen mit hoher Geschwindigkeit aufprallen.
    \subsection{Das Emissionsspektrum der Röntgenstrahlung}
      Ein Spektrum beschreibt die Abhängigkeit der Intensität, also der Zahl der pro Fläche und Zeit einfallenden Photonen, von der Energie der Röntgenstrahlung.
      Das emittierte Spektrum setzt sich aus der in \ref{entstehung_röntgen} beschriebenen Brems- und der charakteristischen Strahlung zusammen.
      Die Bestimmung der diskreten charakteristischen Linien im Spektrum führt auf ein Energieeigenwertproblem zurück, das für elektronenreiche Atome schwer zu lösen ist.
      Deswegen wählt man als erste Näherung den Ansatz der Einelektronennäherung, das heißt das herausgelöste Elektron befindet sich in einem Coulombpotential, in das die Kernladungszahl, also die Anzahl der Protonen, und der Einfluss der verbleibenden Elektronen einfließen.
      Dieser Ansatz lässt sich jedoch nur für Elemente niedriger Ordnungszahl verwenden.

      Die Gestalt der charakteristischen Linien ist unabhängig vom Bindungs- und Aggregatzustand des Materials und stets durch Linien charakterisiert, die sich für größere Ordnungszahl monoton verschieben.
      Für mittlere Kernzahlen muss man für eine gute Näherung die relativistische Masse der Elektronen und den Drehimpuls des aktiven Hüllenelektrons berücksichtigen.

      Mit einer Störungsrechnung erhält man die Sommerfeldsche Feinstrukturformel
      \begin{eqn}
        E_{n,j} = - E_\t{R} \left( z_{\t{eff},1}^2 \frac{1}{n^2} + \alpha^2 z_{\t{eff},2}^4 \frac{1}{n^3} \left( \frac{1}{j + \frac{1}{2}} - \frac{3}{4 n} \right) \right) ,
        \eqlabel{eqn:sommerfeld}
      \end{eqn}%
      wobei $n$ die Hauptquantenzahl und $j$ der Gesamtdrehimpuls, also die Summe aus dem Spin $\pm \frac{1}{2}$ und dem Bahndrehimpuls $\ell$ ist.
      $E_\t{R}$ ist die Rydberg-Energie und $\alpha$ die Sommerfeldsche Feinstrukturkonstante, wobei gilt
      \begin{eqns}
        E_\t{R} & = & \frac{m_\t{e} e^4}{8 \varepsilon_0^2 h^2} \\
        \alpha & = & \frac{e^2}{2 \varepsilon_0 h c} .
      \end{eqns}%
      $m_\t{e}$ ist die Ruhemasse des Elektrons, $e$ die Elementarladung, $\varepsilon_0$ die elektrische Feldkonstante, $h$ das Plancksche Wirkungsquantum und $c$ die Lichtgeschwindigkeit im Vakuum.
      Bei Atomen mit Kernen endlicher Masse muss $E_\t{R}$ durch
      \begin{eqn}
        E_M = E_\t{R} \frac{M}{M + m_\t{e}}
      \end{eqn}%
      ersetzt werden, wobei $M$ die Masse des Kerns ist.

      Die zwei effektiven Kernladungszahlen $z_\t{eff,1}$ und $z_\t{eff,2}$ können folgendermaßen ausgedrückt werden
      \begin{eqns}
        z_\t{eff,1} & = & z - \sigma_{n,\ell} \\
        z_\t{eff,2} & = & z - s_{n,\ell}
      \end{eqns}%
      $\sigma_{n,\ell}$ nennt man die Konstante der vollständigen Abschirmung, die im Wesentlichen von den Hüllenelektronen unter aber auch über der $n$-ten Schale abhängt. 
      Die Konstante der inneren Abschirmung $s_{n,\ell}$ hängt nur von den innerhalb der n-ten Schale auftretenden Hüllenelektronen ab.
      Beide Konstanten sind außerdem stark vom Bahndrehimpuls $\ell$ abhängig.
      Für $n > 2$ gibt es, wie oben angedeutet, pro Quantenzahl mehrere Bahndrehimpulse $\ell$ und mehrere Spins, daher kommt es ab der L-Schale zu einer Feinstrukturaufspaltung in Unterniveaus.
      Elektronen können nur in bestimmte Energieniveaus springen; zum einem muss die Änderung des Bahndrehimpulses $\Delta \ell = 1$ sein, zum anderen ist die Wahrscheinlichkeit sehr gering, dass die Elektronen mehr als drei Schalen überspringen.
      Für Atome mit $z > 69$ müssen Ordnungen der Art $\alpha^4 z^6$ in Betracht gezogen werden.
      
      Die Ermittlung der Abschirmungszahlen durch das Emissionsspektrum ist schwierig; am geschicktesten geschieht dies durch die Analyse von Absorbtionsspektren.
    \subsection{Absorbtionsspektren}
      Tritt Röntgenstrahlung durch Materie, so wird die Strahlung in Abhängigkeit von ihrer Energie und der Art der durchdrungenen Materie in ihrer Intensität geschwächt.
      Der Grund dafür ist Streuung und vor allem ein Absorbtionsvorgang, der innerer Photo-Effekt genannt wird.
      Trifft ein Röntgenquant auf ein inneres Elektron unter der Vorraussetzung, dass die Energie des Photons $E = h \nu$ größer als die Bindungsenergie des Hüllenelektrons ist, so wird dieses aus dem Atomverbund herausgeschlagen und fliegt mit einer kinetischen Energie, die der Differenz aus Energie des Quants und der Bindungsenergie entspricht, entweder in die oberen Hüllen, die bei Übergänge nur sichtbares Licht emitieren, oder in den kontinuierlichen energetischen Bereich über der Ionisierungsenergie.
      Die übrigen Hüllenelektronen füllen sequentiell die auftretenden Löcher und das Atom emitiert so eine im Röntgenbereich liegende Fluoreszensstrahlung.
      Im Energiebereich $E < \SI{100}{\kilo\electronvolt}$ des Röntgenlichts ergibt sich für die Wahrscheinlichkeit des Vorgangs, die durch den Absorbtionsquotienten $\mu$ beschrieben wird nach:
      \begin{eqn}
        \mu \sim z^5 E^{-3,5} 
        \eqlabel{eqn:absorbtionskoeffizient}
      \end{eqn}%
      eine starke Abhängigkeit von der Kernladungszahl und eine umgekehrte Proportionalität zur Energie, die sich unstetig nach Herausschlagen eines Elektrons verhält.
      Als Näherung nimmt man an, dass die optischen Elektronenhüllen energetisch sehr nah beisammen liegen im Verhältnis zu den \enquote{Röntgenhüllen}; deshalb folgert man, dass das Atom immer ionisiert wird.
      Für die K-Schale ergibt sich nach \eqref{eqn:sommerfeld} unter Berücksichtigung, dass die innere Abschirmungszahl $s_{1,0}$ wegen fehlenden inneren Hüllen verschwindet
      \begin{eqn}
        E_\t{K,abs} = E_\t{R} \left( \left( z - \sigma_{1,0} \right)^2 + \frac{\alpha^2}{4} z^4 \right) .
      \end{eqn}%
      Für die Ermittlung der Abschirmungszahl $\sigma_{1,0}$ erweist sich die Umformung
      \begin{eqn}
        \sigma_{1,0} = z - \sqrt{\frac{E_\t{K,abs}}{E_\t{R}} - \frac{\alpha^2}{4} z^4}
        \eqlabel{eqn:sigma_10}
      \end{eqn}%
      als nützlich.

      Zur Ermittlung von $s_{2,1}$ betrachtet man die Energiedifferenz:
      \begin{eqn}
        \Delta E  =  E_\t{L,abs,II} - E_\t{L,abs,III}
      \end{eqn}%
      zwischen zweitem und drittem Energieniveau der L-Schale. 
      Da hier auch schwere Atome untersucht werden, müssen für höhere Genauigkeit Terme höherer Ordnung berücksichtigt werden.
      Nach einer kurzen Rechnung ergibt sich
      \begin{eqn}
        \left( z - s_{2,1} \right)^2 = \left( \frac{4}{\alpha} \sqrt{\frac{\Delta E}{E_\t{R}}} - 5 \frac{\Delta E}{E_\t{R}} \right) \left( 1 + \frac{19}{32} \alpha^2 \frac{\Delta E}{E_\t{R}} \right) \eqlabel{eqn:heavy_atoms}
      \end{eqn}%
      woraus sich die hier relevante Umformung
      \begin{eqn}
        s_{2,1} = z - \sqrt{\left( \frac{4}{\alpha} \sqrt{\frac{\Delta E}{E_\t{R}}} - 5 \frac{\Delta E}{E_\t{R}} \right) \left( 1 + \frac{19}{32} \alpha^2 \frac{\Delta E}{E_\t{R}} \right)}
        \eqlabel{eqn:s_21}
      \end{eqn}%
      ergibt.

      Für die Abschirmungszahlen $\sigma_1$ und $\sigma_2$ von Kupfer muss man den Drehimpuls $\ell$ vernachlässigen und erhält als Näherung
      \begin{eqns}
        E_{\t{K},\alpha} & = & E_\t{R} \left( z - \sigma_1 \right)^2 \\
        E_{\t{K} , \beta} & = & E_{\t{K} , \alpha} - \frac{1}{4} E_\t{R} \left( z - \sigma_2 \right)^2 
      \end{eqns}%
      woraus sich nach Umstellen
      \begin{eqns}
        \sigma_1 & = & z - \sqrt{\frac{E_{\t{K},\alpha}}{E_\t{R}}} \eqlabel{eqn:sigma_1} \\
        \sigma_2 & = & z - 2 \sqrt{\frac{E_{\t{K} , \beta} - E_{\t{K} , \alpha}}{E_\t{R}}} \eqlabel{eqn:sigma_2}
      \end{eqns}%
      ergibt.

      Streuung tritt hier nur für Atome mit niedriger Ladungszahl und schwacher Bindung auf.
      Hierbei kommt es durch den Comptoneffekt zu Stößen der Quanten mit freien Elektronen und zur Auffächerung des Röntgenstrahls.
    \subsection{Theoretische Grundlagen zum Messaufbau}
      Zur Ermittlung der Absorptionsenergien durchstrahlt man das gewählte Material mit unifrequentem Röntgenlicht.
      Das gemessene Spektrum offenbart die Verifizierung von \eqref{eqn:absorbtionskoeffizient} und Unstetigkeiten in der Monotonie immer dann, wenn die Röntgenenergie gleich der Bindungsenergie der verschiedenen Energieniveau und -unterniveaus ist und ein Elektron herausgeschlagen wird.
      Die Stellen entsprechen den Absorbtionsenergien und werden Absorbtionskanten genannt.

      Für die Intensitätsmessung wird wegen der ionisierenden Wirkung ein Geiger-Müller-Zählrohr mit einem elektronischen Zähler und einem Impulsratenmeter verwendet.
      Für die Messung der Energie werden, wie oben legitimiert, die Welleneigenschaften des Röntgenlichts ausgenutzt.
      Man betrachtet Interferenzerscheinung an einem Kristall, also an einem Ensemble von Atomen, die räumlich periodisch auftreten, da die einfallenden Quanten elastisch in alle Richtungen gestreut werden.
      Für bestimmte Winkel mit denen das Röntgenlicht einfällt, ergibt sich ein Gangunterschied, bei dem sich konstruktive Interferenz einstellt.
      Diese speziellen Winkel werden durch die Braggsche Reflexionsbedingung beschrieben, die abhängig von der Wellenlänge $\lambda$ des einfallenden Lichts ist. Ersetzt man diese durch die Beziehung zwischen der Energie des Lichts und ihrer Wellenlänge so ergibt sich
      \begin{eqn}
        E = \frac{h c n}{2 d \sin \theta},
      \end{eqn}%
      also eine Beziehung zwischen Energie und Winkel des einfallenden Röntgenlichtes.
      Dabei ist $d$ der Netzebenenabstand und $n$ die Interferenzordnung.
      Hier kommt nur die erste Interferenzordnung vor, also folgt
      \begin{eqn}
        E = \frac{h c}{2 d \sin \theta} .
        \eqlabel{eqn:bragg}
      \end{eqn}%
  \section{Versuchsaufbau}
    Um die Röntgenspektren aufzunehmen, wird zunächst in einer in \ref{entstehung_röntgen} beschriebenen Röntgenröhre, an der eine Spannung von $\SI{35}{\kilo\volt}$ anliegt und ein Strom von ungefähr \SI{1}{\milli\ampere} fließt, hoch energetische Röntgenstrahlung erzeugt.
    Dieses tritt, um divergente Strahlen zu blocken, durch eine Winkelblende und trifft auf einen rotierbaren LiF-Kristall, der an einem Elektromotor angeschlossen ist.
    Nachdem sie am Kristall reflektiert wurde, wird sie von einem Geiger-Müller-Zählrohr, das um den Winkel $\theta$ zum Kristall geneigt ist, registriert.
    Vor das Rohr kann ein Material geschraubt werden, dessen Absorbtionsspektrum ermittelt werden soll.
    Der entstehende elektrische Impuls wird verstärkt und durch einen Impulsratemeter zu einem Computer geschickt.
    Der Computer übernimmt im Übrigen die Steuerung des Kristallwinkels und des Zählrohrs, sowie die Steuerung der Röntgenröhre.
  \section{Versuchsdurchführung}
    Zunächst wird der Unterschied $\Delta \theta$ ermittelt, der den Abstand von der praktischen von der theoretisch vorhergesagten maximalen Intensität beim Winkel $\theta$ zum Kristall angibt.
    Dazu lässt man den Kristall fix bei $\SI{14}{\degree}$ und lässt den Computer ein Spektrum im Bereich $\SI{27}{\degree}$ bis $\SI{29}{\degree}$ aufzeichnen.
    Daraufhin lässt man das Emissionsspektrum aufzeichnen, wobei nichts vor das Zählrohr geschraubt wird.
    Dies geschieht im Bereich von $\SIrange{7}{30}{\degree}$.
    Man stellt nun den Bereich fest, bei denen die charakteristischen Linien von Kupfer auftreten, und lässt diesen bei der Ermittlung des Absorbtionsspektrums weg.
    Für die restlichen Teilaufgaben werden die Absorbtionsspektren von Niob (\ce{Nb}) mit der Kernladungszahl $z = 41$, für Bismut (\ce{Bi}) mit $z = 79$ und für Quecksilber (\ce{Hg}) mit $z = 80$ ermittelt.
  \section{Messwerte}
    Die gemessenen Werte und verwendete Konstanten stehen in den Tabellen \ref{tab:data} bis \ref{tab:Cu2-data}.
    \begin{table}
      \input{table/data.tex}
      \caption{Werte von Konstanten und dem Netzebenenabstand $d$}
      \label{tab:data}
    \end{table}%
    \begin{table}
      \input{table/Delta_theta-data.tex}
      \caption{Messwerte zur Bestimmung von $\Delta \theta$}
      \label{tab:Delta_theta-data}
    \end{table}%
    \begin{table}
      \input{table/Nb-data.tex}
      \caption{Messwerte des Absorptionsspektrums von Niob}
      \label{tab:Nb-data}
    \end{table}%
    \begin{table}
      \input{table/Bi-data.tex}
      \caption{Messwerte des Absorptionsspektrums von Bismut}
      \label{tab:Bi-data}
    \end{table}%
    \begin{table}
      \input{table/Hg-data.tex}
      \caption{Messwerte des Absorptionsspektrums von Quecksilber}
      \label{tab:Hg-data}
    \end{table}%
    \begin{table}
      \input{table/Cu-data.tex}
      \caption{Messwerte des Emissionsspektrums von Kupfer}
      \label{tab:Cu-data}
    \end{table}%
    \begin{table}
      \input{table/Cu2-data.tex}
      \caption{Messwerte des Emissionsspektrums von Kupfer}
      \label{tab:Cu2-data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    \input{linregress.tex}%

    Wenn man Abbildung \ref{fig:graph-Delta_theta} betrachtet, so erscheint bei \SI{14.5}{\degree} ein Maximum im Spektrum.
    Dies führt zu einem Differenzwinkel von $\Delta \theta = \SI{0.5}{\degree}$.
    Beim Ablesen von Winkeln wird jeweils ein Fehler von \SI{0.1}{\degree} angenommen\input{uncertainties.tex}.
    Dies bedeutet, dass Kristall und Zählrohr relativ gut synchronisiert sind.
    \begin{figure}
      \includesgraphics{graph/Delta_theta.pdf}
      \caption{Plot der Messwerte zur Bestimmung von $\Delta \theta$}
      \label{fig:graph-Delta_theta}
    \end{figure}%

    Für die Bestimmung von $\sigma_{1,0}$ liest man in Abbildung \ref{fig:graph-Nb} bei $\theta \approx \SI{9.75}{\degree}$ eine Kante ab.
    Die mit den Formeln \eqref{eqn:bragg} und \eqref{eqn:sigma_10} berechneten Werte stehen in Tabelle \ref{tab:Nb-calc}.
    Für $\sigma_{1,0}$ ergibt sich ein Wert von
    \begin{eqn}
      \sigma_{1,0} = \num{4.9 +- 0.2} .
    \end{eqn}
    \begin{figure}
      \includesgraphics{graph/Nb.pdf}
      \caption{Plot der Messwerte für Niob}
      \label{fig:graph-Nb}
    \end{figure}%
    \begin{table}
      \input{table/Nb-calc.tex}
      \caption{Berechnete Werte für Niob}
      \label{tab:Nb-calc}
    \end{table}%

    Für die Bestimmung von $s_{2,1}$ für Bismut liest man in Abbildung \ref{fig:graph-Bi} bei $\theta \approx \SI{13.6}{\degree}$ und $\theta \approx \SI{15.8}{\degree}$ Kanten ab.
    Die mit der Formel \eqref{eqn:s_21} berechneten Werte stehen in Tabelle \ref{tab:Bi-calc}.
    Für $s_{2,1}$ ergibt sich ein Wert von
    \begin{eqn}
      s_{2,1} = \num{8 +- 1} .
    \end{eqn}%
    \begin{figure}
      \includesgraphics{graph/Bi.pdf}
      \caption{Plot der Messwerte für Bismut}
      \label{fig:graph-Bi}
    \end{figure}%
    \begin{table}
      \input{table/Bi-calc.tex}
      \caption{Berechnete Werte für Bismut}
      \label{tab:Bi-calc}
    \end{table}%

    Für die Bestimmung von $s_{2,1}$ für Quecksilber liest man in Abbildung \ref{fig:graph-Hg} bei $\theta \approx \SI{13.2}{\degree}$ und $\theta \approx \SI{14.9}{\degree}$ Kanten ab.
    Die mit der Formel \eqref{eqn:s_21} berechneten Werte stehen in Tabelle \ref{tab:Hg-calc}.
    Für $s_{2,1}$ ergibt sich ein Wert von
    \begin{eqn}
      s_{2,1} = \num{8 +- 1} .
    \end{eqn}%
    \begin{figure}
      \includesgraphics{graph/Hg.pdf}
      \caption{Plot der Messwerte für Quecksilber}
      \label{fig:graph-Hg}
    \end{figure}%
    \begin{table}
      \input{table/Hg-calc.tex}
      \caption{Berechnete Werte für Quecksilber}
      \label{tab:Hg-calc}
    \end{table}%

    Für die Bestimmung von $\sigma_1$ und $\sigma_2$ für Kupfer liest man in Abbildung \ref{fig:graph-Cu} bei $\theta \approx \SI{23.0}{\degree}$ und $\theta \approx \SI{20.5}{\degree}$ die charakteristischen Maxima im Emissionsspektrum ab.
    Die mit den Formeln \eqref{eqn:sigma_1} und \eqref{eqn:sigma_2} berechneten Werte stehen in Tabelle \ref{tab:Cu-calc}.
    Für $\sigma_1$ und $\sigma_2$ ergeben sich die Werte
    \begin{eqns}
      \sigma_1 & = & \num{4.91 +- 0.05} \\
      \sigma_2 & = & \num{20.8 +- 0.2} .
    \end{eqns}%
    Für die Energieauflösung der verwendeten Apparatur bei $\theta \approx \SI{23}{\degree}$ ergibt sich
    \begin{eqn}
      E_\frac{1}{2} = \SI{160}{\electronvolt} .
    \end{eqn}%
    \begin{figure}
      \includesgraphics{graph/Cu.pdf}
      \caption{Plot der Messwerte für Kupfer}
      \label{fig:graph-Cu}
    \end{figure}%
    \begin{table}
      \input{table/Cu-calc.tex}
      \caption{Berechnete Werte für Kupfer}
      \label{tab:Cu-calc}
    \end{table}%

    Aus dem linearen linken Ende des Emissionspektrums (Abbildung \ref{fig:graph-Cu2}) wird die maximale Energie der Röntgenquanten $E_\t{gr}$ durch eine lineare Regression berechnet.
    Die Nullstelle der Geraden wird mittels Formel \eqref{eqn:bragg} in die Grenzenergie umgerechnet.
    Es ergibt sich ein Wert von
    \begin{eqn}
      E_\t{gr} = \SI{300 +- 800}{\kilo\electronvolt} .
    \end{eqn}%
    Dieser Wert stimmt nicht mit dem erwarteten Wert von $E = e U = \SI{35}{\kilo\electronvolt}$ überein, liegt allerdings innerhalb des großen Fehlerbereichs.
    Der große Fehler kommt dadurch zustande, dass die Werte, die zur Regression verwendet wurden, starke Schwankungen aufweisen.
    Der Abstand, der durch Extrapolation zwischen Messwerten und der Nullstelle überbrückt werden muss,ist sehr groß im Vergleich zu der Breite des Bereichs, in dem die Messwerte liegen.
    \begin{figure}
      \includesgraphics{graph/Cu2.pdf}
      \caption{Plot der Messwerte für Kupfer}
      \label{fig:graph-Cu2}
    \end{figure}%
    \begin{table}
      \input{table/Cu2-calc.tex}
      \caption{Berechneter Wert für $E_\t{gr}$}
      \label{tab:Cu2-calc}
    \end{table}%

    Die Bestimmung der Konstanten der Abschirmung ist aus mehreren Gründen ungenau.
    Durch die geringen Messauflösungen in $\theta$- und $I$-Richtung, also der auf $\SI{0.1}{\degree}$ begrenzten Winkelauflösung und der auf $\SI{10}{\second}$ gesetzten Messzeit pro Messwert, ist es schwierig die Kanten richtig zu erkennen und abzulesen.
    Die Proben waren teilweise beschädigt, was auch zu Fehlern führen kann.
    Die Messmethode ist zur Bestimmung von $E_\t{gr}$ wegen des sehr hohen Fehlers ungeeignet.
  \makebibliography
\end{document}
