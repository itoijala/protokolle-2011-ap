\input{header.tex}
\newcommand{\setN}{\mathbb{N}}

\renewcommand{\ExperimentNumber}{202}

\begin{document}
  \maketitlepage{Bestimmung des Adiabatenexponeneten für verschiedene Gase mit Ultraschall}{18.10.2011}{25.10.2011}
  \section{Ziel}
    Das Ziel des Versuchs ist die Bestimmung des Adiabatenexponenten $\kappa$ für Argon (\ce{Ar}), Luft (Stickstoff, \ce{N2}) und Kohlenstoffdioxid (\ce{CO2}).
  \section{Theorie}
    Ideale Gase sind eine gute Näherung für in der Natur auftretende Gase bei niedrigem Druck und hinreichend großen Temperaturen.
    Ein ideales Gas besteht aus Molekülen, deren Volumen vernachlässigbar ist und die nur über elastische Stöße miteinander und mit der Gefäßwand  wechselwirken.
    
    Man definiert die Molwärme
    \begin{eqn}
      C = \frac{\dif{q}}{\dif{T}} ,
    \end{eqn}%
    als Quotient aus der zugeführten Wärmemenge $\dif{q}$ und der resultierenden Temperaturänderung $\dif{T}$ und unterscheidet zwischen der isobaren und der isochoren Wärmezufuhr.
    Bei isobarer Wärmezufuhr wird das Gas erwärmt und das Volumen vergrößert.
    Hier heißt die Molwärme $C_\t{p}$.
    Eine isochore Wärmezufuhr (Molwärme $C_\t{V}$) erhöht nur die Temperatur des Gases.

    Da $C_\t{p}$ und $C_\t{V}$ einzeln bei Gasen nur mit hohem Aufwand genau zu messen sind, bestimmt man experimentell den Adiabatenexponeneten
    \begin{eqn}
      \kappa = \frac{C_\t{p}}{C_\t{V}} .
    \end{eqn}%
    Nach dem ersten Hauptsatz der Thermodynamik
    \begin{eqn}
      \dif{U} = \dif{A} + \dif{q} ,
    \end{eqn}%
    wonach eine Änderung der inneren Energie $U$ nur durch Arbeit $\dif{A}$ oder Wärme $\dif{q}$ erreicht werden kann, ergibt sich bei einer Volumenausdehnung, bei der Arbeit vom System verrichtet wird,
    \begin{eqn}
      \dif{U} = - p \dif{V} + \dif{q} ,
    \end{eqn}%
    wobei $V$ das Volumen un $p$ der Druck des Gases sind.
    Für einen isochoren Prozess, also $\dif{V} = 0$, gilt
    \begin{eqn}
      \dif{U} = C_\t{V} \dif{T} \eqlabel{eqn:cv}.
    \end{eqn}%
    Aus der Definition von $C_\t{p}$ folgt
    \begin{eqn}
      \dif{U} = - p \dif{V} + C_\t{p} \dif{T} . \eqlabel{eqn:cp}
    \end{eqn}%
    Aus \eqref{eqn:cv} und \eqref{eqn:cp} folgt
    \begin{eqn}
      C_\t{p} - C_\t{V} = p \frac{\dif{V}}{\dif{T}} .
    \end{eqn}%
    Mit der allgemeinen Gasgleichung
    \begin{eqn}
      p V = R T
      \eqlabel{eqn:gasgleichung}
    \end{eqn}%
    und der allgemeinen Gaskonstante $R$ folgt
    \begin{eqn}
      C_\t{p} - C_\t{V} = R ,
    \end{eqn}%
    also
    \begin{eqn}
      \kappa = \frac{C_\t{V} + R}{C_\t{V}} .
    \end{eqn}%

    Um $C_\t{V}$ zu berechnen, muss die innere Energie $U$ eines idealen Gases bekannt sein.
    Diese besteht beim idealen Gas nur aus kinetischer Energie.
    Für ein Mol des Gases gilt
    \begin{eqn}
      U = \frac{1}{2} m \sum_{i = 1}^{N_\t{A}} v_i^2 = \frac{N_\t{A}}{2} m \mean{v^2} ,
      \eqlabel{eqn:innere-energie}
    \end{eqn}%
    wobei $N_\t{A}$ die Avogadro-Konstante ist.
    Nach einer kurzen Rechnung ergibt sich für den Druck im Behälter
    \begin{eqn}
      p = \frac{N_\t{A}}{3} \frac{m}{V} \mean{v^2} .
    \end{eqn}%
    Setzt man dies in \eqref{eqn:gasgleichung} ein, erhält man
    \begin{eqn}
      \frac{N_\t{A}}{3} m \mean{v^2} = R T .
    \end{eqn}%
    Mit \eqref{eqn:innere-energie} folgt
    \begin{eqn}
      U = \frac{3}{2} R T ,
      \eqlabel{eqn:innere-energie-2}
    \end{eqn}%
    woraus für $C_\t{V}$ folgt:
    \begin{eqn}
      C_\t{V} = \frac{3}{2} R .
    \end{eqn}%

    Aus \eqref{eqn:innere-energie-2} folgt für die mittlere kinetische Energie eines Moleküls
    \begin{eqn}
      \mean{E_\t{kin}} = \frac{3}{2} k_\t{B} T
    \end{eqn}%
    mit der Boltzmann-Konstante
    \begin{eqn}
      k_\t{B} = \frac{R}{N_\t{A}} .
    \end{eqn}%
    Aus Symmetriegründen ergibt sich das Äquipartitionsgesetz, das besagt, dass die kinetische Energie über alle Raumrichtungen gleich verteilt ist.
    Daraus folgt für die mittlere Energie eines Teilchens mit $f$ Freiheitsgraden
    \begin{eqn}
      \mean{E} = \frac{f}{2} k_\t{B} T,
    \end{eqn}%
    beziehungsweise für die innere Energie eines Mols:
    \begin{eqn}
      U = \frac{f}{2} R T .
    \end{eqn}%
    Es folgt
    \begin{eqns}
      C_\t{V} &=& \frac{f}{2} R \eqlabel{eqn:atome-cv} \\
      \kappa &=& \frac{f + 2}{f} . \eqlabel{eqn:atome-kappa}
    \end{eqns}%

    Je nach Molekülstruktur und Temperatur divergieren die Werte für $C_\t{V}$ und $\kappa$.
    \paragraph{Einatomige Moleküle}
      Wie oben erwähnt, gelten für Atome \eqref{eqn:atome-cv} und \eqref{eqn:atome-kappa} mit $f = 3$, weil sie als punktförmig angenommen werden können und nur die drei translatorischen Freiheitsgrade besitzen.
      Also gilt
      \begin{eqns}
        C_\t{V} & = & \frac{3}{2} R \\
        \kappa & = & \frac{5}{3} . \eqlabel{eqn:kappa1}
      \end{eqns}%
    \paragraph{Starre, zweiatomige Moleküle}
      Diese sind als linienförmig anzunehmen und haben drei Translations- und zwei Rotationsfreiheitsgrade.
      Der dritte Rotationsfreiheitsgrad verschwindet, da die Rotation um die Molekülachse nicht angeregt werden kann.
      Damit gilt
      \begin{eqns}
        C_\t{V} & = & \frac{5}{2} R \\
        \kappa & = & \frac{7}{5} \eqlabel{eqn:kappa2gestr} .
      \end{eqns}%
    \paragraph{Dreiatomige, starre Moleküle}
      Falls die Atome auf einer Geraden liegen, also gestreckt sind, verändert sich am vorigen Paragraphen nichts.
      Bei einem gewinkelten Molekül kommt der dritte Freiheitsgrad der Rotation hinzu.
      Dann gilt
      \begin{eqns}
        C_\t{V} & = & 3 R \\
        \kappa & = & \frac{4}{3} .
      \end{eqns}%
    \paragraph{Mehratomige Moleküle}
      Jetzt treten Oszillationen auf.
      Hier besteht die mittlere Energie jeweils zur Hälfte aus kinetischer und potentieller Energie.
      Insgesamt gilt pro Molekül und Oszillationsfreiheitsgrad
      \begin{eqn}
        \mean{E_\t{osc}} = k_\t{B} T . \eqlabel{eqn:e-osc-nq}
      \end{eqn}%
      Um die Anzahl der Freiheitsgrade zu erhalten, überlegt man sich, dass jedes Atom im Molekül drei Koordinaten braucht, um im Raum beschrieben zu werden.
      Es ergeben sich $3 n$ Translationsfreiheitsgrade, von den man noch sechs abziehen muss, da nur der Schwerpunkt die Translationen und Rotationen für das Molekül als ganzes ausführt.
      Für gestreckte Moleküle ergeben sich
      \begin{eqns}
        C_\t{V} & = & \left( 3 n - \frac{5}{2} \right) R \\
        \kappa & = & \frac{3 \left( 2 n - 1 \right)}{6 n - 5} .
      \end{eqns}%
      Für nicht-gestrekte Moleküle gilt
      \begin{eqns}
        C_\t{V} & = & 3 \left( n - 1 \right) R \\
        \kappa & = & \frac{3 n - 2}{3 \left( n - 1 \right)} .
      \end{eqns}%

    Experimentelle Werte sind durchgehend kleiner als die mit der bisherigen Theorie vorausgesagten.
    Des Weiteren steigt die spezifische Wärme monoton mit der Temperatur.
    Um dies zu erklären, muss man die Quantelung der Energie berücksichtigen.
    Für die jeweiligen Bewegungsformen gilt für die Größe der Energiequanten, die aufgenommen oder abgegeben werden können,
    \begin{eqns}[rClcrCl]
      \Delta E_\t{trans} & = & z^2 \frac{h^2}{8 m a^2} , & \qquad &  z & \in & \setN \\
      \Delta E_\t{rot} & = & \frac{h^2}{8 \pi^2 \theta} J \left( J + 1 \right) , & \qquad & J & \in & \setN_0 \\
      \Delta E_\t{osc} & = & z h \nu , & \qquad & z & \in & \setN ,
    \end{eqns}%
    wobei $h$ das Plancksche Wirkungsquantum, $a$ die Behältergröße, $m$ die Masse des Moleküls, $\theta$ das Trägheitsmoment um die Rotationsachse und $\nu$ die Eigenfrequenz der Schwingung sind.

    Vergleicht man $\Delta E$ mit der mittleren Energie pro Freiheitsgrad $\frac{1}{2} k_\t{B} T$, so gibt es folgende Fälle:
    Ist $\Delta E \ll k_\t{B} T$, dann kann bei allen Molekülen die Bewegungsform angeregt werden.
    Dies ist für $\Delta E_\t{trans}$ und $\Delta E_\t{rot}$ auch bei sehr tiefen Temperaturen der Fall.
    Allerdings ist oft $\Delta E_\t{osc} \approx k_\t{B} T$.
    Hier muss dann zusätzlich die Energieverteilung der Moleküle im Gas berücksichtigt werden.
    Viele der Moleküle haben nicht genug Energie, um Oszillationen anzugeregen.
    Dieser Anteil steigt mit sinkender Temperatur.
    Es folgt für die mittlere Oszillationsenergie, die bei $k_\t{B} T \gg \Delta E_\t{osc}$ in \eqref{eqn:e-osc-nq} übergeht,
    \begin{eqn}
      \mean{E_\t{osc}} = \frac{h \nu}{\exp \left( \frac{h \nu}{k_\t{B} T} \right) - 1} .
    \end{eqn}%
    Für nicht-gestreckte Moleküle folgt
    \begin{eqns}
      C_\t{V} & = & 3 R + R \sum_{i = 1}^{3 n - 6} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2} \\
      \kappa & = & \frac{4 + \sum\limits_{i = 1}^{3 n - 6} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2}}
      {3 + \sum\limits_{i = 1}^{3 n - 6} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2}}
    \end{eqns}%
    und für gestreckte Moleküle
    \begin{eqns}
      C_\t{V} & = & \frac{5}{2} R + R \sum_{i = 1}^{3 n - 5} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2} \\
      \kappa & = & \frac{\frac{7}{2} + \sum\limits_{i = 1}^{3 n - 5} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2}}
      {\frac{5}{2} + \sum\limits_{i = 1}^{3 n - 5} \left( \frac{h \nu_i}{k_\t{B} T} \right)^2 \frac{\exp \left( \frac{h \nu_i}{k_\t{B} T} \right)}{\left( \exp \left( \frac{h \nu_i}{k_\t{B} T} \right) - 1 \right)^2}} , \eqlabel{eqn:kappa-osc}
    \end{eqns}%
    wobei $\nu_i$ die Eigenfrequenzen der Schwingungszustände des Moleküls sind.

    Die Elastizitätstheorie liefert einen Zusammenhang zwischen $\kappa$ und der Schallgeschwindigkeit $c$ im Gas.
    Da die Moleküle nur durch elastische Stöße mitenander wechselwirken, ist der Schall eine longitudinale Welle, die sich durch Druckschwankungen $\Delta p$ durch das Gas ausbreitet.
    Für die Schallgeschwindigkeit gilt
    \begin{eqn}
      c^2 = \frac{\eta}{\rho}
    \end{eqn}%
    mit der Dichte $\rho$ und dem Kompressionsmodul
    \begin{eqn}
      \eta = - V \frac{\dif{p}}{\dif{V}} .
    \end{eqn}%
    Mit der Poissonschen Gleichung
    \begin{eqn}
      p V^\kappa = \t{const}
    \end{eqn}%
    und \eqref{eqn:gasgleichung} folgt
    \begin{eqn}
      \kappa = \frac{M c^2}{R T} = \frac{M \nu^2 \lambda^2}{R T} , \eqlabel{eqn:calc-kappa}
    \end{eqn}%
    wobei $M$ die molare Masse des Gases und $\nu$ die Frequenz und $\lambda$ die Wellenlänge der Schallwelle sind.
  \section{Versuchsaufbau}
    \label{aufbau}
    Die Messapparatur besteht im Wesentlichen aus zwei Piezokristallen, die in einem luftdichten Behälter mit dem zu untersuchenden Gas einander gegenüber installiert sind. 
    Der Sendekristall ist auf einem Schlitten montiert, der durch eine Mikrometerschraube verschoben werden kann.
    Eine Messskala auf der Schraube gibt an, wie weit der Schlitten verschoben wurde.
    Durch Anlegen einer Sinusspannung an den Sendekristall erzeugt dieser eine Schallwelle, die sich durch das Gas zum  Empfängerkristall ausbreitet und diesen zu Schwingungen anregt.
    Die abgegriffene Spannung am Empfängerkristall wird auf der $y$-Achse eines Oszilloskops aufgetragen.
    Die Wechselspannung für den Sendekristall wird mit einem quarzgestuerten Generator erzeugt, dessen Referenzausgang mit dem $x$-Achseneingang des Oszilloskops verbunden wird.
    Dadurch enstehen Lissajous-Figuren, aus deren Fläche man Informationen über die Phasenverschiebung der am Empfänger ankommenden und am Sender erzeugten Schwingung, sowie der Amplitude der Schallwelle gewinnen kann.

    Des Weiteren befindet sich im Behältnis ein Temperaturfühler, der mit einem elektronischen Thermomenter verbunden ist.
    
    Außerhalb des Behälters befindet sich ein System aus diversen Ventilen und Zuleitungen mit denen man $\text{CO}_2$ und $\text{Ar}$, sowie Luft durch ein Belüftungsventil in den Behälter einleiten kann, sowie diesen mit einer Vakuumpumpe wieder evakuieren kann. 
    Zur Veranschaulichung ist Abbildung \ref{fig:aufbau} gegeben.
    \begin{figure}
      \includegraphics[scale=0.6]{graphic/aufbau}
      \caption{Aufbau des Versuchs \cite{anleitung202}}
      \label{fig:aufbau}
    \end{figure}%
  \section{Versuchsdurchführung}
    Zunächst wechselt man am Generator solange die Frequenz $\nu$, bis man die Eigenfrequenz des Sendekristalls erreicht hat. 
    Dies erkennt man daran, dass die Amplitude maximal wird, also die Fläche der Lissajous-Figur am größten wird.
    
    Nun wird der Behälter evakuiert und das gewünschte Gas eingeleitet.
    Daraufhin wird der Schlitten mit der Schraube auf Nullstellung zurückgefahren.
    Man verschiebt nun den Schlitten und  liest die Strecke auf der Schraube ab, bei der die Lissajous-Figur zu einer Geraden wird, also die in Kapitel \ref{aufbau} beschriebene Phasenverschiebung $0$ oder $\pi$ beträgt.
    Die Differenz zweier solch ermittelten, aufeinanderfolgenden Werte $L$ beträgt eine halbe Wellenlänge $\frac{\lambda}{2}$ der Schallwelle.
  \section{Messwerte}
  Die gemessenen Werte sind in Tabelle \ref{tab:messwerte} gegeben.
  Sonstige Werte und Konstanten sind in Tabelle \ref{tab:sonstige} aufgelistet.  
    \begin{table}
      \input{table/L.tex}
      \caption{Messwerte für $L$}
      \label{tab:messwerte}
    \end{table}%
    \begin{table}
      \input{table/const.tex}
      \caption{Sonstige Werte}
      \label{tab:sonstige}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    \begin{eqns}
      \lambda_i & = & 2 \left( L_i - L_{i - 1} \right) \eqlabel{eqn:calc-lambda} \\
      \rerror{\kappa} & = & \sqrt{4 \left( \rerror{\lambda} \right)^2}
    \end{eqns}%

    \begin{table}
      \input{table/kappa.tex}
      \caption{Berechnete Werte}
      \label{tab:berechnet}
    \end{table}%
    Die berechneten Werte für die Wellenlänge (Gleichung \eqref{eqn:calc-lambda}) und $\kappa$ (Gleichung \eqref{eqn:calc-kappa}), sowie deren Abweichung vom Mittelwert und deren Standardabweichung stehen in Tabelle \ref{tab:berechnet}.
    Aus dem gemessenen Wert für $\kappa_{\ce{N2}}$ kann man den Schluss ziehen, dass \ce{N2} bei Raumtemperatur als starr betrachtet werden kann, da der theoretische Wert aus \eqref{eqn:kappa2gestr} gut mit diesem übereinstimmt.
    Die berechneten Werte für $\kappa_{\ce{Ar} , \t{theor}}$ (Gleichung \eqref{eqn:kappa1}) und $\kappa_{\ce{CO2} , \t{theor}}$ (Gleichung \eqref{eqn:kappa-osc}) liegen auch nah an den gemessenen Werten.
    Dies zeigt, dass dieser Versuch gut zur Bestimmung von $\kappa$ geeignet ist.
    Eine geringe Fehlerquelle bestand darin, dass die Lissajous-Figuren unscharf wurden und man nicht genau ablesen konnte, wann die Fläche verschwand.
  \makebibliography
\end{document}
