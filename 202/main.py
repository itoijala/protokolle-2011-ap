#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

name = [r'\t{Ar}',
        r'\t{N}_2',
        r'\t{CO}_2']

T = loadtxt('data/T') + 273.15

L_tmp = loadtxt('data/L', unpack=True) * 1e-3
L = list((list((value for value in tmp if not isnan(value))) for tmp in L_tmp))

table = r'\sisetup{table-format=2.2,round-mode=places,round-precision=2}' + '\n'
table += r'\begin{tabular}{c s' + ' S' * len(L) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{$' + value + '$}' for value in name)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $T$ & \celsius'
for i in range(len(T)):
  table += ' & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(T[i] - 273.15) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $L$ & \milli\meter'

for i in range(max_len(L)):
  if i > 0:
    table += '  &'
  for j in range(len(L)):
    table += ' & '
    if i < len(L[j]):
      table += str(L[j][i] * 1e3)
    else:
      table = table[:-1]
  table += r' \\' + '\n'

table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L.tex', 'w').write(table)

R = N_A * k_B
nu = loadtxt('data/nu') * 1e3
M = loadtxt('data/M')
nu_co2 = loadtxt('data/nu_co2')

table = r'\sisetup{table-format=2.4e+2,round-mode=places,round-precision=2}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $k_\t{B}$ & \joule\per\kelvin & ' + str(k_B) + r' \\' + '\n'
table += r'  $h$ & \joule\second & ' + str(h) + r' \\' + '\n'
table += r'  $R$ & \joule\per\mole\per\kelvin & ' + '{:.6f}'.format(R) + r' \\' + '\n'
table += r'  $\nu$ & \kilo\hertz & ' + str(nu * 1e-3) + r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(M)):
  table += r'  $M_{' + name[i] + '}$ & \gram\per\mole & \multicolumn{1}{S[round-mode=off]}{' + str(M[i] * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\nu_{\t{CO}_2}$ & \tera\hertz & '
for i in range(len(nu_co2)):
  if i > 0:
    table += '  & & '
  table += r'\multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(nu_co2[i] * 1e-12) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/const.tex', 'w').write(table)

lambdas = list((ediff1d(value) * 2 for value in L))
lambda_mean = list((mean(value) for value in lambdas))
lambda_std = list((std(value) for value in lambdas))
lambda_error = list((error(value) for value in lambdas))
lambda_rerror = list((rerror(value) for value in lambdas))

kappa = []
kappa_error = []
kappa_rerror = []
for i in range(len(L)):
  kappa.append(nu**2 * lambda_mean[i]**2 * M[i] / R / T[i])
  kappa_rerror.append(2 * abs(lambda_rerror[i]))
  kappa_error.append(rel2abs(kappa[i], kappa_rerror[i]))

s = 0
for i in range(len(nu_co2)):
  s += (h * nu_co2[i] / k_B / T[2])**2 * exp(h * nu_co2[i] / k_B / T[2]) / (exp(h * nu_co2[i] / k_B / T[2]) - 1)**2
kappa_theor = [5 / 3,
               7 / 5,
               (7 / 2 + s) / (5 / 2 + s)]

table = r'\sisetup{table-format=1.4,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s' + ' S' * len(L) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{$' + value + '$}' for value in name)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\lambda$ & \milli\meter'

for i in range(max_len(lambdas)):
  if i > 0:
    table += '  &'
  for j in range(len(lambdas)):
    table += ' & '
    if i < len(lambdas[j]):
      table += str(lambdas[j][i] * 1e3)
    else:
      table = table[:-1]
  table += r' \\' + '\n'

table += r'  \midrule' + '\n'
table += r'  $\mean{\lambda}$ & \milli\meter'
for i in range(len(L)):
  table += ' & ' + str(lambda_mean[i] * 1e3)
table += r' \\' + '\n'
table += r'  $\error{\mean{\lambda}}$ & \milli\meter'
for i in range(len(L)):
  table += ' & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(lambda_error[i] * 1e3) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\kappa$ & & ' + ' & '.join(vectorize(str)(kappa)) + r'\\' + '\n'
table += r'  $\error{\kappa}$ &'
for i in range(len(L)):
  table += ' & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(kappa_error[i]) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\kappa_\t{theor}$ &'
for i in range(len(L)):
  table += ' & ' + str(kappa_theor[i])
table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/kappa.tex', 'w').write(table)
