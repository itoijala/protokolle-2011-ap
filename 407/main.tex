\input{header.tex}

\addbibresource{lit.bib}

\renewcommand{\ExperimentNumber}{407}

\begin{document}
  \maketitlepage{Fresnel'sche Formeln}{24.01.2012}{09.02.2012}
  \section{Ziel}
    Ziel des Versuches ist die experimentelle Verifizierung der Fresnel'schen Formeln.
  \section{Theorie}
    Licht breitet sich immer dann geradlinig aus, wenn es nicht mit Materie oder externen Gravitationsfeldern wechselwirkt.
    Wenn das Licht jedoch von einem Medium mit einem Brechungsindex $n_1$ in ein Medium mit dem Brechungsindex $n_2$ übertritt, kommt es in der Regel zur Reflexion und Brechung, da die Ausbreitungsgeschwindigkeiten des Lichts in den Medien verschieden sind.
    Aussagen über die Intensitätsverteilung des Lichts auf die beiden Vorgänge liefert die Wellentheorie des Lichts, also die Maxwell'schen Gleichungen, die sich unter der Annahme, dass nicht-magnetische, isolierende Materialen betrachtet werden, also $\mu \approx 1$ und $\V{\jmath} = 0$, zu
    \begin{eqns}
      \Curl \V{H} &=& \varepsilon \varepsilon_0 \PD{\V{E}}{t} \eqlabel{eqn:maxw_1} \\
       \Curl \V{E}&=& - \mu_0 \PD{\V{H}}{t} \eqlabel{eqn:maxw_2}
    \end{eqns}
    reduzieren.
    $\V{E}$ ist die elektrische, $\V{H}$ die magnetische Feldstärke, $\V{\jmath}$ die Stromdichte, $\varepsilon$ die relative Dielektrizitätskonstante des Mediums, $\varepsilon_0$ die elektrische Feldkonstante, $\mu$ die Permeabilität des Mediums und $\mu_0$ die magnetische Feldkonstante.
    \subsection{Die Strahlungsleistung des elektromagnetischen Feldes}
    Im Folgenden soll ein Zusammenhang zwischen der Intensität des an der Mediengrenze reflektierten Lichts und der Ausbreitungsgeschwindigkeit in einem Medium gefunden werden, der es erlaubt, eine Aussage zur oben genannten Problemstellung zu treffen.

    Mit Hilfe der Identität
    \begin{eqns}
      \Div{\del{\Cross{\V{E}}{\V{H}}}} &=& \DotProduct{\V{H}}{\del{\Curl{\V{E}}}} - \DotProduct{\V{E}}{\del{\Curl \V{H}}}\\
      \itext{lassen sich \eqref{eqn:maxw_1} und \eqref{eqn:maxw_2} nach skalarer Multiplikation mit $\V{E}$ und $\V{H}$ zu}
      \Div{\del{\Cross{\V{E}}{\V{H}}}} &=& -\mu_0 \DotProduct{\V{H}}{\PD{\V{H}}{t}} - \varepsilon \varepsilon_0 \DotProduct{\V{E}}{\PD{\V{E}}{t}} \eqlabel{eqn:maxw_res}
    \end{eqns}
    zusammenfassen.
    Für die elektrische Energie pro Volumeneinheit gilt weiterhin die Formel
    \begin{eqns}
      W_\t{el} &=& \frac{1}{2} \varepsilon \varepsilon_0 E^2 \eqlabel{eqn:w_el} \\
      \itext{und für die magnetische Feldenergie}
      W_\t{mag} &=& \frac{1}{2} \mu_0 H^2 \eqlabel{eqn:w_mag}.
    \end{eqns}
    Es fällt auf, dass die Terme auf der rechten Seite des Gleichheitszeichen von \eqref{eqn:maxw_res} die partiellen zeitlichen Ableitungen der Energien repräsentieren.
    Daher ergibt sich Gleichung \eqref{eqn:maxw_res} zu
    \begin{eqns}
      \Div{\del{\Cross{\V{E}}{\V{H}}}} &+& \PD{W_\t{el}}{t} + \PD{W_\t{mag}}{t} = 0 . \\
      \itext{Durch Integration über ein Volumen folgt mit dem Gauß'schen Satz}
      \Int{\cdot\del{\Cross{\V{E}}{\V{H}}}}{\V{A}, \partial V} &+& {\Style{DDisplayFunc=outset} \pderiv{\del{\Int{W_\t{el}}{V, V} + \Int{W_\t{mag}}{V, V}}}{t}} = 0 . \eqlabel{eqn:kontinuität}
    \end{eqns}
    Da durch die magnetische und elektrische Energie alle Energieformen in dem Volumen vertreten sind, stellt der erste Term in Gleichung \eqref{eqn:kontinuität} einen Ausdruck für den Energiestrom pro Zeiteinheit durch die Oberfläche des Volumens dar.
    Dieser Energietransport, der unabhängig von der Materie ist, wird als Strahlung und die Größe
    \begin{eqn}
      \V{S} = \Cross{\V{E}}{\V{H}}
    \end{eqn}
    als Poynting-Vektor bezeichnet, dessen Dimension Leistung pro Fläche ist.

    Im Folgenden wird der Betrag von $\V{S}$ betrachtet; die Vektoren $\V{E}$ und $\V{H}$ werden als ebene Wellen angenommen.
    Mit Hilfe der Maxwell'schen Gleichungen ergibt sich folgender Zusammenhang zwischen den Beträgen des magnetischen und elektrischen Feldes:
    \begin{eqn}
      H_0 = \sqrt{\frac{\varepsilon \varepsilon_0}{\mu_0}} E_0 ,
    \end{eqn}
    woraus mit den Gleichungen \eqref{eqn:w_el} und \eqref{eqn:w_mag} die Äquivalenz von elektrischer und magnetischer Energie pro Volumeneinheit in einem elektromagnetischen Feld folgt.
    Aus der Äquivalenz und der Annahme, dass die Welle mit ihrer Ausbreitungsgeschwindigkeit $v$ durch die Oberfläche $A \coloneqq \partial V$ des Volumens $V$ strömt, also die Energie in der Zeit $\dif{t}$ ein Volumen $A v \dif{t}$ ausfüllt, ergibt sich für das Flussintegral
    \begin{eqn}
      \Int{\V{S}}{\V{A}, \partial V} \dif{t} = A \varepsilon \varepsilon_0 v E^2 \dif{t} . \eqlabel{eqn:strahlung}
    \end{eqn}
    Somit ergibt sich für den Betrag des Poynting-Vektors
    \begin{eqn}
      S = \varepsilon \varepsilon_0 v E^2 .
    \end{eqn}
    \subsection{Amplitude des reflektierten Strahls}
    Aus der Kenntnis der Intensitäten der einfallenden und reflektierten Strahlen kann auch die Intensität des gebrochenen Strahls berechnet werden.
    Für den reflektierten Teil lässt sich die Energiegleichung
    \begin{eqn}
      S_\t{e}F_\t{e} = S_\t{r}F_\t{e} + S_\t{d}F_\t{d} ,
    \end{eqn}
    aufstellen, wobei $F$ die Querschnittsfläche des jeweiligen Strahls ist \mbox{(Abbildung \ref{fig:theory1})}.
    Mit der Beziehung \eqref{eqn:strahlung} und geometrischen Überlegungen anhand \mbox{Abbildung \ref{fig:theory2}} lässt sie sich unter der Annahme, dass das Licht aus dem Vakuum mit der Vakuumlichtgeschwindigkeit $c$ auf ein Medium trifft, umformen zu
    \begin{eqn}
      c\varepsilon_0E_\t{e}^2\Cos{\alpha} = c \varepsilon_0 E_\t{r}^2 \Cos{\alpha} + v \varepsilon \varepsilon_0 E_\t{d}^2 \Cos{\beta} . \eqlabel{eqn:energiegl}
    \end{eqn}
    \begin{figure}
      \includesgraphics{graphic/1.pdf}
      \caption{Einfallender, reflektierter und gebrochener Strahl \cite{anleitung407}}
      \label{fig:theory1}
    \end{figure}
    \begin{figure}
      \includesgraphics{graphic/3.pdf}
      \caption{Geometrische Überlegungen bei der Brechung und Reflexion \cite{anleitung407}}
      \label{fig:theory2}
    \end{figure}
    Da das Verhältnis der Lichtgeschwindigkeit im Vakuum und der Geschwindigkeit im Medium gleich dem Brechungsindex ist, folgt mit Hilfe der Maxwell'schen Relation
    \begin{eqn}
      n^2 = \varepsilon
    \end{eqn}
    für \eqref{eqn:energiegl}
    \begin{eqn}
      \del{E_\t{e}^2 - E_\t{r}^2}\Cos{\alpha} = nE_\t{d}^2\Cos{\beta} \eqlabel{eqn:energ} .
    \end{eqn}

    Im Folgenden wird nun zwischen senkrechter $E_\perp$ und tangentialer $E_\parallel$ Polarisation relativ zur Einfallsebene, die von einfallendem und reflektiertem Lichtstrahl aufgespannt wird, unterschieden.
    Zunächst wird die senkrecht polarisierte Komponente des elektrischen Feldes, die tangential zur Grenzebene schwingt, betrachtet.

    Die Elektrodynamik sagt für die tangential zur Grenzfläche zweier Medien mit unterschiedlicher Dielektrizitätskonstante schwingende Komponente aus, dass sie stetig in das Medium übergeht, da das Wegintegral längs einer geschlossenen Kurve durch beide Medien verschwindet.
    Daraus ergibt sich die Stetigkeitsbeziehung für die Tangentialkomponente des senkrecht polarisierten Feldes (eine Normalkomponente gibt es hier nicht); daher folgt
    \begin{eqns}
      \V{E_{\perp, \t{d}}} &=& \V{E_{\perp,\t{e}}} + \V{E_{\perp, \t{r}}}\eqlabel{eqn:stetigkeit}\\
      \itext{für das elektrische Feld, womit sich Gleichung \eqref{eqn:energ} vereinfacht zu}
      \V{E_{\perp, \t{r}}} &=& \V{-E_{\perp, \t{e}}} \frac{n\Cos{\beta} - \Cos{\alpha}}{n\Cos{\beta} + \cos{\alpha}}. \\
      \itext{Die Amplitude des reflektierten Strahls lässt sich mit Hilfe des Snellius'schen Brechungsgesetzes}
      n &=& \frac{\Sin{\alpha}}{\Sin{\beta}}\\
      \itext{in Abhängigkeit von $\beta$ und $\alpha$}
      \Fun{\V{E_{\perp, \t{r}}}}{\alpha} &=& -\V{E_{\perp, \t{e}}} \frac{\Sin{\alpha - \beta}}{\Sin{\alpha + \beta}}, \\
      \itext{oder von $\alpha$ und n}
      \Fun{\V{E_{\perp, \t{r}}}}{\alpha} &=& - \V{E_{\perp, \t{e}}} \frac{\del{\sqrt{n^2 - \Sin{\alpha}^2} - \Cos{\alpha}}^2}{n^2 - 1}\eqlabel{eqn:fresnel_senk}.\\
      \itext{ausdrücken. Mit den Abkürzungen}
      A &\coloneqq& \frac{E_\t{r}}{E_\t{e}} = \sqrt{\frac{I}{I_\t{e}}}\\
      \gamma &\coloneqq&
      \begin{case}
        \del{\frac{A - 1}{A + 1}}^2, & \alpha \le \ArcTan{n} \\
        \del{\frac{A + 1}{A - 1}}^2, & \alpha > \ArcTan{n}
      \end{case} \\
      \itext{ergibt sich}
      n &=& \sqrt{\frac{A^2 + 2 A \Cos{2 \alpha} + 1}{A^2 - 2A + 1}}. \eqlabel{eqn:n_senk}
      \end{eqns}
      Aus den Gleichungen folgt, dass für $\alpha = \frac{\pi}{2}$ der Bruch in Gleichung \eqref{eqn:fresnel_senk} gleich 1 wird (streifender Einfall).
    Bei senkrechtem Einfall ergibt sich für die senkrecht polarisierte Komponente
    \begin{eqn}
      \Fun{\V{E_{\perp, \t{r}}}}{0} = - \V{E_{\perp, \t{e}}} \frac{n - 1}{n + 1}.
    \end{eqn}

    Für die tangential polarisierte, also senkrecht zur Grenzfläche schwingende Komponente, die sich in eine Tangential- und Normalkomponente aufteilt, ergibt sich für den tangentialen Anteil eine zu Gleichung \eqref{eqn:stetigkeit} analoge Stetigkeitsbedingung für die Tangentialkomponenten.
    Ähnlich wie beim senkrecht polarisierten Feldvektor ergibt sich mit Hilfe des Snellius'schen Brechungsgesetzes für den tangentialen Feldvektor
    \begin{eqns}
      \Fun{\V{E_{\parallel, \t{r}}}}{\alpha} &=& \V{E_{\parallel, \t{e}}} \frac{\Tan{\alpha - \beta}}{\Tan{\alpha + \beta}}\\
      \itext{oder}
      \Fun{\V{E_{\parallel, \t{r} }}}{\alpha} &=& \V{E_{\parallel, \t{e}}} \frac{n^2 \Cos{\alpha} - \sqrt{n^2 - \Sin{\alpha}^2}}{n^2 \Cos{\alpha} + \sqrt{n^2 - \Sin{\alpha}^2}}.\\
      \itext{Für die Auswertung erweist sich folgende Umformung als nützlich}
n &=& \sqrt{\frac{1}{2 \gamma \Cos{\alpha}^2} \del{1 + \sqrt{1 - 4 \gamma \Cos{\alpha}^2 \Sin{\alpha}^2}}}. \eqlabel{eqn:n_paral}
    \end{eqns}
    Die tangentiale Komponente des elektrischen Feldes verschwindet also für
    \begin{eqn}
      \alpha_\t{B} + \beta_\t{B} = \frac{\pi}{2}.
    \end{eqn}
    Nach Snellius gilt
    \begin{eqn}
      \alpha_\t{B} = \ArcTan{n} . \eqlabel{eqn:brewster}
    \end{eqn}
    $\alpha_\t{B}$ ist der so genannte Brewsterwinkel, bei dem ein Strahl nicht mehr reflektiert wird und mit voller Intensität in das Medium eindringt.
  \section{Aufbau und Durchführung}
    Der Versuchsaufbau besteht aus einem Helium-Neon-Laser, der monochromatisches und linear polarisiertes Licht der Wellenlänge $\lambda$ aussendet.
    Dieses trifft auf einen Silizium-Spiegel, der den Strahl reflektiert und auf einen Photodetektor wirft.
    Der Laser ist um seine Symmetrieachse drehbar, sodass die Polarisation des elektrischen Feldes veränderbar ist.
    Hier wird mit einer Polarisation mit einem Winkel von $0$ und $\frac{\pi}{2}$ relativ zur Einfallsebene gemessen.
    Der Spiegel ist auf einem Drehteller aufgestellt, der um seine vertikale Achse gedreht werden kann; der jeweilige Winkel, unter dem der Laser auf den Spiegel einfällt, kann auf einer Messskala abgelesen werden.
    Der Kurzschlussstrom des Photoelements ist proportional zur einfallenden Intensität; der Detektor ist auf einem schwenkbaren Arm angebracht, der es ermöglicht das Photoelement in den Strahl zu drehen.

    Die Hauptschwierigkeit besteht in der Justage des Lasers.
    Zunächst wird der Probenhalter aus dem Strahlengang entfernt, sodass der Strahl direkt auf das Photoelement trifft; bei dieser Gelegenheit wird die einfallende Lichtintensität $I_\t{e}$ gemessen.
    Nun wird ein Polarisationsfilter in den Strahlengang gebracht.
    Der Laser wird solange um seine Längsachse gedreht, bis ein Intensitätsminimum erreicht wird.
    Die Höhenverstellung des Lasers muss so justiert werden, dass der Laserstrahl auf die horizontalen Markierungsstriche den Detektorarm in $\SI{0}{\degree}-$ und in $\SI{180}{\degree}$-Stellung trifft.
    Anschließend wird in die Bohrung des Goniometers eine Zentriernadel gesteckt, auf die der Laser exakt ausgerichtet wird.
    Nun wird der Spiegel wieder in das Goniometer gesetzt und ein Einfallswinkel von $\SI{0}{\degree}$ eingestellt; mit Hilfe einer Stellschraube am Spiegel wird der Spiegel so eingestellt, dass der Laserstrahl wieder auf den Laserkopf trifft.
    Abschließend wird das Goniometer genau auf $\SI{0}{\degree}$ gestellt, die Klemmschraube ein wenig gelöst und der Probenhalter um seine vertikale Achse gedreht, sodass der einfallende Strahl in sich reflektiert wird.
    Zieht man die Klemmschraube fest, so kann man nun den Winkel schrittweise abfahren und die Intensität messen.
    Zu Korrekturzwecken bei der Auswertung wird am Ende noch der Dunkelstrom $I_\t{du}$ gemessen.
    Zur Veranschaulichung ist Abbildung \ref{fig:aufbau} gegeben.
    \begin{figure}
      \includesgraphics{graphic/5.pdf}
      \caption{Versuchsaufbau \cite{anleitung407}}
      \label{fig:aufbau}
    \end{figure}
  \section{Messwerte und Auswertung}
    \input{statistics.tex}%

    Die Messwerte für die Intensitäten bei $\SI{0}{\degree}$- (parallel) und $\SI{90}{\degree}$-Polarisation (senkrecht) sind in den Tabellen \ref{tab:0} und \ref{tab:90} aufgeführt.
    Des Weiteren stehen die mit den Formeln \eqref{eqn:n_paral} und \eqref{eqn:n_senk} berechneten Brechungsindices ebenfalls in diesen Tabellen.
    Die gemessenen Ströme werden mit dem Dunkestrom korrigiert:
    \begin{eqns}
      I_\t{e} &=& I_{\t{e},\t{ex}} - I_\t{du} \\
      I &=& I_\t{ex} - I_\t{du} .
    \end{eqns}
    Alle Brechungsindices, die zu stark vom errechneten Mittelwert abweichen, also ab \SI{71}{\degree} bei senkrechter Polarisation, werden verworfen.
    Bei paralleler Polarisation zeigen die berechneten Brechungsindizes keine systematische Abhängigkeit von $\alpha$ (Abbildung \ref{fig:n_0}).
    Bei senkrechter Polarisation weichen die berechneten Werte von $n$ ab \SI{71}{\degree} stark vom Mittelwert ab (Abbildung \ref{fig:n_90}); sie werden nicht in den Mittelwert einbezogen.
    Der Brewsterwinkel, der mit Formel \eqref{eqn:brewster} errechnet wurde, passt gut zu den Messwerten, auch die statistischen Schwankungen von Brechungsindex und Brewsterwinkel sind klein.
    Die Messwerte und die jeweiligen Theoriekurven, die mit den gemittelten Brechungsindizes aus dem Experiment berechnet wurden, sind zum Vergleich in Abbildung \ref{fig:0} und \ref{fig:90} geplottet.
    In Tabelle \ref{tab:0} ist des Weiteren ein Literaturwert für den Brechungsindex von Silizium eingetragen.
    Er liegt nah an dem experimentellen Wert bei paralleler Polarisation und weicht bei senkrechter Polarisation mehr ab.
    \begin{table}
      \input{table/0.tex}
      \caption{Messwerte und errechnete Brechungsindices für Polarisation parallel zur Einfallsebene}
      \label{tab:0}
    \end{table}
    \begin{figure}
      \includesgraphics{graph/0.pdf}
      \caption{Messwerte und Theoriekurve für Polarisation parallel zur Einfallsebene}
      \label{fig:0}
    \end{figure}
    \begin{figure}
      \includesgraphics{graph/n_0.pdf}
      \caption{Berechnete Werte von $n$ in Abhängigkeit von $\alpha$ für Polarisation parallel zur Einfallsebene}
      \label{fig:n_0}
    \end{figure}
    \begin{table}
      \input{table/90.tex}
      \caption{Messwerte und errechnete Brechungsindices für Polarisation senkrecht zur Einfallsebene}
      \label{tab:90}
    \end{table}
    \begin{figure}
      \includesgraphics{graph/90.pdf}
      \caption{Messwerte und Theoriekurve für Polarisation senkrecht zur Einfallsebene}
      \label{fig:90}
    \end{figure}
    \begin{figure}
      \includesgraphics{graph/n_90.pdf}
      \caption{Berechnete Werte von $n$ in Abhängigkeit von $\alpha$ für Polarisation senkrecht zur Einfallsebene}
      \label{fig:n_90}
    \end{figure}
  \nocite{n-silicon}
  \makebibliography
\end{document}
