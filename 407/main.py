#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

lambda_ = loadtxt('data/lambda') * 1e-9
I_e = loadtxt('data/I_e') * 1e-3
I_du = loadtxt('data/I_du') * 1e-9

I_min = loadtxt('data/I_min0') * 1e-6

alpha, I_ex = loadtxt('data/0', unpack=True)
alpha *= deg_rad
I_ex *= 1e-6
I = I_ex - I_du

A = sqrt(I / I_e)
gamma = ((A - 1) / (A + 1))**2
a = list(I).index(min(I))
for i in range(len(gamma)):
  if i > a:
    gamma[i] = 1 / gamma[i]

n = sqrt(1 / 2 / gamma / cos(alpha)**2 * (1 + sqrt(1 - 4 * gamma * cos(alpha)**2 * sin(alpha)**2)))
n_mean = ufloat((mean(n), error(n)))

alpha_B = atan(n_mean)

savetxt('build/plot/0', (alpha, A, n))
savetxt('build/plot/n0', (vals(n_mean),))

alpha1 = alpha[:len(alpha) / 2]
alpha2 = alpha[len(alpha) / 2:]
I_ex1 = I_ex[:len(alpha) / 2]
I_ex2 = I_ex[len(alpha) / 2:]
n1 = n[:len(alpha) / 2]
n2 = n[len(alpha) / 2:]

table = r'\sisetup{round-mode=places}' + '\n'
table += r'\begin{tabular}{S[table-format=2.0,round-mode=off] S[table-format=1.3,round-precision=3] S[table-format=3.2] S[table-format=1.2,round-precision=2] | S[table-format=2.0,round-mode=off] S[table-format=1.3,round-precision=3] S[table-format=3.2] S[table-format=2.2,round-precision=2]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$\lambda$} & \multicolumn{1}{s}{\nano\meter} & \multicolumn{2}{S[table-format=3.2,round-mode=off]}{' + str(int(lambda_ * 1e9)) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$I_{\t{e},\t{ex}}$} & \multicolumn{1}{s}{\milli\ampere} & \multicolumn{2}{S[table-format=3.2]}{' + str(I_e * 1e3) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$I_\t{du}$} & \multicolumn{1}{s}{\nano\ampere} & \multicolumn{2}{S[table-format=3.2,round-mode=off]}{' + str(int(I_du * 1e9)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{\alpha}{\degree}} & \multicolumn{1}{c}{\SIlabel{\alpha}{\radian}} & \multicolumn{1}{c}{\SIlabel{I_\t{ex}}{\micro\ampere}} & \multicolumn{1}{c|}{$n$} & \multicolumn{1}{c}{\SIlabel{\alpha}{\degree}} & \multicolumn{1}{c}{\SIlabel{\alpha}{\radian}} & \multicolumn{1}{c}{\SIlabel{I_\t{ex}}{\micro\ampere}} & \multicolumn{1}{c}{$n$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(alpha1)):
  table += '  ' + '{:.0f}'.format(alpha1[i] / deg_rad) + ' & ' + '{:.6f}'.format(alpha1[i]) + ' & ' + str(I_ex1[i] * 1e6) + ' & ' + '{:.6f}'.format(n1[i]) + ' & ' + '{:.0f}'.format(alpha2[i] / deg_rad) + ' & ' + '{:.6f}'.format(alpha2[i]) +' & ' + str(I_ex2[i] * 1e6) + ' & ' + '{:.6f}'.format(n2[i]) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  \multicolumn{1}{c}{$\mean{n}$} & & \multicolumn{2}{S}{' + '{:.6f}'.format(float(vals(n_mean))) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$\merror{n}$} & & \multicolumn{2}{S}{' + '{:.6f}'.format(float(stds(n_mean))) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$n_\t{lit}$ \cite{n-silicon}} & & \multicolumn{2}{S[round-mode=off]}{3.879} \\' + '\n'
table += r'  \multicolumn{1}{c}{$\lambda_\t{lit}$} & \multicolumn{1}{s}{\nano\meter} & \multicolumn{2}{S[round-mode=off]}{630} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  \multicolumn{1}{c}{$\alpha_\t{B}$} & \multicolumn{1}{s}{\radian} & \multicolumn{2}{S[round-precision=4]}{' + '{:.6f}'.format(float(vals(alpha_B))) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$\error{\alpha_\t{B}}$} & \multicolumn{1}{s}{\radian} & \multicolumn{2}{S[round-mode=figures,round-precision=1]}{' + '{:.6f}'.format(float(stds(alpha_B))) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/0.tex', 'w').write(table)

I_min = loadtxt('data/I_min90') * 1e-6

alpha, I_ex = loadtxt('data/90', unpack=True)
alpha *= deg_rad
I_ex *= 1e-6
I = I_ex - I_du

A = sqrt(I / I_e)

n = sqrt((A**2 + 2 * A * cos(2 * alpha) + 1) / (A**2 - 2 * A + 1))
n_mean = ufloat((mean(n[:-9]), error(n[:-9])))

savetxt('build/plot/90', (alpha, A, n))
savetxt('build/plot/n90', (vals(n_mean),))

alpha1 = alpha[:len(alpha) / 2 + 1]
alpha2 = alpha[len(alpha) / 2 + 1:]
I_ex1 = I_ex[:len(alpha) / 2 + 1]
I_ex2 = I_ex[len(alpha) / 2 + 1:]
n1 = n[:len(alpha) / 2 + 1]
n2 = n[len(alpha) / 2 + 1:]

table = r'\sisetup{round-mode=places}' + '\n'
table += r'\begin{tabular}{S[table-format=2.0,round-mode=off] S[table-format=1.3,round-precision=3] S[table-format=3.2] S[table-format=1.2,round-precision=2] | S[table-format=2.0,round-mode=off] S[table-format=1.3,round-precision=3] S[table-format=3.2] S[table-format=1.2,round-precision=2]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{\alpha}{\degree}} & \multicolumn{1}{c}{\SIlabel{\alpha}{\radian}} & \multicolumn{1}{c}{\SIlabel{I_\t{ex}}{\micro\ampere}} & \multicolumn{1}{c|}{$n$} & \multicolumn{1}{c}{\SIlabel{\alpha}{\degree}} & \multicolumn{1}{c}{\SIlabel{\alpha}{\radian}} & \multicolumn{1}{c}{\SIlabel{I_\t{ex}}{\micro\ampere}} & \multicolumn{1}{c}{$n$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(alpha1)):
  table += '  ' + '{:.0f}'.format(alpha1[i] / deg_rad) + ' & ' + '{:.6f}'.format(alpha1[i]) + ' & ' + str(I_ex1[i] * 1e6) + ' & ' + '{:.6f}'.format(n1[i])
  if i < len(alpha2):
    table += ' & ' + '{:.0f}'.format(alpha2[i] / deg_rad) + ' & ' + '{:.6f}'.format(alpha2[i]) + ' & ' + str(I_ex2[i] * 1e6) + ' & ' + '{:.6f}'.format(n2[i]) + r' \\' + '\n'
  else:
    table += r' & & & \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  \multicolumn{1}{c}{$\mean{n}$} & & \multicolumn{2}{S}{' + '{:.6f}'.format(float(vals(n_mean))) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$\merror{n}$} & & \multicolumn{2}{S}{' + '{:.6f}'.format(float(stds(n_mean))) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/90.tex', 'w').write(table)
