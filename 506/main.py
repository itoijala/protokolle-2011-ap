#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

l = loadtxt('data/l') * 1e-2

p_1 = loadtxt('data/p_a_1') * 1e-3
p_2 = loadtxt('data/p_a_2') * 1e-3
I_1, U_1 = loadtxt('data/U_a_1', unpack=True)
I_1 *= 1e-3
I_2, U_2 = loadtxt('data/U_a_2', unpack=True)
I_2 *= 1e-3

A_1, B_1, A_1_error, B_1_error = linregress_error(I_1, U_1)
A_1_rerror = abs2rel(A_1, A_1_error)
B_1_rerror = abs2rel(B_1, B_1_error)
A_2, B_2, A_2_error, B_2_error = linregress_error(I_2[16:], U_2[16:])
A_2_rerror = abs2rel(A_2, A_2_error)
B_2_rerror = abs2rel(B_2, B_2_error)

savetxt('build/plot/U_a_1-line', (A_1, B_1))
savetxt('build/plot/U_a_2-line', (A_2, B_2))

open('build/result/p_a_1.tex', 'w').write(r'\SI{' + str(p_1 * 1e3) + r'}{\milli\bar}')
open('build/result/p_a_2.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(p_2 * 1e3) + r'}{\milli\bar}')
open('build/result/A_1.tex', 'w').write(r'\SI{' + '{:.0f}'.format(A_1 * 1e-3) + '+-' + '{:.0f}'.format(A_1_error * 1e-3) + r'}{\volt\per\milli\ampere}')
open('build/result/A_2.tex', 'w').write(r'\SI{' + '{:.0f}'.format(A_2 * 1e-3) + '+-' + '{:.0f}'.format(A_2_error * 1e-3) + r'}{\volt\per\milli\ampere}')
open('build/result/I_lin_1.tex', 'w').write(r'\SIrange[round-mode=places,round-precision=2]{' + str(I_1[0] * 1e3) + '}{' + str(I_1[-1] * 1e3) + r'}{\milli\ampere}')
open('build/result/I_lin_2.tex', 'w').write(r'\SIrange[round-mode=places,round-precision=2]{' + str(I_2[16] * 1e3) + '}{' + str(I_2[-1] * 1e3) + r'}{\milli\ampere}')

table = r'\sisetup{table-format=+3.2,round-mode=figures}' + '\n'
table += r'\begin{tabular}{c s S S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $p$ & \milli\bar & \multicolumn{1}{S[round-precision=1]}{' + str(p_1 * 1e3) + r'} & \multicolumn{1}{S[round-precision=2]}{' + str(p_2 * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $A$ & \volt\per\milli\ampere & \multicolumn{1}{S[round-precision=3]}{' + str(A_1 * 1e-3) + r'} & \multicolumn{1}{S[round-precision=2]}{' + str(A_2 * 1e-3) + r'} \\' + '\n'
table += r'  $\error{A}$ & \volt\per\milli\ampere & \multicolumn{1}{S[round-precision=1]}{' + str(A_1_error * 1e-3) + r'} & \multicolumn{1}{S[round-precision=1]}{' + str(A_2_error * 1e-3) + r'} \\' + '\n'
table += r'  $B$ & \volt & \multicolumn{1}{S[round-precision=3]}{' + str(B_1) + r'} & \multicolumn{1}{S[round-precision=3]}{' + str(B_2) + r'} \\' + '\n'
table += r'  $\error{B}$ & \volt & \multicolumn{1}{S[round-precision=1]}{' + str(B_1_error) + r'} & \multicolumn{1}{S[round-precision=1]}{' + str(B_2_error) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/a-calc.tex', 'w').write(table)

I_1_1 = I_1[0:len(I_1) / 2]
I_1_2 = I_1[len(I_1) / 2:]
U_1_1 = U_1[0:len(U_1) / 2]
U_1_2 = U_1[len(U_1) / 2:]
I_2_1 = I_2[0:len(I_2) / 2]
I_2_2 = I_2[len(I_2) / 2:]
U_2_1 = U_2[0:len(U_2) / 2]
U_2_2 = U_2[len(U_2) / 2:]

table = r'\sisetup{round-mode=places}' + '\n'
table += r'\begin{tabular}{' + ' || '.join([' | '.join(['S[table-format=1.2,round-precision=2] S[table-format=3.0,round-mode=off]'] * 2)] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-precision=1]||}{' + str(p_1 * 1e3) + '}'
table += r' & \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-precision=1]}{' + str(p_2 * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(4):
  table += r'\multicolumn{1}{c}{$I / \si{\milli\ampere}$} & \multicolumn{1}{c'
  if i < 3:
    table += '|'
  if i == 1:
    table += '|'
  table += r'}{$U / \si{\volt}$}'
  if i < 3:
    table += ' & '
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(max_len([I_1_1, I_2_1])):
  table += '  '
  if i < len(I_1_1):
    table += str(I_1_1[i] * 1e3) + ' & ' + no_point(U_1_1[i]) + ' &'
  else:
    table += '& &'
  if i < len(I_1_2):
    table += ' ' + str(I_1_2[i] * 1e3) + ' & ' + no_point(U_1_2[i])
  else:
    table += ' &'
  table += ' & '
  if i < len(I_2_1):
    table += str(I_2_1[i] * 1e3) + ' & ' + no_point(U_2_1[i]) + ' &'
  else:
    table += '& &'
  if i < len(I_2_2):
    table += ' ' + str(I_2_2[i] * 1e3) + ' & ' + no_point(U_2_2[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/U_a.tex', 'w').write(table)

p = loadtxt('data/p_b') * 1e-3
d = loadtxt('data/d_b') * 1e-2
I, U = loadtxt('data/U_b', unpack=True)
I *= 1e-3

I_1 = I[0:len(I) / 2]
I_2 = I[len(I) / 2:]
U_1 = U[0:len(U) / 2]
U_2 = U[len(U) / 2:]

table = r'\sisetup{round-mode=places}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S[table-format=1.2,round-precision=2] S[table-format=3.0,round-mode=off]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-precision=1]}{' + str(p * 1e3) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$d$} & \multicolumn{1}{s}{\centi\meter} & \multicolumn{2}{S[round-mode=off]}{' + no_point(d * 1e2) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for j in range(1,3):
  table += r'\multicolumn{1}{c}{$I / \si{\milli\ampere}$} & \multicolumn{1}{c'
  if j < 2:
    table += '|'
  table += r'}{$U / \si{\volt}$}'
  if j < 2:
    table += ' & '
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for j in range(len(I_1)):
  table += '  ' + str(I_1[j] * 1e3) + ' & ' + no_point(U_1[j]) + ' &'
  if j < len(I_2):
    table += ' ' + str(I_2[j] * 1e3) + ' & ' + no_point(U_2[j])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/U_b.tex', 'w').write(table)

p_1 = loadtxt('data/p_c_1') * 1e-3
p_2 = loadtxt('data/p_c_2') * 1e-3
I_S_1 = loadtxt('data/I_S_c_1') * 1e-3
I_S_2 = loadtxt('data/I_S_c_2') * 1e-3
s_1, U_1 = loadtxt('data/U_c_1', unpack=True)
s_1 *= 1e-3
s_2, U_2 = loadtxt('data/U_c_2', unpack=True)
s_2 *= 1e-3

open('build/result/p_c_1.tex', 'w').write(r'\SI[round-mode=places,round-precision=1]{' + str(p_1 * 1e3) + r'}{\milli\bar}')
open('build/result/p_c_2.tex', 'w').write(r'\SI[round-mode=places,round-precision=2]{' + str(p_2 * 1e3) + r'}{\milli\bar}')
open('build/result/I_S_c_1.tex', 'w').write(r'\SI[round-mode=places,round-precision=2]{' + str(I_S_1 * 1e3) + r'}{\milli\ampere}')
open('build/result/I_S_c_2.tex', 'w').write(r'\SI[round-mode=places,round-precision=2]{' + str(I_S_2 * 1e3) + r'}{\milli\ampere}')

s_1_1 = s_1[0:len(s_1) / 2 + 1]
s_1_2 = s_1[len(s_1) / 2 + 1:]
U_1_1 = U_1[0:len(U_1) / 2 + 1]
U_1_2 = U_1[len(U_1) / 2 + 1:]
s_2_1 = s_2[0:len(s_2) / 2]
s_2_2 = s_2[len(s_2) / 2:]
U_2_1 = U_2[0:len(U_2) / 2]
U_2_2 = U_2[len(U_2) / 2:]

table = r'\sisetup{round-mode=places}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S[table-format=3.0,round-mode=off] S[table-format=3.0,round-mode=off]'] * 2) + ' || ' +  ' | '.join(['S[table-format=3.0,round-mode=off] S[table-format=4.0,round-mode=off]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-precision=1]||}{' + str(p_1 * 1e3) + '}'
table += r' & \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-precision=2]}{' + str(p_2 * 1e3) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$I_\t{S}$} & \multicolumn{1}{s}{\milli\ampere} & \multicolumn{2}{S[round-precision=2]||}{' + str(I_S_1 * 1e3) + '}'
table += r' & \multicolumn{1}{c}{$I_\t{S}$} & \multicolumn{1}{s}{\milli\ampere} & \multicolumn{2}{S[round-precision=2]}{' + str(I_S_2 * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(4):
  table += r'\multicolumn{1}{c}{$s / \si{\milli\meter}$} & \multicolumn{1}{c'
  if i < 3:
    table += '|'
  if i == 1:
    table += '|'
  table += r'}{$U / \si{\volt}$}'
  if i < 3:
    table += ' & '
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(max_len([s_1_1, s_2_1])):
  table += '  '
  if i < len(s_1_1):
    table += no_point(s_1_1[i] * 1e3) + ' & ' + no_point(U_1_1[i]) + ' &'
  else:
    table += '& &'
  if i < len(s_1_2):
    table += ' ' + no_point(s_1_2[i] * 1e3) + ' & ' + no_point(U_1_2[i])
  else:
    table += ' &'
  table += ' & '
  if i < len(s_2_1):
    table += no_point(s_2_1[i] * 1e3) + ' & ' + no_point(U_2_1[i]) + ' &'
  else:
    table += '& &'
  if i < len(s_2_2):
    table += ' ' + no_point(s_2_2[i] * 1e3) + ' & ' + no_point(U_2_2[i])
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/U_c.tex', 'w').write(table)

p = loadtxt('data/p_d') * 1e-3
I_S = loadtxt('data/I_S_d') * 1e-3
s, I = loadtxt('data/I_d', unpack=True)
s *= 1e-3
I *= 1e-9

s_1 = s[0:len(s) / 2]
s_2 = s[len(s) / 2:]
I_1 = I[0:len(I) / 2]
I_2 = I[len(I) / 2:]

table = r'\sisetup{}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S[table-format=3.0] S[table-format=1.3,round-mode=places,round-precision=3]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$p$} & \multicolumn{1}{s}{\milli\bar} & \multicolumn{2}{S[round-mode=places,round-precision=2]}{' + str(p * 1e3) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$I_\t{S}$} & \multicolumn{1}{s}{\milli\ampere} & \multicolumn{2}{S}{' + no_point(I_S * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for j in range(1,3):
  table += r'\multicolumn{1}{c}{$s / \si{\milli\meter}$} & \multicolumn{1}{c'
  if j < 2:
    table += '|'
  table += r'}{$I / \si{\nano\ampere}$}'
  if j < 2:
    table += ' & '
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for j in range(len(s_1)):
  table += '  ' + no_point(s_1[j] * 1e3) + ' & ' + str(I_1[j] * 1e9) + ' &'
  if j < len(s_2):
    table += ' ' + no_point(s_2[j] * 1e3) + ' & ' + str(I_2[j] * 1e9)
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/I_d.tex', 'w').write(table)
