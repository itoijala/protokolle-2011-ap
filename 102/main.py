#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

R_D2 = loadtxt('data/R_D2') * 1e-6
R_D = R_D2 / 2
R_D_mean = mean(R_D)
R_D_std = std(R_D)
R_D_error = error(R_D)
R_D_rerror = rerror(R_D)

table = r'\sisetup{table-format=3.3,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $2 R_\t{D}$ & \micro\meter & ' + (r' \\' + '\n  & & ').join((vectorize(str)(R_D2 * 1e6))) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{R_\t{D}}$ & \micro\meter & ' + str(R_D_mean * 1e6) + r' \\' + '\n'
table += r'  $\error{\mean{R_\t{D}}}$ & \micro\meter & ' + str(R_D_error* 1e6) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/R_D.tex', 'w').write(table)

T_G = loadtxt('data/T_G')
T_G_mean = mean(T_G)
T_G_std = std(T_G)
T_G_error = error(T_G)
T_G_rerror = rerror(T_G)

table = r'\sisetup{table-format=2.4,round-mode=figures,round-precision=5}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $T_\t{G}$ & \second & ' + (r' \\' + '\n  & & ').join((vectorize(str)(T_G))) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{T_\t{G}}$ & \second & ' + str(T_G_mean) + r' \\' + '\n'
table += r'  $\error{\mean{T_\t{G}}}$ & \second & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(T_G_error) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/T_G.tex', 'w').write(table)

m_K, m_K_rerror = loadtxt('data/m_K')
m_K *= 1e-3
m_K_error = rel2abs(m_K, m_K_rerror)

R_K2, R_K_rerror = loadtxt('data/R_K2')
R_K2 *= 1e-3
R_K = R_K2 / 2
R_K_error = rel2abs(R_K, R_K_rerror)

L_D, L_D_error = loadtxt('data/L_D') * 1e-2
L_D_rerror = abs2rel(L_D, L_D_error)

E, E_error = loadtxt('data/E')
E_rerror = abs2rel(E, E_error)

theta_KH = loadtxt('data/theta_KH') * 1e-7

N_S = loadtxt('data/N_S')

R_S = loadtxt('data/R_S') * 1e-3

mu_0 = loadtxt('data/mu_0')

table = r'\sisetup{table-format=3.3e+2,round-mode=figures,round-precision=4}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $m_\t{K}$ & \gram & ' + str(m_K * 1e3) + r' \\' + '\n'
table += r'  $\rerror{m_\t{K}}$ & \percent & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(m_K_rerror) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $R_\t{K}$ & \milli\meter & ' + str(R_K * 1e3) + r' \\' + '\n'
table += r'  $\rerror{R_\t{K}}$ & \percent & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(R_K_rerror) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $L_\t{D}$ & \centi\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(L_D * 1e2) + r'} \\' + '\n'
table += r'  $\error{L_\t{D}}$ & \centi\meter & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(L_D_error * 1e2) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $E$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(E * 1e-11) + r'e11} \\' + '\n'
table += r'  $\error{E}$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=1,scientific-notation=true]}{' + str(E_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\theta_\t{KH}$ & \gram\square\centi\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(theta_KH * 1e7) + r'} \\' + '\n'
table += r'  $N_\t{S}$ & & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(N_S) + r'} \\' + '\n'
table += r'  $R_\t{S}$ & \milli\meter & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(R_S * 1e3) + r'} \\' + '\n'
table += r'  $\mu_0$ & \newton\per\ampere\squared & \multicolumn{1}{S[round-mode=off]}{4 \pi e-7} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/other.tex', 'w').write(table)

theta = 2 / 5 * m_K * R_K**2 + theta_KH
theta_error = sqrt((2 / 5 * R_K**2)**2 * m_K_error**2 + (4 / 5 * m_K * R_K)**2 * R_K_error**2)
theta_rerror = abs2rel(theta, theta_error)

G = 8 * pi * theta * L_D / T_G_mean**2 / R_D_mean**4
G_rerror = sqrt(theta_rerror**2 + L_D_rerror**2 + 4 * T_G_rerror**2 + 16 * R_D_rerror**2)
G_error = rel2abs(G, G_rerror)

mu = E / 2 / G - 1
mu_error = sqrt((1 / 2 / E)**2 * E_error**2 + (E / 2 / G**2)**2 * G_error**2)
mu_rerror = abs2rel(mu, mu_error)

Q = - E * G / 3 / (E - 3 * G)
Q_error = sqrt((G**2 / (E - 3 * G)**2)**2 * E_error**2 + (E**2 / (E - 3 * G)**2)**2 * G_error**2)
Q_rerror = abs2rel(Q, Q_error)

table = r'\sisetup{table-format=4.3e2,round-mode=figures,round-precision=5}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\theta$ & \gram\square\centi\meter & ' + str(theta * 1e7) + r' \\' + '\n'
table += r'  $\error{\theta}$ & \gram\square\centi\meter & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(theta_error * 1e7) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $G$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(G * 1e-9) + r'e9} \\' + '\n'
table += r'  $\error{G}$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3,scientific-notation=fixed,fixed-exponent=9]}{' + str(G_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mu$ & & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(mu) + r'} \\' + '\n'
table += r'  $\error{\mu}$ & & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(mu_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $Q$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(Q * 1e-11) + r'e11} \\' + '\n'
table += r'  $\error{Q}$ & \newton\per\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3,scientific-notation=fixed,fixed-exponent=11]}{' + '{:.6f}'.format(Q_error * 1e-8) + r'e8} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/calc1.tex', 'w').write(table)

I_S, I_S_error = loadtxt('data/I_S', unpack=True)
I_S_rerror = vectorize(abs2rel)(I_S, I_S_error)

def calc_B_S(I_S):
  return 8 * mu_0 / sqrt(5)**3 * N_S * I_S / R_S

def calc_B_S_error(I_S, I_S_error):
  return sqrt((8 * mu_0 / sqrt(5)**3 * N_S / R_S)**2 * I_S_error**2)

B_S = vectorize(calc_B_S)(I_S)
B_S_error = vectorize(calc_B_S_error)(I_S, I_S_error)
B_S_rerror = vectorize(abs2rel)(B_S, B_S_error)

T_m_tmp = loadtxt('data/T_m', unpack=True)
T_m = []
for i in range(len(T_m_tmp)):
  T_m.append(list((value for value in T_m_tmp[i] if not isnan(value))))
T_m_mean = vectorize(mean)(T_m)
T_m_std = vectorize(std)(T_m)
T_m_error = vectorize(error)(T_m)
T_m_rerror = vectorize(rerror)(T_m)

table = r'\sisetup{table-format=2.4,round-mode=places,round-precision=2}' + '\n'
table = r'\begin{tabular}{c s' + (' S' * len(I_S)) + '}\n'
table += r'  \toprule' + '\n'
table += r'  $I_\t{S}$ & \ampere'
for i in range(len(I_S)):
  table += ' & \multicolumn{1}{S[round-mode=places,round-precision=2]}{' + str(I_S[i]) + '}'
table += r' \\' + '\n'

table += r'  $\error{I_\t{S}}$ & \ampere & '
for i in range(len(I_S_error)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{S[round-mode=places,round-precision=3]}{' + str(I_S_error[i]) + '}'
table += r' \\' + '\n'

table += r'  $B_\t{S}$ & \milli\tesla & '
for i in range(len(B_S)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{S['
  if B_S[i] != 0:
    table += 'round-mode=figures,round-precision=3'
  else:
    table += 'round-mode=places,round-precision=2'
  table += ']}{' + str(B_S[i] * 1e3) + '}'
table += r' \\' + '\n'

table += r'  $\error{B_\t{S}}$ & \micro\tesla & '
for i in range(len(B_S_error)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{S['
  if B_S_error[i] != 0:
    table += 'round-mode=figures,round-precision=3'
  else:
    table += 'round-mode=places,round-precision=2'
  table += ']}{' + str(B_S_error[i] * 1e6) + '}'
table += r' \\' + '\n'

table += r'  $T_\t{m}$ & \second & '
max_len = len(T_m[0])
for i in range(1, len(T_m)):
  if len(T_m[i]) > max_len:
    max_len = len(T_m[i])
for i in range(max_len):
  if i > 0:
    table += '  & & '
  for j in range(len(T_m)):
    if j > 0:
      table += ' & '
    if i < len(T_m[j]):
      table += r'\multicolumn{1}{S[round-mode=places,round-precision=3]}{' + str(T_m[j][i]) + '}'
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'

table += r'  $\mean{T_\t{m}}$ & \second'
for i in range(len(T_m)):
  table += r' & \multicolumn{1}{S[round-mode=places,round-precision=3]}{' + str(T_m_mean[i]) + '}'
table += r' \\' + '\n'

table += r'  $\error{\mean{T_\t{m}}}$ & \second'
for i in range(len(T_m)):
  table += r' & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(T_m_error[i]) + '}'
table += r' \\' + '\n'

table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/T_m.tex', 'w').write(table)

T_E = loadtxt('data/T_E')
T_E_mean = mean(T_E)
T_E_std = std(T_E)
T_E_error = error(T_E)
T_E_rerror = rerror(T_E)

table = r'\sisetup{table-format=2.3,round-mode=places,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $T_\t{E}$ & \second & '
for i in range(len(T_E)):
  if i > 0:
    table += '  & & '
  table += str(T_E[i]) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{T_\t{E}}$ & \second & \multicolumn{1}{S[round-mode=figures,round-precision=5]}{' + str(T_E_mean) + r'} \\' + '\n'
table += r'  $\error{\mean{T_\t{E}}}$ & \second & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(T_E_error) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/T_E.tex', 'w').write(table)

B_S_mean = mean(B_S)
T_m_2_mean = mean(1 / T_m_mean**2)
B_S_2_mean = mean(B_S**2)
B_S_T_m_2_mean = mean(B_S / T_m_mean**2)

A, B, A_error, B_error = linregress_error(B_S, 1 / T_m_mean**2)
A_rerror = abs2rel(A, A_error)
B_rerror = abs2rel(B, B_error)

m = 4 * pi**2 * theta * A
m_rerror = sqrt(theta_rerror**2 + A_rerror**2)
m_error = rel2abs(m, m_rerror)

B_E = abs(pi / m * (4 * pi * theta / T_E_mean**2 - G * R_D_mean**4 / 2 / L_D))
B_E_error = sqrt((pi / m**2 * (4 * pi * theta / T_E_mean**2 - G * R_D_mean**4 / 2 / L_D))**2 * m_error**2
               + (4 * pi**2 / m / T_E_mean**2)**2 * theta_error**2
               + (8 * pi**2 * theta / m / T_E_mean**3)**2 * T_E_error**2
               + (pi / 2 * R_D_mean**4 / m / L_D)**2 * G_error**2
               + (2 * pi * G * R_D_mean**3 / m / L_D)**2 * R_D_error**2
               + (pi / 2 * G * R_D_mean**4 / m / L_D**2)**2 * L_D_error**2)
B_E_rerror = abs2rel(B_E, B_E_error)

savetxt('build/plot/graph-points', (B_S, T_m_mean, T_m_error))
savetxt('build/plot/graph-line', [A, B])

table = r'\sisetup{table-format=2.4,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\mean{B_\t{S}}$ & \milli\tesla & ' + str(B_S_mean * 1e3) + r' \\' + '\n'
table += r'  $\mean{\frac{1}{T_\t{m}^2}}$ & \per\square\second & ' + str(T_m_2_mean) + r' \\' + '\n'
table += r'  $\mean{B_\t{S}^2}$ & \milli\tesla\squared & ' + str(B_S_2_mean * 1e6) + r' \\' + '\n'
table += r'  $\mean{\frac{B_\t{S}}{T_\t{m}^2}}$ & \milli\tesla\per\square\second & ' + str(B_S_T_m_2_mean * 1e3) + r' \\' + '\n'
table += r'  $A$ & \per\tesla\per\square\second & ' + str(A) + r' \\' + '\n'
table += r'  $\error{A}$ & \per\tesla\per\square\second & ' + str(A_error) + r' \\' + '\n'
table += r'  $B$ & \per\square\second & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(B) + r'} \\' + '\n'
table += r'  $\error{B}$ & \per\square\second & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(B_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $m$ & \ampere\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(m) + r'} \\' + '\n'
table += r'  $\error{m}$ & \ampere\square\meter & \multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(m_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $B_\t{E}$ & \micro\tesla & ' + str(B_E * 1e6) + r' \\' + '\n'
table += r'  $\error{B_\t{E}}$ & \micro\tesla & ' + str(B_E_error * 1e6) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/calc2.tex', 'w').write(table)

open('build/result/G.tex', 'w').write(r'\SI{' + '{:.0f}'.format(G * 1e-9) + ' +- ' + '{:.0f}'.format(G_error * 1e-9) + r' e9}{\newton\per\square\meter}')
open('build/result/Q.tex', 'w').write(r'\SI{' + '{:.1f}'.format(Q * 1e-11) + ' +- ' + '{:.1f}'.format(Q_error * 1e-11) + r' e11}{\newton\per\square\meter}')
open('build/result/Q_rerror.tex', 'w').write(r'\SI{' + '{:.0f}'.format(Q_rerror) + r'}{\percent}')
open('build/result/m.tex', 'w').write(r'\SI{' + '{:.1f}'.format(m * 1e3) + ' +- ' + '{:.1f}'.format(m_error * 1e3) + r' e-3}{\ampere\square\meter}')
open('build/result/B_E.tex', 'w').write(r'\SI{' + '{:.0f}'.format(B_E * 1e6) + ' +- ' + '{:.0f}'.format(B_E_error * 1e6) + r'}{\micro\tesla}')
open('build/result/B_E_rerror.tex', 'w').write(r'\SI{' + '{:.0f}'.format(B_E_rerror) + r'}{\percent}')
