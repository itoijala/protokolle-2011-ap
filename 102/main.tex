\input{header.tex}

\addbibresource{lit.bib}

\renewcommand{\ExperimentNumber}{102}

\begin{document}
  \maketitlepage{Drehschwingungen}{11.10.2011}{08.11.2011}
  \section{Kontinuumsmechanik}
    \subsection{Ziel}
      Ziel des Versuchs ist die Bestimmung des Schubmoduls $G$ und des Kompressionsmoduls $Q$ eines Metalls.
    \subsection{Theorie} 
      Mechanische Kräfte können in zwei Arten aufgeteilt werden: Volumenkräfte und Oberflächenkräfte.
      Im Folgenden geht es um Oberflächenkräfte, die wiederum in die Normalspannung $\sigma$, oder Druck $P$,  und die Tangentialspannung $\tau$ aufgeteilt werden können.
      Wirken solche Kräfte auf Körper, kommt es zu einer Gestalts- oder Volumenveränderung, die dem Hooke'schen Gesetz folgt.
      Dieses besagt, dass die Änderung solange proportional zur Kraft ist, wie sie sich im Hooke'schen Bereich befindet, also reversibel ist.
      Dies ist der Fall für kleine Kräfte und Auslenkungen.
      Das Hooke'sche Gesetz lautet für Längen- und Volumenänderungen
      \begin{eqns}
        \sigma &=& E \frac {\Delta L}{L} \\
        P &=& Q \frac {\Delta V}{V} .
      \end{eqns}%
      
      In beiden Fällen sagt es aus, dass eine Ausdehnung proportional zur Kraft ist.
      Im Allgemeinen werden die Proportionalitätsfaktoren $E$ und $Q$ durch eine symmetrische 6$\times$6-Matrix $C$ bestimmt.
      Bei isotropen Körpern gibt es aus Symmetriegründen nur zwei unabhängige elastische Konstanten.
      In diesem Versuch interessieren uns das Torsionsmodul $G$ und das Kompressionsmodul $Q$.
      $G$ beschreibt die Gestaltselastizität des Materials und $Q$ die Volumenelastizität.
      Da $Q$ in der Praxis schwer zu messen ist, werden zwei weitere Konstanten, das Elastizitätsmodul $E$ und die Poisson'sche Querkontraktionszahl $\mu$, eingeführt.

      Die Zusammenhänge zwischen $G$, $Q$, $E$ und $\mu$ sind:
      \begin{eqns}
        E &=& 2G \del{\mu + 1} \\
        E &=& 3 \del{1 - 2\mu} Q .
      \end{eqns}%
      Da im Folgenden $G$ und $E$ experimentell bestimmt werden, sind folgende Umformungen nützlich:
      \begin{eqns}
        \mu &=& \frac{E}{2G} - 1 \\
        Q &=& -\frac{EG}{3 \del{E - 3G}} .
      \end{eqns}%

      Um die elastische Nachwirkung, durch die die Deformationen verspätet, also nach dem Kraftstoß, auftreten und verschwinden, zu umgehen, braucht man ein dynamisches Messverfahren.
      Um $G$ zu bestimmen, wählt man daher ein Verfahren, bei dem ein Draht durch tangential angreifende Kräfte tordiert wird.
      Durch eine anfängliche Auslenkung wird der Draht zu Drehschwingungen veranlasst.
      Die Kräfte $\dif{F}$, die am Draht im Abstand $r$ von der Drehachse  angreifen, erzeugen Drehmomente $\dif{M}$, die die konzentrischen Schichten des Drahtes scheren.
      Das Drehmoment ist definiert als
      \begin{eqn}
        \dif{M} = r \dif{F} .
      \end{eqn}%
      Über das Hooke'sche Gesetz und durch Integration über den Radius des Drahtes erhält man eine lineare Beziehung zwischen $M$ und $\varphi$, dem Torsionswinkel:
      \begin{eqns}
        M &=& D \varphi . \\
        D &=& \frac{\pi}{2} \frac{G R_\t{D}^4}{L_\t{D}}
      \end{eqns}%
      ist die Richtgröße des Drahtes, wobei $R_\t{D}$ der Radius des Drahtes mit der Länge $L_\t{D}$ ist.
      
      Die harmonische Drehschwingung des Drahtes wird, bei Vernachlässigung der Reibung, beschrieben durch:
      \begin{eqn}
        D \varphi + \theta \ddot{\varphi} = 0 , \eqlabel{eqn:phi-dgl}
      \end{eqn}%
      wobei $\theta$ das Trägheitsmoment der Kugel mit Halterung
      \begin{eqn}
        \theta = \theta_\t{K} + \theta_\t{KH} = \int_{\SetR^3} \rho r^2 \dif{V} + \theta_\t{KH} = \frac{2}{5} m_\t{K} R_\t{K}^2 + \theta_\t{KH} .
      \end{eqn}%
      ist.
      Gleichung \eqref{eqn:phi-dgl} wird gelöst durch
      \begin{eqn}
        \varphi(t) = \varphi_0 \cos \frac{2 \pi}{T_\t{G}} t \eqlabel{eqn:phi-lösung}
      \end{eqn}%
      mit
      \begin{eqn}
        T_\t{G} = 2 \pi \sqrt{\frac{\theta}{D}} .
      \end{eqn}%
      Da die Periodendauer $T_\t{G}$ experimentell genau gemessen werden kann, erhält man einen guten Wert für $G$ durch die Formel:
      \begin{eqn}
        G = 8 \pi \frac{\theta L_\t{D}}{T_\t{G}^2 R_\t{D}^4} .
      \end{eqn}%
    \subsection{Versuchsaufbau und -durchführung}
      \label{g-aufbau}
      Eine Metallkugel mit Radius $R_\t{K}$ und Masse $m_\t{K}$ hängt an einem Torsionsdraht mit Durchmesser $2 R_\t{D}$ und Länge $L_\t{D}$ aus dem zu untersuchenden Material.
      Oben befindet sich ein Justierrad, um den Draht zu verdrehen.
      Die Kugel wird so in die Aufhängung gelegt, dass der Stabmagnet in der Kugel identisch parallel zum Draht ausgerichtet ist.
      Außerdem ist oberhalb der Kugel auf dem Draht ein Spiegel befestigt.
      Eine Lichtquelle erzeugt Licht.
      Dieses Licht, das zuvor von einem Spalt und einer Sammellinse fokussiert wurde, wird am Spiegel reflektiert und trifft anfangs auf eine Mattscheibe neben einem Lichtdetektor.
      Wenn das Licht auf den Lichtdetektor trifft, sendet dieser ein Signal an eine digitale Schaltung, die eine elektronische Uhr steuert.
      \begin{figure}
        \includegraphics[scale=0.4]{graphic/aufbau.png}
        \qquad
        \includegraphics[scale=0.4]{graphic/aufbau2.png}
        \caption{Aufbau des Versuchs \cite{anleitung102}}
        \label{fig:aufbau}
      \end{figure}%

      Die Schaltung (Abbildung \ref{fig:schaltung}) besteht im Wesentlichen aus einem Zähler, aufgebaut aus zwei getakteten Flip-Flops.
      Beim ersten Signal des Detektors springt der Ausgang $Q$, ausgelöst durch den Impuls der Lichtschranke auf den Eingang $T$, des ersten Flip-Flops von H auf L.
      Dadurch wird die Uhr gestartet.
      Das zweite Signal wird ignoriert, da $Q$ beim ersten Flip-Flop von L auf H springt und dies keinen Impuls darstellt.
      Beim dritten Signal wird die Uhr gestoppt, da der erste Flip-Flop, wie beim ersten Signal einen Impuls an die Uhr sendet.
      Das vierte Signal stellt die Uhr auf 0 zurück, da nun die monostabile Kippstufe durch den zweiten Flip-Flop ausgelöst wird.
      \begin{figure}
        \includegraphics[width=\textwidth]{pstricks/schaltung.pdf}
        \caption{Schaltung zur Steuerung der elektronischen Uhr}
        \label{fig:schaltung}
      \end{figure}%

      Zunächst wird mit einer Mikrometerschraube der Durchmesser $2R_\t{D}$ an mehreren Stellen des Drahtes bestimmt.
      Die Länge $L_\t{D}$ des Drahtes wird mit einem Maßband gemessen.

      Durch das Justierrad wird der Draht verdreht, sodass eine harmonische Schwingung entsteht.
      Das am Spiegel reflektierte Licht trifft während einer Messung vier Mal auf den Lichtsensor.
      Die Schaltung steuert die Uhr in oben beschriebener Weise.
      Zur Verdeutlichung ist Abbildung \ref{fig:aufbau} gegeben. Der Magnet in der Kugel, sowie die Helmholtz-Spulen werden erst in \ref{magnet} relevant.
    \subsection{Messwerte} 
      In den Tabellen \ref{tab:R_D}, \ref{tab:T_G} und \ref{tab:sonstige-werte} sind die Messwerte zur Bestimmung von $G$, $\mu$ und $Q$ aufgelistet.
      $N_\t{S}$, $R_\t{S}$ und $\mu_0$ sind die Anzahl der Windungen und der Radius der Helmholtzspulen aus \ref{magnet} und die magnetische Feldkonstante.
      \begin{table}
        \input{table/R_D.tex}
        \caption{Messwerte für $R_\t{D}$}
        \label{tab:R_D}
      \end{table}%
      \begin{table} 
        \input{table/T_G.tex}
        \caption{Messwerte für $T_\t{G}$}
        \label{tab:T_G}
      \end{table}%
      \begin{table}  
        \input{table/other.tex}
        \caption{Sonstige Werte}
        \label{tab:sonstige-werte}
      \end{table}%
      \FloatBarrier
    \subsection{Auswertung}
      \input{statistics.tex}%
      \input{linregress.tex}%

      Zur Fehlerbestimmung von $\theta$, $G$, $\mu$ und $Q$ sind hier nicht triviale partielle Ableitungen, sowie die endgültigen Formeln zur Fehlerfortpflanzung gegeben.
      \begin{eqns}
        \error{\theta} & = & \sqrt{\left( \frac{2}{5} R_\t{K}^2 \right)^2 \left( \error{m_\t{K}} \right)^2 + \left( \frac{4}{5} m_\t{K} R_\t{K} \right)^2 \left( \error{R_\t{K}} \right)^2} \\
        \rerror{G} & = & \sqrt{\left( \rerror{\theta} \right)^2 + \left( \rerror{L_{\t{D}}} \right)^2 + 4 \left( \rerror{T_{G}}\right)^2 + 16 \left( \rerror{R_{\t{D}}}\right)^2} \\
        \frac{\partial \mu}{\partial E} & = & \frac{1}{2 G} \\
        \frac{\partial \mu}{\partial G} & = & - \frac{E}{2 G^2} \\
        \error{\mu} & = & \sqrt{\left( \frac{1}{2 E} \right)^2 \left( \error{E} \right)^2 + \left( - \frac{E}{2 G^2} \right)^2 \left( \error{G} \right)^2} \\
        \frac{\partial Q}{\partial E} & = & \frac{G^2}{\left( E - 3 G \right)^2} \\
        \frac{\partial Q}{\partial G} & = & - \frac{E^2}{\left( E - 3 G \right)^2} \\
        \error{Q} & = & \sqrt{\left( \frac{G^2}{ \left( E - 3 G \right)^2} \right)^2 \left( \error{E} \right)^2 + \left( - \frac{E^2}{\left( E - 3 G \right)^2 } \right)^2 \left( \error{G} \right)^2}
      \end{eqns}%
      Dadurch ergeben sich die Werte in Tabelle \ref{fig:calcwert}
      \begin{table} 
        \input{table/calc1.tex}
        \caption{Berechnete Werte}
        \label{fig:calcwert}
      \end{table}%
    \subsection{Diskussion}
      \label{firstdiscuss}
      Zur Aufgabe in diesem Versuch gehörte die Bestimmung von $E$.
      Dies war wegen Ausfall der Messapparatur nicht möglich.
      Ein Literaturwert ist in Tabelle \ref{tab:sonstige-werte} angegeben.
      
      Aus den Messungen zur Bestimmung von $G$ ergibt sich ein Wert von
      \begin{eqn}
        G = \input{result/G.tex} .
      \end{eqn}%
      Bei der Bestimmung von $G$ gab es verschiedene Fehlerquellen.
      Die Pendelbewegungen der Kugel waren auf Grund der hohen Empfindlichkeit der Messapparatur und der Länge des Drahtes fast nie perfekt zu dämpfen.
      Hinzu kam, dass durch Betätigen des Justierrades der Spiegel anfing zu wackeln, was zu Lichtimpulsen führte, die die Zeitmessung störten.
      Des Weiteren war die Aufhängung der Kugelhalterung defekt. Die Fläche des die Kugel aufnehmenden Ringes war somit nicht senkrecht zum Draht.
      Dies lieferte einen allerdings zu vernachlässigenden Fehler beim Trägheitsmoment der Kugelhalterung.
      Außerdem war es sehr schwierig den Stabmagneten, auch wegen der defekten Aufhängung, exakt identisch parallel zum Draht aufzuhängen. 
      Somit waren geringe unerwünschte Einflüsse vom Erdmagnetfeld nicht auszuschließen.
     
      Für $Q$ ergibt sich ein Wert von
      \begin{eqn}
        Q = \input{result/Q.tex} .
      \end{eqn}%
      Der relativ große Fehler von $\input{result/Q_rerror.tex}$ bei $Q$ kommt wahrscheinlich daher, dass mehrmals nur fehlerbehaftete Größen in die Formel eingingen.
  \section{Magnetostatik}
    \subsection{Ziel}
      Ziel des Versuchs ist die Bestimmung des magnetischen Moments $m$ eines Stabmagneten und der horizontalen Komponente des Erdmagnetfeldes $B_\t{E}$.
    \subsection{Theorie} 
      Befindet sich ein Magnet in einem äußeren, homogenen Magnetfeld $\V{B}$, so wirken die Kräfte $F_\t{N}$ und $F_\t{S}$ auf die Pole des Magneten.
      Diese sind entgegengesetzt, entlang der Feldlinien des äußeren Feldes gerichtet.
      Zwar ergibt sich keine resultierende Kraft auf den Magneten, allerdings wird ein Drehmoment $\V{M}$ durch den Abstand der Angriffspunkte der Kräfte vom Mittelpunkt des Magneten erzeugt, das wie folgt lautet:
      \begin{eqn}
        \V{M} = \V{m} \times \V{B} ,
      \end{eqn}%
      beziehungsweise
      \begin{eqn}
        M = m B \sin \gamma
      \end{eqn}%
      mit dem Winkel $\gamma$ zwischen Feldlinien und Dipolachse.
      
      Nutzt man diese Formel aus, so lässt sich $m$ mit Hilfe des Aufbaus aus \ref{g-aufbau} bestimmen.
      Durch das homogene Magnetfeld, dessen Feldlinien parallel zum Stabmagneten laufen, wird aus \eqref{eqn:phi-dgl}
      \begin{eqn}
        m B \sin \varphi + D \varphi + \theta \ddot{\varphi} = 0 . \eqlabel{eqn:phi-dgl-2}
      \end{eqn}%
      Für kleine Auslenkungen, für die gilt
      \begin{eqn}
        \sin \varphi = \varphi + \dots ,
      \end{eqn}%
      geht \eqref{eqn:phi-dgl-2} in
      \begin{eqn}
        \left( m B + D \right) \varphi + \theta \ddot{\varphi} = 0
      \end{eqn}%
      über.
      Die Lösung lautet nun wie in \eqref{eqn:phi-lösung}, aber mit
      \begin{eqn}
        T_\t{m} = 2 \pi \sqrt{\frac{\theta}{m B + D}} .
      \end{eqn}%

      Für Helmholtzspulen, die für das homogene Magnetfeld verwendet werden, gilt: 
      \begin{eqns}
        \V{B}_\t{S}(x) &=& \frac{\mu_0}{2} N_\t{S} I_\t{S} R_\t{S}^2 \del{\frac{1}{\sqrt{\del{\frac{R_\t{S}}{2} + x}^2 + R_\t{S}^2}^3} + \frac{1}{\sqrt{\del{\frac{R_\t{S}}{2} - x}^2 + R_\t{S}^2}^3}} \UnitV{e}_x \\
        \V{B}_\t{S}(0) &=& \frac{8 \mu_0}{\sqrt{5}^3} \frac{N_\t{S} I_\t{S}}{R_\t{S}} \UnitV{e}_x .
      \end{eqns}%
      Aus den obigen Überlegungen ergibt sich die Formel für $m$:
      \begin{eqns}
        m &=& \frac{\pi}{B_\t{S}} \left( 4 \pi \frac{\theta}{T_\t{m}^2} - \frac{G R_\t{D}^4}{2 L_\t{D}} \right) \\
        m &=& \frac{\sqrt{5}^3 \pi}{8 \mu_0} \frac{R_\t{S}}{N_\t{S} I_\t{S}} \left( 4 \pi \frac{\theta}{T_\t{m}^2} - \frac{G R_\t{D}^4}{2 L_\t{D}} \right) .
      \end{eqns}%
      $m$ kann aus den experimentell gewonnenen Daten durch
      \begin{eqn}
        \frac{1}{T_\t{m}^2} = \frac{1}{4 \pi^2} \frac{m}{\theta} B_\t{S} - \frac{1}{8 \pi} \frac{G R_\t{D}^4}{\theta L_\t{D}}
      \end{eqn}%
      bestimmt werden.
      Mit diesem Verfahren kann auch die horizontale Komponenete des Erdmagnetfeldes gemessen werden.
      Hierzu wird statt der Helmholtzspulen das Erdmagnetfeld verwendet und der Stabmagnet in Nord-Süd-Richtung ausgerichtet, wobei jetzt das magnetische Moment des Stabmagneten bekannt ist.
      Es ergibt sich:
      \begin{eqn}
        B_\t{E} = \frac{\pi}{m} \left( 4 \pi \frac{\theta}{T_\t{E}^2} - \frac{G R_\t{D}^4}{2 L_\t{D}} \right) .
      \end{eqn}%
    \subsection{Versuchsaufbau und -durchführung}
    \label{magnet}
      \subsubsection{Aufbau und Durchführung zur Bestimmung von $m$}
        Zu dem Aufbau aus \ref{g-aufbau} werden Helmholtzspulen ergänzt, die ein homogenes Magnetfeld erzeugen.
        Die Kugel wird so ausgerichtet, dass der Stabmagnet in der Kugel entlang der Feldlinien der Spulen gerichtet ist.
        Die Schwingungsdauer wird bei verschiedenen Stärken des Magnetfelds gemessen.
      \subsubsection{Aufbau und Durchführung zur Bestimmung von $B_\t{E}$}
        Der Aufbau aus \ref{g-aufbau} wird so angepasst, das der Stabmagnet in der Kugel in Nord-Süd-Richtung gerichtet ist.
        Dann wird die Schwingungsdauer wieder gemessen.
    \subsection{Messwerte}
      In Tabelle \ref{fig: Tm} und Tabelle \ref{fig: Te} sind die ermittelten Messwerte zur späteren Bestimmung des magnetischen Momentes $m$ und der horizontalen Erdmagnetfeldkomponente $B_{\t{E}}$ gegeben. 
      \begin{table} 
        \input{table/T_E.tex}
        \caption{Messwerte für $T_\t{E}$}
        \label{fig: Te}
      \end{table}%
      \begin{landscape}
        \begin{table}
          \input{table/T_m.tex}
          \caption{Messwerte für $T_\t{m}$}
          \label{fig: Tm}
        \end{table}%
      \end{landscape}
    \subsection{Auswertung}
      Um das magnetische Moment des Stabmagneten zu bestimmen trägt man wie in Abbildung \ref{fig:graph} $\frac{1}{T^2_{\t{m}}}$ gegen $B_\t{S}$ auf.
      Mit Hilfe der Formel \eqref{eqn:linregress} gewinnt man aus der Steigung der Regressionsgeraden das magnetische Moment.
      \begin{eqns}
        \frac{1}{T_\t{m}^2} &=& A B_\t{S} + B \\
        A &=& \frac{\mean{\frac{B_\t{S}}{T_\t{m}^2}} - \mean{B_\t{S}} \mean{\frac{1}{T_\t{m}^2}}}{\mean{B_\t{S}^2} - \mean{B_\t{S}}^2} \\
        B &=& \frac{\mean{\frac{1}{T_\t{m}^2}} \mean{B_\t{S}^2} - \mean{\frac{B_\t{S}}{T_\t{m}^2}} \mean{B_\t{S}}}{\mean{B_\t{S}^2} - \mean{B_\t{S}}^2} \\
        m &=& 4 \pi^2 \theta A \\
        \rerror{m} &=& \sqrt{\left( \rerror{\theta} \right)^2 + \left( \rerror{A} \right)^2}
      \end{eqns}%
      Zur Bestimmung des absoluten Fehlers der horizontalen Komponenten des Erdmagnetfeldes sind im Folgenden die partiellen Ableitungen und die endgültige Formel angegeben.
      \begin{eqns}
        \frac{\partial B_\t{E}}{\partial m} & = & - \frac{\pi}{m^2} \left( 4 \pi \frac{\theta}{T_\t{E}^2} - \frac{G R_\t{D}^4}{2 L_\t{D}} \right) \\
        \frac{\partial B_\t{E}}{\partial \theta} & = & \frac{4 \pi^2}{m T_\t{E}^2} \\
        \frac{\partial B_\t{E}}{\partial T_\t{E}} & = & - 8 \pi^2 \frac{\theta}{m T_\t{E}^3} \\
        \frac{\partial B_\t{E}}{\partial G} & = & - \frac{\pi}{2} \frac{R_\t{D}^4}{m L_\t{D}} \\
        \frac{\partial B_\t{E}}{\partial R_\t{D}} & = & - 2 \pi \frac{G R_\t{D}^3}{m L_\t{D}} \\
        \frac{\partial B_\t{E}}{\partial L_\t{D}} & = & \frac{\pi}{2} \frac{G R_\t{D}^4}{m L_\t{D}^2} \\
        \error{B_\t{E}} & = & \left( \left( - \frac{\pi}{m^2} \left( 4 \pi \frac{\theta}{T_\t{E}^2} - \frac{G R_\t{D}^4}{2 L_\t{D}} \right) \right)^2 \left( \error{m} \right)^2
        + \left( \frac{4 \pi^2}{m T_\t{E}^2} \right)^2 \left( \error{\theta} \right)^2 \right. \\
        && \left. + \left( - 8 \pi^2 \frac{\theta}{m T_\t{E}^3} \right)^2 \left( \error{T_\t{E}} \right)^2
        + \left( - \frac{\pi}{2} \frac{R_\t{D}^4}{m L_\t{D}} \right)^2 \left( \error{G} \right)^2 \right. \\
        && \left. + \left( - 2 \pi \frac{G R_\t{D}^3}{m L_\t{D}} \right)^2 \left( \error{R_\t{D}} \right)^2
        + \left( \frac{\pi}{2} \frac{G R_\t{D}^4}{m L_\t{D}^2} \right)^2 \left( \error{L_\t{D}} \right)^2 \right)^\frac{1}{2}
      \end{eqns}%
      \begin{figure}
        \includesgraphics{graph/graph.pdf}
        \caption{Lineare Regression von $\frac{1}{T_\t{m}^2}$ gegen $B_\t{S}$}
        \label{fig:graph}
      \end{figure}%
      Die restlichen berechneten Werte der linearen Regression, für das magnetische Moment $m$ des Stabmagneten und das Erdmagnetfeld $B_{\t{E}}$ stehen in Tabelle \ref{fig:calcmBe}.
      \begin{table}
        \input{table/calc2.tex}
        \caption{Berechnete Werte}
       \label{fig:calcmBe}
      \end{table}%
    \subsection{Diskussion}
      Für das magnetische Moment errechnet sich ein Wert von 
      \begin{eqn}
        m = \input{result/m.tex} .
      \end{eqn}%
      Bei der Bestimmung des magnetischen Moments war es schwierig den Stabmagneten in seiner Ruhelage exakt parallel zu den magnetischen Feldlinien auszurichten. 
      Hinzu kamen schon bekannte Probleme aus \ref{firstdiscuss}.

      Für die Horizontalkomponente des Erdmagnetfelds ergab sich ein Wert von 
      \begin{eqn}
        B_\t{E} = \input{result/B_E.tex} .
      \end{eqn}%
      Das Verfahren zur Bestimmung des Erdmagnetfeldes ist ziemlich ungenau und ergibt einen relativen Fehler von $\input{result/B_E_rerror.tex}$.
      Zu den bereits aufgelisteten Gründen zur Ungenauigkeit bei der Zeitmessung kommt die nicht exakte Ausrichtung des Stabmagneten in Nord-Süd-Richtung hinzu.
      Ein Literaturwert für die horizontale Komponente des Erdmagnetfeldes in Deutschland ist ungefähr \SI{20}{\micro\tesla} \cite{magnetic-field-horizontal}.
      Daraus folgt, dass trotz der ungenauen Messung unter Berücksichtigung des absoluten Fehlers ein richtiger Wert gemessen wird. 
  \makebibliography
\end{document}
