\input{header.tex}

\renewcommand{\ExperimentNumber}{353}

\begin{document}
  \maketitlepage{Das Relaxationsverhalten eines RC-Kreises}{08.11.2011}{29.11.2011}
  \section{Ziel}
    Ziel des Versuches ist die Bestimmung der Zeitkonstante eines RC-Kreises mit Hilfe verschiedener Messansätze.
    Des Weiteren soll experimentell bestätigt werden, dass in einem angeregten RC-Kreis die Spannung am Kondensator für hohe Frequenzen die Generatorspannung integriert.
  \section{Theorie}
    Kehrt ein System nach einer Auslenkung ohne Oszillationen in seinen Ruhezustand $A(\infty)$ zurück, so kommt es dabei zu Relaxationserscheinungen.
    Wenn man die Größe $A$ betrachtet, gilt
    \begin{eqn}
      \od{A}{t} = c \left( A(t) - A(\infty) \right)
    \end{eqn}%
    mit einer Proportionalitätskonstante $c$.
    Nach Separation der Variablen ergibt sich für  die Lösung der Differentialgleichung
    \begin{eqn}
      A(t) = A(\infty) + \left( A(0) - A(\infty) \right) e^{ct} .
    \end{eqn}%

    \begin{figure}
      \includegraphics{pstricks/1.pdf}
      \caption{Entladung (Stellung 1) und Aufladung (Stellung 2) eines Kondensators über einen Widerstand}
      \label{fig:circuit-theorie-entladung}
    \end{figure}%
    Die Ent- und Aufladevorgänge eines Kondensators über einen Widerstand (Abbildung \ref{fig:circuit-theorie-entladung}) zeigen ein Relaxationsverhalten.
    Für die Ladung $Q(t)$ auf den Platten des Kondensators mit Kapazität $C$, die sich über den Widerstand $R$ entlädt,  lässt sich die Differentialgleichung
    \begin{eqn}
      \dot{Q} + \frac{1}{R C} Q = 0
    \end{eqn}%
    aufstellen.
    Die Gleichung wird durch
    \begin{eqn}
      Q(t) = Q(0) e^{- \frac{1}{R C} t}
    \end{eqn}%
    gelöst.
    Beim Aufladevorgang gilt die Gleichung
    \begin{eqn}
      Q(t) = C U_0 (1 - e^{- \frac{1}{R C} t}) .
    \end{eqn}%
    Die Größe $R C$ wird Zeitkonstante des Relaxationsvorgangs genannt.

    \begin{figure}
      \includegraphics{pstricks/2.pdf}
      \caption{RC-Kreis mit Anregung}
      \label{fig:circuit-anregung}
    \end{figure}%
    Ein RC-Kreis, der durch eine Sinusspannung angeregt wird, zeigt auch ein Relaxationsverhalten.
    Ist die Anregungsfrequenz $\omega \ll \frac{1}{R C}$, so ist die Spannung $U_\t{C} (t)$ am Kondensator fast simultan zu der Anregungsspannung
    \begin{eqn}
      U(t) = U_0 \cos(\omega t) .
    \end{eqn}%
    Wird $\omega$ größer, so entsteht eine frequenzabhängige Phasenverschiebung $\varphi(\omega)$ zwischen $U_\t{C}$ und $U$.
    Aus der Maschenregel folgt für die Schaltung in Abbildung \ref{fig:circuit-anregung}
    \begin{eqns}
      U & = & U_\t{R} + U_\t{C} \\
      U_0 \cos(\omega t) & = & R C \dot{U}_\t{C} + U_\t{C} . \eqlabel{eqn:anregung-dgl}
    \end{eqns}%
    Die Gleichung wird durch den Ansatz
    \begin{eqn}
      U_\t{C} (t) = A(\omega) \cos(\omega t + \varphi(\omega))
    \end{eqn}%
    gelöst.
    Für $\varphi$ folgt
    \begin{eqn}
      \varphi(\omega) = \arctan(- \omega R C) \eqlabel{eqn:theorie_phi}
    \end{eqn}%
    und für $A$
    \begin{eqn}
      A(\omega) = \frac{U_0}{\sqrt{1 + \omega^2 R^2 C^2}}. \eqlabel{eqn:theorie_A}
    \end{eqn}%
    Aus \eqref{eqn:theorie_phi} und \eqref{eqn:theorie_A} folgt für den Zusammenhang zwischen $\varphi$ und $A$
    \begin{eqn}
      \frac{A}{U_0} = \cos \varphi .
    \end{eqn}%

    Der RC-Kreis kann unter bestimmten Voraussetzungen auch als Integrator verwendet werden.
    Geht man von $\omega \gg \frac{1}{R C}$ aus, so gilt $|U_\t{C}| \ll |U|$ und \eqref{eqn:anregung-dgl} wird zu
    \begin{eqn}
      U(t) = R C \dot{U}_\t{C} .
    \end{eqn}%
    Integriert man beide Seiten, folgt
    \begin{eqn}
      U_\t{C} (t) = \frac{1}{R C} \int_0^t U(t') \dif{t'} .
    \end{eqn}%
  \section{Versuchsaufbau und -durchführung}
    \subsection{Bestimmung von $RC$ durch Beobachtung der Entladekurve des Kondensators}
      \label{aufbau-entladung}
      \begin{figure}
        \includegraphics{pstricks/3.pdf}
        \caption{Versuchsaufbau aus \ref{aufbau-entladung}}
        \label{fig:circuit-entladung}
      \end{figure}%
      Zunächst wird ein Schaltkreis gemäß Abbildung \ref{fig:circuit-entladung} aufgebaut.
      Während der Messung beobachtet man die Spanning $U_\t{C}$ in Abhängigkeit von der Zeit.
      Entsprechend der angelegten Rechteckspannung kommt es zu Auf- und Entladevorgängen am Kondensator.
      Bei hoher Frequenz der Rechteckspannung werden diese Prozesse nicht ganz abgeschlossen sondern gehen fließend ineinander über.
      Der Aufladevorgang verhält sich bei der Bestimmung von $RC$ zum Entladevorgang analog.
      Im Folgenden wird daher nur der Entladevorgang betrachtet.
     
      Da die Minimalspannung $U_{\t{C},0}$ für die Auswertung wichtig ist, wird zunächst eine niedrige Frequenz $\omega$ gewählt, sodass die niedrigste in vernünftiger Zeit am Kondensator auftretende Spannung ermittelt werden kann.
      Die Ablenkgeschwindigkeit auf der Zeitachse und der Triggerpegel am Oszillographen werden so eingestellt, dass der gesamte Entladevorgang am Bildschirm sichtbar wird; diese Werte werden gespeichert.
      Anschließend erhöht man die Frequenz und stellt Trigger und Ablenkgeschwindigkeit auf der Zeitachse so ein, dass die Kondensatorspannung sich am Bildschirm um den Faktor 5 bis 10 ändert.
      Sind alle Paramter richtig eingestellt, werden diese Messdaten ebenfalls gespeichert.
    \subsection{Bestimmung von $RC$ aus der Frequenzabhängigkeit der Amplitude und Phasenverschiebung bei angelegter Sinusspannung} 
      \label{aufbau-A-phi}
      \begin{figure}
        \includegraphics{pstricks/4.pdf}
        \caption{Versuchsaufbau aus \ref{aufbau-A-phi}}
        \label{fig:circuit-A-phi}
      \end{figure}%
      Zu Beginn wird durch Betrachten des Oszilloskopbildes festgestellt, dass die Generatorspannung unabhängig von der Frequenz ist. 
      Mit Hilfe der Schaltung aus Abbildung \ref{fig:circuit-A-phi} wird dann die Spannungsamplitude $A$ am Kondensator mit einem Millivoltmeter und die Phasenverschiebung $\varphi$ mit Hilfe des Oszillographen und folgender Formel in Abhängigkeit von der Frequenz $\omega$ ermittelt:
      \begin{eqn}
        \varphi = a \omega \eqlabel{eqn:phase} .
      \end{eqn}%
      $a$ ist hierbei die (negative) zeitliche Differenz der Nulldurchgänge der Generatorspannung und der Spannung am Kondensator.
      Dabei ist zu beachten, dass die beiden Spannungskurven stets symmetrisch zu $x$-Achse sind, da sonst die Nulldurchgänge verfälscht werden.
      Die Messung wird über 3 Zehnerpotenzen hinweg durchgeführt.
    \subsection{Der RC-Kreis als Integrator}
      \label{aufbau-integral}
      Die Integration der Generatorspannung am Kondensator kann mit der Schaltung aus Abbildung \ref{fig:circuit-A-phi} realisiert werden.
      Die Zeitablenkung, der Trigger und die $y$-Ablenkung am Oszillographen werden so eingestellt, dass die Spannungsverläufe übereinander dargestellt werden.
      Die Messung wird für eine Rechteckspannung, eine Sinusspannung und eine Dreiecksspannung durchgeführt und die Messdaten gespeichert.
  \section{Messwerte}
    Die Messwerte für $A$ und $a$ in Abhängigkeit von der Frequenz $\nu = \frac{\omega}{2 \pi}$ sind in Tabelle \ref{tab:a-data} aufgeführt. 
    Die Messwerte aus \ref{aufbau-entladung} und \ref{aufbau-integral} befinden sich elektronischer Form im Anhang.
    \begin{table}
      \input{table/a-data.tex}
      \caption{Amplitude $A$ und Zeitdifferenz $a$ in Abhängigkeit von der Frequenz~$\nu$}
      \label{tab:a-data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    \input{linregress.tex}%
    \subsection{Entladevorgang am Kondensator}
      Die Spannung $U_{\t{C} , 0}$, also die Spannung, die sich nach sehr langer Zeit bei der Entladung einstellt, wird aus einer Messung mit sehr niedriger Frequenz bestimmt und steht in Tabelle \ref{tab:calc}.
      Die gespeicherten Messwerte sind in Abbildung \ref{fig:graph-U_C} geplottet.
      Für die lineare Ausgleichsrechnung ergeben sich mit
      \begin{eqn}
        \ln \left( \frac{U_\t{C} - U_{\t{C} , 0}}{U_{\t{C} , 0}} \right) = D t + E
      \end{eqn}%
      und den Formeln \eqref{eqn:linregress-A} bis \eqref{eqn:linregress-B_error} die Parameter $D$ und $E$ sowie deren Fehler, aufgeführt in Tabelle \ref{tab:calc}.
      \begin{figure}
        \includesgraphics{graph/U_C.pdf}
        \caption{Regressionsgerade zur Ermittlung von $RC_\t{ent}$}
        \label{fig:graph-U_C}
      \end{figure}%
      Die sich daraus ergebende Regressionsgerade ist ebenfalls in Abbildung \ref{fig:graph-U_C} aufgetragen.
      Aus der Steigung der Regressionsgeraden ergibt sich für
      \begin{eqn}
        RC_\t{ent} := (R + R_\t{i}) C = \input{result/RC_ent.tex}
      \end{eqn}%
      mit dem Innenwiderstand des Generators
      \begin{eqn}
        R_\t{i} = \input{result/R_i.tex} ,
      \end{eqn}%
      der nicht herausgerechnet werden kann, da $R$ und $C$ nicht einzeln bekannt sind.
    \subsection{Bestimmung aus Amplitude und Phasenverschiebung der Kondensatorspannung}
      Die Messwerte für die Amplitude in Abhängigkeit von der Frequenz werden in Abbildung \ref{fig:graph-A} geplottet, die für die Phase in Abbildung \ref{fig:graph-phi}.
      Für die nicht-lineare Ausgleichrechnung verwendet man als Fitfunktion für die Amplitude Gleichung \eqref{eqn:theorie_A}; für die Phase verwendet man Gleichung \eqref{eqn:theorie_phi}.
      Dabei variert man die Konstanten $(R + R_\t{i})C$ und erhält bei der Amplitude (vgl. Tabelle \ref{tab:calc}) einen Wert von
      \begin{eqn}
        RC_A = \input{result/RC_A.tex}
      \end{eqn}%
      und bei der Phase einen Wert von
      \begin{eqn}
        RC_\varphi = \input{result/RC_phi.tex} .
      \end{eqn}%
      $U_0$ musste ebenfalls durch die nicht lineare Ausgleichrechnung genähert werden, da die Amplitude nicht bei $\omega = 0$ gemessen wurde; der genäherte Wert ist in \ref{tab:calc} angegeben.
      Daher ist in Abbildung \ref{fig:graph-A} nur die Amplitude und nicht der Quotient $\frac{A}{U_0}$ dargestellt.
      Die Abhängigkeit der Relativamplitude von der Phasenverschiebung ist in Abbildung \ref{fig:graph-polar} dargestellt. 

      Alle drei Werte für $RC$ liegen im selben Bereich um $\SI{1.4}{\milli\second}$.
      Die Messung über die Entladekurve ergab den kleinsten Fehler, die über die Phasenverschiebung den größten.
      Dies könnte damit erklärt werden, dass die letzten Messwerte von $\varphi$ größere Abweichungen von der Theoriekurve aufweisen.
      Die Messung von $RC$ mittels der Entladekurve ist die einfachste und schnellste der drei Messmethoden.
      Sie ergibt auch den genausten Wert für $RC$.
      \begin{figure}
        \includesgraphics{graph/A.pdf}
        \caption{Fit der Amplitude $A$ zur Ermittlung von $RC_A$}
        \label{fig:graph-A}
      \end{figure}%
      \begin{figure}
        \includesgraphics{graph/phi.pdf}
        \caption{Fit der Phase $\varphi$ am Kondensator zur Ermittlung von $RC_\varphi$}
        \label{fig:graph-phi}
      \end{figure}%
      \begin{figure}
        \includesgraphics{graph/polar.pdf}
        \caption{Abhängigkeit der Relativamplitude $\frac{A}{U_0}$ von der Phasenverschiebung $\varphi$}
        \label{fig:graph-polar}
      \end{figure}%
      \begin{table}
        \input{table/calc.tex}
        \caption{Ermittelte Parameter für die Regressionsgerade, der genäherte Wert für $U_0$, sowie die jeweils ermittelten Zeitkonstanten $RC$}
        \label{tab:calc}
      \end{table}%
    \subsection{Der RC-Kreis als Integrator}
      In den Abbildungen \ref{fig:graph-U_rechteck} bis \ref{fig:graph-U_dreieck} sind die Kondensatorspannungen bei \mbox{rechteck-,} sinus- und dreieckförmigen Generatorspannungen dargestellt.
      Die Rechteckspannung am Generator führt zu einer Dreieckspannung am Kondensator mit Extrema an den unstetigen Stellen der Generatorspannung.
      Bei der Sinusspannung ergibt sich eine Kosinusspannung am Kondensator.
      Die Dreieckspannung am Generator erzeugt einen Parabelähnlichen Spannungsverlauf am Kondensator.
      Alle diese Ergebnisse waren zu erwarten.
      Die Integrationen der Rechteck- und Sinusspannungen sind trivial.
      Die Integration der linearen Steigung der Dreieckspannung ergibt einen Parabelverlauf.
      \begin{figure}
        \includesgraphics{graph/U_rechteck.pdf}
        \caption{Rechteckgeneratorspannung und integrierte Kondensatorspannung}
        \label{fig:graph-U_rechteck}
      \end{figure}%
      \begin{figure}
        \includesgraphics{graph/U_sin.pdf}
        \caption{Sinusgeneratorspannung und integrierte Kondensatorspannung}
        \label{fig:graph-U_sin}
      \end{figure}%
      \begin{figure}
        \includesgraphics{graph/U_dreieck.pdf}
        \caption{Dreiecksgeneratorspannung und integrierte Kondensatorspannung}
        \label{fig:graph-U_dreieck}
      \end{figure}%
  \makebibliography
\end{document}
