#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

U_C_0 = loadtxt('data/U_C_0')
t, U_C = loadtxt('data/U_C', unpack=True)
U_C = U_C - U_C_0

R_i = loadtxt('data/R_i')

D, E, D_error, E_error = linregress_error(t, nominal_values(log(U_C)))
D_rerror = abs2rel(D, D_error)
E_rerror = abs2rel(E, E_error)

RC_ent = - 1 / D
RC_ent_rerror = D_rerror
RC_ent_error = rel2abs(RC_ent, RC_ent_rerror)

savetxt('build/plot/U_C-points', (t, U_C))
savetxt('build/plot/U_C-line', (- U_C_0, D, E))

open('build/result/RC_ent.tex', 'w').write(r'\SI{' + '{:.4f}'.format(RC_ent * 1e3) + '+-' + '{:.4f}'.format(RC_ent_error * 1e3) + r'}{\milli\second}')
open('build/result/R_i.tex', 'w').write(r'\SI{' + '{:.0f}'.format(float(R_i)) + r'}{\ohm}')

nu, A, a = loadtxt('data/a', unpack=True)
a *= -1e-3
omega = 2 * pi * nu

def A_calc(omega, U_0, RC):
  return nominal_values(U_0 / sqrt(1 + omega**2 * RC**2))

popt, pcov = curve_fit(A_calc, omega, A)
U_0 = popt[0]
U_0_error = sqrt(pcov[0][0])
U_0_rerror = abs2rel(U_0, U_0_error)
RC_A = popt[1]
RC_A_error = sqrt(pcov[1][1])
RC_A_rerror = abs2rel(RC_A, RC_A_error)

savetxt('build/plot/A-points', (omega, A))
savetxt('build/plot/A-line', (U_0, RC_A))

open('build/result/RC_A.tex', 'w').write(r'\SI{' + '{:.3f}'.format(RC_A * 1e3) + '+-' + '{:.3f}'.format(RC_A_error * 1e3) + r'}{\milli\second}')

table = r'\sisetup{table-format=1.5,round-mode=figures,round-precision=4}' + '\n'
table += r'\begin{tabular}{S[table-format=5.0,round-mode=off] S S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$\nu / \si{\hertz}$} & \multicolumn{1}{c}{$A / \si{\volt}$} & \multicolumn{1}{c}{$- a / \si{\milli\second}$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(nu)):
  table+='  ' + '{:.0f}'.format(nu[i]) + ' & ' + str(A[i]) + ' & ' + str(a[i] * -1e3) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/a-data.tex', 'w').write(table)

def phi_calc(omega, RC):
  return nominal_values(arctan(- omega * RC))

popt, pcov = curve_fit(phi_calc, omega, a * omega)
RC_phi = popt[0]
RC_phi_error = sqrt(pcov[0][0])
RC_phi_rerror = abs2rel(RC_phi, RC_phi_error)

savetxt('build/plot/phi-points', (omega, a * omega))
savetxt('build/plot/phi-line', (RC_phi,))

open('build/result/RC_phi.tex', 'w').write(r'\SI{' + '{:.2f}'.format(RC_phi * 1e3) + '+-' + '{:.2f}'.format(RC_phi_error * 1e3) + r'}{\milli\second}')

table = r'\sisetup{table-format=+1.4,round-mode=figures,round-precision=4}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $U_{\t{C} , 0}$ & \volt & ' + str(U_C_0) + r' \\' + '\n'
table += r'  $D$ & \per\milli\second & ' + '{:.5f}'.format(D * 1e-3) + r' \\' + '\n'
table += r'  $\error{D}$ & \per\milli\second & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(D_error * 1e-3) + r'} \\' + '\n'
table += r'  $E$ & & \multicolumn{1}{S[round-mode=places]}{' + '{:.5f}'.format(E) + r'} \\' + '\n'
table += r'  $\error{E}$ & & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(E_error) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $RC_\t{ent}$ & \milli\second & \multicolumn{1}{S[round-mode=places,round-precision=4]}{' + '{:.5f}'.format(RC_ent * 1e3) + r'} \\' + '\n'
table += r'  $\error{RC_\t{ent}}$ & \milli\second & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(RC_ent_error * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $U_0$ & \volt & ' + '{:.5f}'.format(U_0) + r' \\' + '\n'
table += r'  $\error{U_0}$ & \volt & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(float(U_0_error)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $RC_A$ & \milli\second & \multicolumn{1}{S[round-mode=places,round-precision=3]}{' + '{:.5f}'.format(RC_A * 1e3) + r'} \\' + '\n'
table += r'  $\error{RC_A}$ & \milli\second & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(RC_A_error * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $RC_\varphi$ & \milli\second & \multicolumn{1}{S[round-mode=places,round-precision=2]}{' + '{:.5f}'.format(RC_phi * 1e3) + r'} \\' + '\n'
table += r'  $\error{RC_\varphi}$ & \milli\second & \multicolumn{1}{S[round-precision=1]}{' + '{:.5f}'.format(RC_phi_error * 1e3) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/calc.tex', 'w').write(table)
