\input{header.tex}

\renewcommand{\ExperimentNumber}{103}

\begin{document}
  \maketitlepage{Biegung elastischer Stäbe}{13.12.2011}{20.12.2011}
  \section{Ziel}
    Das Ziel des Versuchs ist die Bestimmung des Elastizitätsmoduls eines Metalls mittels der Biegung eines Stabs.
  \section{Theorie}\label{theorie}
    Eine Spannung, also eine Oberflächenkraft, kann die Gestalt oder das Volumen eines Körpers verändern.
    Die Spannung kann in die Komponenten parallel und senkrecht zur Oberfläche aufgeteilt werden; sie werden als Tangentialspannung $\tau$ respektive Normalspannung $\sigma$ bezeichnet.

    Bei kleinen relativen Änderungen $\frac{\Delta L}{L}$ gilt das Hooke'sche Gesetz
    \begin{eqn}
      \sigma = E \frac{\Delta L}{L} \eqlabel{eqn:hooke}
    \end{eqn}%
    mit dem materialabhängigen Elastizitätsmodul $E$ als Proportionalitätsfaktor.
    Ein Messverfahren zur Bestimmung von $E$ ist die Biegung eines Stabes, da bei dieser Konstruktion relativ große messbare Änderungen von kleinen Kräften hervorgerufen werden.

    Eine stabförmige Probe der Länge $L$ wird gebogen, wenn sie an einem Ende fest eingespannt und am anderen Ende ein Gewicht der Masse $m_\t{G}$ gehangen wird, das die Kraft $F = m_\t{G} g$ mit der Erdbeschleunigung $g$ hervorruft.
    Aus einer Messung der Biegung $\Fun{D}{x}$ an der Stelle $x$ relativ zu der Biegung ohne Gewicht lässt sich $E$ berechnen.
    Bei der Biegung entsteht ein Drehmomentgleichgewicht, da die Probe gemäß \eqref{eqn:hooke} eine Gegenkraft aufbringt, um in seine Ruhelage zurückzukehren.

    Zur Berechnung von $\Fun{D}{x}$ wird der Querschnitt des Stabs $Q$, der den Abstand $x$ zur Einspannung hat, betrachtet.
    Das Gewicht bewirkt das Drehmoment
    \begin{eqn}
      M_F = F \del{L - x}
    \end{eqn}%
    im Abstand $x$ von der Einspannung.
    In den oberen Schichten des Stabs, die gedehnt werden, entsteht eine Zugspannung und in den unteren Schichten, die gestaucht werden, eine Druckspannung.
    In der Mitte befindet sich die neutrale Faser, deren Länge durch die Biegung nicht verändert wird.
    Die Gegenkraft bewirkt im Querschnitt $Q$ das Drehmoment
    \begin{eqn}
      M_\sigma = \Int{y \Fun{\sigma}{y}}{Q,Q},
    \end{eqn}%
    wobei $y$ der Abstand zur neutralen Faser ist.
    Im Gleichgewicht gilt
    \begin{eqns}
      M_\sigma &=& M_F , \\
      \itext{also}
      \Int{y \Fun{\sigma}{y}}{Q,Q} &=& F \del{L - x} . \eqlabel{gleichgewicht}
    \end{eqns}%
    Nach \eqref{eqn:hooke} gilt für $\sigma$
    \begin{eqns}
      \Fun{\sigma}{y} &=& E \frac{\delta x}{\Delta x} , \\
      \itext{wobei $\delta x$ die Längenänderung der Faser der Länge $\Delta x$ ist. Mit dem Krümmungsradius $R$ des Stabes gilt}
      \Fun{\sigma}{y} &=& E \frac{y \frac{\Delta x}{R}}{\Delta x} \\
      &=& E \frac{y}{R} .
    \end{eqns}%
    Für große $R$ gilt
    \begin{eqns}
      \frac{1}{R} &\approx& \D[2]{D}{x} , \\
      \itext{falls}
      \D[2]{D}{x} &\ll& 1 , \\
      \itext{woraus}
      E \D[2]{D}{x} \Int{y^2}{Q,Q} &=& F \del{L - x}
    \end{eqns}%
    folgt.
    Mit der Definition
    \begin{eqns}
      I &\coloneqq& \Int{y^2}{Q,Q} \\
      \itext{folgt nach Integration}
      \Fun{D}{x} &=& \frac{m_\t{G} g}{2 I E} \del{- \frac{1}{3} x^3 + L x^2} , \\
      \itext{wobei}
      \Fun{D}{0} &=& \Fun{\D{D}{x}}{0} = 0 .
    \end{eqns}%
    $E$ kann also aus Messwerten von $x$ und $D$ durch eine lineare Regression bestimmt werden.
    Mit
    \begin{eqns}
      \xi &\coloneqq& - \frac{1}{3} x^3 + L x^2 \\
      \itext{gilt}
      \Fun{D}{\xi} &=& A \xi , \\
      \itext{also folgt für $E$}
      E &=& \frac{m_\t{G} g}{2 I A} .
    \end{eqns}%

    Der Versuch kann auch mit einem Stab durchgeführt werden, der an beiden Enden auf einer Auflage platziert und an deren Mitte ein Gewicht gehängt wird.
    Jetzt gilt
    \begin{eqns}
      M_F &=& - \frac{F}{2} \cdot
      \begin{case}[l'rCcCl]
        x , & 0 &\leq& x &\leq& \frac{L}{2} \\
        L - x , & \frac{L}{2} &\leq& x &\leq& L%
      \end{case} , \\
      \itext{also wird aus \eqref{gleichgewicht}}
      \D[2]{D}{x} &=& - \frac{F}{2 I E} \cdot
      \begin{case}[l'rCcCl]
        x , & 0 &\leq& x &\leq& \frac{L}{2} \\
        L - x , & \frac{L}{2} &\leq& x &\leq& L%
      \end{case} , \\
      \itext{woraus mit den Bedingungen}
      \Fun{D}{0} &=& \Fun{D}{L} = \Fun{\D{D}{x}}{\frac{L}{2}} = 0 \\
      \itext{die Gleichung}
      \Fun{D}{x} &=& \frac{m_\t{G} g}{48 I E} \cdot
      \begin{case}[l'rCcCl]
        - 4 x^3 + 3 L^2 x , & 0 &\leq& x &\leq& \frac{L}{2} \\
        4 x^3 - 12 L x^2 + 9 L^2 x - L^3 , & \frac{L}{2} &\leq& x &\leq& L%
      \end{case}
    \end{eqns}%
    folgt.
    Mit
    \begin{eqns}
      \xi &\coloneqq&
      \begin{case}[l'rCcCl]
        - 4 x^3 + 3 L^2 x , & 0 &\leq& x &\leq& \frac{L}{2} \\
        4 x^3 - 12 L x^2 + 9 L^2 x - L^3 , & \frac{L}{2} &\leq& x &\leq& L%
      \end{case} \\
      \itext{gilt}
      \Fun{D}{\xi} &=& A \xi , \\
      \itext{also folgt für $E$}
      E &=& \frac{m_\t{G} g}{2 I A} .
    \end{eqns}%

    Für einen Stab mit quadratischem Querschnitt (Seitenlänge $a$) gilt
    \begin{eqns}
      I &\coloneqq& \Int{y^2}{Q,Q} \\
      &=& \frac{a^4}{12} \\
      \itext{und für einen Stab mit kreisförmigem Querschnitt (Durchmesser $d$) gilt}
      I &=& \frac{\pi d^4}{64} .
    \end{eqns}%
  \section{Aufbau und Durchführung}
    Zur Bestimmung der Materialkonstanten $E$ werden ein eckiger einseitig eingespannter und ein runder ein-, sowie beidseitig, eingespannter Stab untersucht.
    Bei der einseitigen Einspannung werden die Stäbe auf einer Seite in einem Schraubblock fixiert.
    Oberhalb der Stäbe befinden sich zwei verschiebbare Messuhren, die durch einen Stift, der durch einer Feder gestützt ist, Abstandsveränderungen registrieren können.
    Zur verdeutlichung ist Abbildung \ref{fig:aufbau} gegeben.
    \begin{figure}
      \includesgraphics{graphic/aufbau.png}
      \caption{Versuchsaufbau \cite{anleitung103}}
      \label{fig:aufbau}
    \end{figure}%

    Für die Messung muss die Durchbiegung ohne Gewicht berücksichtigt werden.
    Dies wird erreicht, indem die Skala der Messuhren durch Verdrehen genullt wird.
    Anschließend wird ein Gewicht der Masse $m$ am freien Ende des Stabes angehangen und die Durchbiegung am Ort $x_{\t{g}}$ ermittelt.
    Bei der zweiseitigen Einspannung wird der runde Stab nicht fixiert, sondern liegt lediglich auf zwei Auflagepunkten.
    Das Gewicht wird mittig zwischen den Auflagepunkten an der Stelle $x_{\t{G}}$ aufgehängt.
  \section{Messwerte}
    \input{statistics.tex}%

    Die Messwerte sind in den Tabellen \ref{tab:other} bis \ref{tab:zweifach-data} aufgetragen.
    Die Skala zur Bestimmung von $x$ war nicht zur Einspannung kalibriert; der Unterschied beträgt $x_0$.
    Es gilt
    \begin{eqn}
      x = x_{\t{g}} - x_0
    \end{eqn}%
    für den Messwert $x_{\t{g}}$.
    \begin{table}
      \input{table/other.tex}
      \caption{Abmessungen der Messingstäbe und Konstanten}
      \label{tab:other}
    \end{table}%
    \begin{table}
      \input{table/eckig-data.tex}
      \caption{Messwerte für den einfach-eingespannten quadratischen Messingstab}
      \label{tab:eckig-data}
    \end{table}%
    \begin{table}
      \input{table/rund-data.tex}
      \caption{Messwerte für den einfach-eingespannten runden Messingstab}
      \label{tab:rund-data}
    \end{table}%
    \begin{table}
      \input{table/zweifach-data.tex}
      \caption{Messwerte für den zweifach-eingespannten runden Messingstab}
      \label{tab:zweifach-data}
    \end{table}%
  \section{Auswertung}
    \input{linregress.tex}%

    Die Messwerte sind für den quadratischen, einfach eingespannten Messingstab in Abbildung \ref{fig:eckig}, für den runden, einfach eingespannten in Abbildung \ref{fig:rund} aufgetragen.
    Die Messwerte für den doppelt eingespannten, runden Stab sind aufgeteilt in diejenigen, die mit der linken Messuhr (Abbildung \ref{fig:zweifach-links}) und jenen, die mit der rechten Messuhr (Abbildung \ref{fig:zweifach-rechts}), ermittelt wurden.
    Diese werden, wie in \ref{theorie} beschrieben, linearisiert und mit Hilfe einer linearen Ausgleichsrechnung gefittet.
    Die Werte aus der Ausgleichsrechnung für die einfach eingespannten Stäbe stehen in den Tabellen \ref{tab:eckig-calc} und \ref{tab:rund-calc}.
    Die gefitteten Konstanten für den doppelt eingespannten Messingstab, sind, den Graphen entsprechend, in die Tabellen \ref{tab:zweifach-links-calc} und \ref{tab:zweifach-rechts-calc} eingetragen\input{uncertainties.tex}.
    Die ermittelten Werte für das Trägheitsmoment $I$ und für $E$ stehen ebenfalls in diesen Tabellen.
    Die Ausgleichsgeraden sind in den Abbildungen \ref{fig:eckig} bis \ref{fig:zweifach-links} aufgetragen.
    Die Fehler für das Trägheitsmoment und für $E$ sind klein, die Werte allerdings nicht miteinander kompatibel.

    Alle berechneten Werte für $E$ liegen relativ nah aneinander.
    Ungenauigkeiten bei der Messung traten durch die Messuhren auf.
    Insbesondere bei der Messung des zweifach eingespannten Stabes wurde die Durchbiegung durch die zweite Messuhr verfälscht, die auf der anderen Seite ein zusätzliches Gewicht darstellte.
    Die experimentell ermittelten Werte bestätigen die Aussage der Theorie, dass $E$ nur material- und nicht formabhängig ist.
    \begin{figure}
      \includesgraphics{graph/eckig.pdf}
      \caption{Linearisierte Messwerte und Regressionsgerade für den einfach-eingespannten quadratischen Messingstab}
      \label{fig:eckig}
    \end{figure}%
    \begin{table}
      \input{table/eckig-calc.tex}
      \caption{Berechnete Werte für den einfach-eingespannten quadratischen Messingstab}
      \label{tab:eckig-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/rund.pdf}
      \caption{Linearisierte Messwerte und Regressionsgerade für den einfach-eingespannten runden Messingstab}
      \label{fig:rund}
    \end{figure}%
    \begin{table}
      \input{table/rund-calc.tex}
      \caption{Berechnete Werte für den einfach-eingespannten runden Messingstab}
      \label{tab:rund-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/zweifach-links.pdf}
      \caption{Linearisierte Messwerte und Regressionsgerade für die linke Hälfte des zweifach-eingespannten runden Messingstabs}
      \label{fig:zweifach-links}
    \end{figure}%
    \begin{table}
      \input{table/zweifach-links-calc.tex}
      \caption{Berechnete Werte für die linke Hälfte des zweifach-eingespannten runden Messingstabs}
      \label{tab:zweifach-links-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/zweifach-rechts.pdf}
      \caption{Linearisierte Messwerte und Regressionsgerade für die rechte Hälfte des zweifach-eingespannten runden Messingstabs}
      \label{fig:zweifach-rechts}
    \end{figure}%
    \begin{table}
      \input{table/zweifach-rechts-calc.tex}
      \caption{Berechnete Werte für die rechte Hälfte des zweifach-eingespannten runden Messingstabs}
      \label{tab:zweifach-rechts-calc}
    \end{table}%
  \makebibliography
\end{document}
