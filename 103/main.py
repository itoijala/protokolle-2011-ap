#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

a = loadtxt('data/a', unpack=True) * 1e-3
d = loadtxt('data/d', unpack=True) * 1e-3
x_0 = loadtxt('data/x_0') * 1e-3
a_mean = ufloat((mean(a), error(a)))
d_mean = ufloat((mean(d), error(d)))

table  = r'\sisetup{table-format=1.3,round-mode=figures,round-precision=4}' + '\n'
table += r'\begin{tabular}{c s S S[table-format=1.3]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $g$ & \meter\per\square\second & \multicolumn{2}{S}{' + str(g) + r'} \\' + '\n'
table += r'  $x_0$ & \milli\meter & \multicolumn{2}{S[round-mode=off]}{' + str(int(x_0 * 1e3)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  & & \multicolumn{1}{c}{\SIlabel{a}{\milli\meter}} & \multicolumn{1}{c}{\SIlabel{d}{\milli\meter}} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  & & '
for i in range(max_len((a, d))):
  if i > 0:
    table += '  & & '
  if i < len(a):
    table += str(a[i] * 1e3) + ' & '
  else:
    table += '& '
  if i < len(d):
    table += str(d[i] * 1e3) + ' '
  table += r'\\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{\phantom{d}}$ & \milli\meter & \multicolumn{1}{S}{' + str(a_mean.nominal_value * 1e3) + r'} & ' + str(d_mean.nominal_value * 1e3) + r' \\' + '\n'
table += r'  $\merror{\phantom{d}}$ & \milli\meter & \multicolumn{1}{S[round-precision=1]}{' + str(a_mean.std_dev() * 1e3) + r'} & \multicolumn{1}{S[round-precision=1]}{' + str(d_mean.std_dev() * 1e3) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/other.tex', 'w').write(table)

L = loadtxt('data/L_eckig') * 1e-2
m = loadtxt('data/m_eckig') * 1e-3
x_g, D = loadtxt('data/D_eckig', unpack=True)
x_g *= 1e-2
D *= 1e-5

x = x_g - x_0
xi = - x**3 / 3 + L * x**2
I = a_mean**4 / 12

A, B = ulinregress(xi, D)
E = m * g / 2 / I / A

savetxt('build/plot/eckig-points', (xi, D))
savetxt('build/plot/eckig-line', (A.nominal_value, B.nominal_value))

x_g_1 = x_g[:len(x_g) / 2]
x_g_2 = x_g[len(x_g) / 2:]
D_1 = D[:len(D) / 2]
D_2 = D[len(D) / 2:]

table = r'\begin{tabular}{' + '|'.join(['S[table-format=2.0] S[table-format=1.2,round-mode=places,round-precision=2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$L$} & \multicolumn{1}{s}{\centi\meter} & \multicolumn{2}{S[table-format=4.1]}{' + str(L * 1e2) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$m_\t{G}$} & \multicolumn{1}{s}{\gram} & \multicolumn{2}{S[table-format=4.1]}{'+ str(m * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(2):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{\SIlabel{x_\t{g}}{\centi\meter}} & \multicolumn{1}{c'
  if i < 1:
    table += '|'
  table += r'}{\SIlabel{D}{\milli\meter}}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(x_g_1)):
  table += '  ' + str(int(x_g_1[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_1[i] * 1e3) + ' & '
  if i < len(x_g_2):
    table += str(int(x_g_2[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_2[i] * 1e3)
  else:
    table += '&'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/eckig-data.tex' , 'w').write(table)

table = r'\sisetup{table-format=1.5e-2,round-mode=figures,round-precision=1}'
table += r'\renewcommand{\arraystretch}{1.2}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\square\meter & ' + '{:.6f}'.format(A.std_dev()) + r' \\' + '\n'
table += r'  $B$ & \meter & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(B.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \meter & ' + '{:.6f}'.format(B.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $I$ & \meter\tothe{4} & \multicolumn{1}{S[round-precision=4]}{' + str(I.nominal_value) + r'} \\' + '\n'
table += r'  $\error{I}$ & \meter\tothe{4} & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-10]}{' + '{:.6e}'.format(I.std_dev()) + r'} \\' + '\n'
table += r'  $E$ & \newton\per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6e}'.format(E.nominal_value) + r'} \\' + '\n'
table += r'  $\error{E}$ & \newton\per\square\meter & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=10]}{' + '{:.6e}'.format(E.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/eckig-calc.tex', 'w').write(table)

L = loadtxt('data/L_rund') * 1e-2
m = loadtxt('data/m_rund') * 1e-3
x_g, D = loadtxt('data/D_rund', unpack=True)
x_g *= 1e-2
D *= 1e-5

x = x_g - x_0
xi = - x**3 / 3 + L * x**2
I = pi / 64 * d_mean**4

A, B = ulinregress(xi, D)
E = m * g / 2 / I / A

savetxt('build/plot/rund-points', (xi, D))
savetxt('build/plot/rund-line', (A.nominal_value, B.nominal_value))

x_g_1 = x_g[:len(x_g) / 2]
x_g_2 = x_g[len(x_g) / 2:]
D_1 = D[:len(D) / 2]
D_2 = D[len(D) / 2:]

table = r'\begin{tabular}{' + '|'.join(['S[table-format=2.0] S[table-format=1.2,round-mode=places,round-precision=2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$L$} & \multicolumn{1}{s}{\centi\meter} & \multicolumn{2}{S[table-format=4.1]}{' + str(L * 1e2) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$m_\t{G}$} & \multicolumn{1}{s}{\gram} & \multicolumn{2}{S[table-format=4.1]}{'+ str(m * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(2):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{\SIlabel{x_\t{g}}{\centi\meter}} & \multicolumn{1}{c'
  if i < 1:
    table += '|'
  table += r'}{\SIlabel{D}{\milli\meter}}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(x_g_1)):
  table += '  ' + str(int(x_g_1[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_1[i] * 1e3) + ' & '
  if i < len(x_g_2):
    table += str(int(x_g_2[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_2[i] * 1e3)
  else:
    table += '&'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/rund-data.tex' , 'w').write(table)

table = r'\sisetup{table-format=1.5e-2,round-mode=figures,round-precision=1}'
table += r'\renewcommand{\arraystretch}{1.2}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\square\meter & ' + '{:.6f}'.format(A.std_dev()) + r' \\' + '\n'
table += r'  $B$ & \meter & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(B.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \meter & ' + '{:.6f}'.format(B.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $I$ & \meter\tothe{4} & \multicolumn{1}{S[round-precision=4]}{' + str(I.nominal_value) + r'} \\' + '\n'
table += r'  $\error{I}$ & \meter\tothe{4} & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-10]}{' + '{:.6e}'.format(I.std_dev()) + r'} \\' + '\n'
table += r'  $E$ & \newton\per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6e}'.format(E.nominal_value) + r'} \\' + '\n'
table += r'  $\error{E}$ & \newton\per\square\meter & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=10]}{' + '{:.6e}'.format(E.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/rund-calc.tex', 'w').write(table)

L = loadtxt('data/L_zweifach') * 1e-2
m = loadtxt('data/m_zweifach') * 1e-3
x_g, D = loadtxt('data/D_zweifach', unpack=True)
x_g *= 1e-2
D *= 1e-5

x = x_g - x_0
xi_links = []
xi_rechts = []
D_links = []
D_rechts = []
for i in range(len(x)):
  if x[i] <= L / 2:
    xi_links.append(-4 * x[i]**3 + 3 * L**2 * x[i])
    D_links.append(D[i])
  else:
    xi_rechts.append(4 * x[i]**3 - 12 * L * x[i]**2 + 9 * L**2 * x[i] - L**3)
    D_rechts.append(D[i])
I = pi / 64 * d_mean**4

A_links, B_links = ulinregress(xi_links, D_links)
E_links = m * g / 48 / I / A_links
A_rechts, B_rechts = ulinregress(xi_rechts, D_rechts)
E_rechts = m * g / 48 / I / A_rechts

savetxt('build/plot/zweifach-links-points', (xi_links, D_links))
savetxt('build/plot/zweifach-links-line', (A_links.nominal_value, B_links.nominal_value))
savetxt('build/plot/zweifach-rechts-points', (xi_rechts, D_rechts))
savetxt('build/plot/zweifach-rechts-line', (A_rechts.nominal_value, B_rechts.nominal_value))

x_g_1 = x_g[:len(x_g) / 2]
x_g_2 = x_g[len(x_g) / 2:]
D_1 = D[:len(D) / 2]
D_2 = D[len(D) / 2:]

table = r'\begin{tabular}{' + '|'.join(['S[table-format=2.0] S[table-format=1.2,round-mode=places,round-precision=2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$L$} & \multicolumn{1}{s}{\centi\meter} & \multicolumn{2}{S[table-format=4.1]}{' + str(L * 1e2) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$m_\t{G}$} & \multicolumn{1}{s}{\gram} & \multicolumn{2}{S[table-format=4.1]}{'+ str(m * 1e3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  '
for i in range(2):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{\SIlabel{x_\t{g}}{\centi\meter}} & \multicolumn{1}{c'
  if i < 1:
    table += '|'
  table += r'}{\SIlabel{D}{\milli\meter}}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(x_g_1)):
  table += '  ' + str(int(x_g_1[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_1[i] * 1e3) + ' & '
  if i < len(x_g_2):
    table += str(int(x_g_2[i] * 1e2)) + ' & ' + '{:.6f}'.format(D_2[i] * 1e3)
  else:
    table += '&'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/zweifach-data.tex' , 'w').write(table)

table = r'\sisetup{table-format=1.5e2,round-mode=figures,round-precision=1}'
table += r'\renewcommand{\arraystretch}{1.2}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A_links.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\square\meter & ' + '{:.6f}'.format(A_links.std_dev()) + r' \\' + '\n'
table += r'  $B$ & \meter & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(B_links.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \meter & ' + '{:.6f}'.format(B_links.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $E$ & \newton\per\square\meter & \multicolumn{1}{S[round-precision=2]}{' + '{:.6e}'.format(E_links.nominal_value) + r'} \\' + '\n'
table += r'  $\error{E}$ & \newton\per\square\meter & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=10]}{' + '{:.6e}'.format(E_links.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/zweifach-links-calc.tex', 'w').write(table)

table = r'\sisetup{table-format=-1.5e2,round-mode=figures,round-precision=1}'
table += r'\renewcommand{\arraystretch}{1.2}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\square\meter & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A_rechts.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\square\meter & ' + '{:.6f}'.format(A_rechts.std_dev()) + r' \\' + '\n'
table += r'  $B$ & \meter & \multicolumn{1}{S[round-precision=1]}{' + '{:.6f}'.format(B_rechts.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \meter & ' + '{:.6f}'.format(B_rechts.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $E$ & \newton\per\square\meter & \multicolumn{1}{S[round-precision=2]}{' + '{:.6e}'.format(E_rechts.nominal_value) + r'} \\' + '\n'
table += r'  $\error{E}$ & \newton\per\square\meter & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=10]}{' + '{:.6e}'.format(E_rechts.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/zweifach-rechts-calc.tex', 'w').write(table)
