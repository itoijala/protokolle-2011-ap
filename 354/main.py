#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *
from peakfind import peakdetect 

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

### Parsing of data ###
L, L_err, C, C_err, R_1, R_1_err, R_2, R_2_err = loadtxt('data/gerätedaten')
L = ufloat((L * 1e-3, L_err * 1e-3))
C = ufloat((C * 1e-9, C_err * 1e-9))
R_1 = ufloat((R_1, R_1_err))
R_2 = ufloat((R_2, R_2_err))

### setting of experiment ###

table = r'\sisetup{table-format=3.2,round-mode=figures,round-precision=1}'
table += r'\renewcommand{\arraystretch}{1.2}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $L$ & \milli\henry & \multicolumn{1}{S[round-precision=3]}{' + str(L.nominal_value * 1e3) + r'} \\' + '\n'
table += r'  $\error{L}$ & \milli\henry  & ' + str(L.std_dev() * 1e3) + r' \\' + '\n'
table += r'  $C$ & \nano\farad  & \multicolumn{1}{S[round-precision=3]}{' + str(C.nominal_value * 1e9) + r'} \\' + '\n'
table += r'  $\error{C}$ & \nano\farad  & ' + str(C.std_dev() * 1e9) + r' \\' + '\n'
table += r'  $R_1$ & \ohm  & \multicolumn{1}{S[round-precision=3]}{' + str(R_1.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_1}$ & \ohm  & ' + str(R_1.std_dev()) + r' \\' + '\n'
table += r'  $R_2$ & \ohm & \multicolumn{1}{S[round-precision=3]}{' + str(R_2.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_2}$ & \ohm & ' + str(R_2.std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/gerätedaten.tex', 'w').write(table)

t, U = loadtxt('data/einhüllende', unpack=True)

### Finding extremums, saving plot data###
maxtab, mintab = peakdetect(U, t, 4, 1)
savetxt('build/plot/max', maxtab)
tmax, Umax = loadtxt('build/plot/max', unpack=True)
savetxt('build/plot/min', mintab)
tmin, Umin = loadtxt('build/plot/min', unpack=True)
extrema = mintab + maxtab
savetxt('build/plot/maxmin', extrema)

###curve fit of the envelope ###

def Ucalc(t, A, B, C):
  return vals(A * vals(exp(B * t)) + C)

A_oben, B_oben, C_oben = ucurve_fit(Ucalc, vals(tmax), vals(Umax))
savetxt('build/plot/posefunc', (A_oben.nominal_value, B_oben.nominal_value, vals(C_oben)))
A_unten, B_unten, C_unten = ucurve_fit(Ucalc, vals(tmin), vals(Umin))
savetxt('build/plot/negefunc', (A_unten.nominal_value, B_unten.nominal_value, vals(C_unten)))

# experimental and theoretical values from the exponent of the envelope ###
T_ex_1 = -1 / B_oben
R_ef_1 = -B_oben * 2 * L
T_ex_2 = -1 / B_unten
R_ef_2 = -B_unten * 2 * L
R_ef = (R_ef_1 + R_ef_2) / 2
T_ex = (T_ex_1 + T_ex_2) / 2
T_theo = 2 * L / R_1

table = r'\sisetup{table-format=-3.2,round-mode=figures,round-precision=1}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A_\t{max}$ & \volt & \multicolumn{1}{S[round-precision=4]}{' + '{:.6f}'.format(A_oben.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A_\t{max}}$ & \volt & ' + '{:.6f}'.format(A_oben.std_dev()) + r' \\' + '\n'
table += r'  $B_\t{max}$ & \per\kilo\second & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(B_oben.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{B_\t{max}}$ & \per\kilo\second & ' + '{:.6f}'.format(B_oben.std_dev() * 1e-3) + r' \\' + '\n'
table += r'  $C_\t{max}$ & \volt & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(C_oben.nominal_value) + r'} \\' + '\n' 
table += r'  $\error{C_\t{max}}$ & \volt & ' + '{:.6f}'.format(C_oben.std_dev()) + r' \\' + '\n'
table += r'  $A_\t{min}$ & \volt & \multicolumn{1}{S[round-precision=4]}{' + '{:.6f}'.format(A_unten.nominal_value) + r'} \\' + '\n' 
table += r'  $\error{A_\t{min}}$ & \volt & ' + '{:.6f}'.format(A_unten.std_dev()) + r' \\' + '\n'
table += r'  $B_\t{min}$ & \per\kilo\second & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(B_unten.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{B_\t{min}}$ & \per\kilo\second & ' + '{:.6f}'.format(B_unten.std_dev() * 1e-3) + r' \\' + '\n'
table += r'  $C_\t{min}$ & \volt & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(C_unten.nominal_value) + r'} \\' + '\n' 
table += r'  $\error{C_\t{min}}$ & \volt & ' + '{:.6f}'.format(C_unten.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $T_\t{ex,max}$ & \micro\second  & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(T_ex_1.nominal_value * 1e6) + r'} \\' + '\n'
table += r'  $\error{T_\t{ex,max}}$ & \micro\second  & ' + '{:.6f}'.format(T_ex_1.std_dev() * 1e6) + r' \\' + '\n'
table += r'  $R_\t{eff,max}$ & \ohm & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(R_ef_1.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_\t{eff,max}}$ &\ohm & ' + '{:.6f}'.format(R_ef_1.std_dev()) + r' \\' + '\n'
table += r'  $T_\t{ex,min}$ & \micro\second & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(T_ex_2.nominal_value * 1e6) + r'} \\' + '\n'
table += r'  $\error{T_\t{ex,min}}$ & \micro\second &' + '{:.6f}'.format(T_ex_2.std_dev() * 1e6) + r' \\' + '\n'
table += r'  $R_\t{eff,min}$ & \ohm & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(R_ef_2.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_\t{eff,min}}$ & \ohm & ' + '{:.6f}'.format(R_ef_2.std_dev()) + r' \\' + '\n'
table += r'   \midrule' + '\n'
table += r'  $\mean{T_\t{ex}}$ & \micro\second  & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(T_ex.nominal_value * 1e6) + r'} \\' + '\n'
table += r'  $\merror{T_\t{ex}}$ & \micro\second  & ' + '{:.6f}'.format(T_ex.std_dev() * 1e6) + r' \\' + '\n'
table += r'  $\mean{R_\t{eff}}$ & \ohm & \multicolumn{1}{S[round-precision=2]}{' + '{:.6f}'.format(R_ef.nominal_value) + r'} \\' + '\n'
table += r'  $\merror{R_\t{eff}}$ & \ohm &' + '{:.6f}'.format(R_ef.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $T_\t{ex, theor}$ & \micro\second & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(T_theo.nominal_value * 1e6) + r'} \\' + '\n'
table += r'  $\error{T_\t{ex, theor}}$ & \micro\second &' + '{:.6f}'.format(T_theo.std_dev() * 1e6) + r' \\' + '\n'
table += r'  $R_1$ & \ohm  & \multicolumn{1}{S[round-precision=3]}{' + str(R_1.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_1}$ & \ohm & ' + str(R_1.std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/tau.tex', 'w').write(table)

### critical damping ###    
t, U = loadtxt('data/aperiod_grenz', unpack=True)
savetxt('build/plot/apgrenz-points', (t,U))
R_ap = loadtxt('data/R_1p')
R_ap = ufloat((R_ap, 0.03 * R_ap + 0.002))
R_ap_theo = sqrt(4 * L / C)

table = r'\sisetup{table-format=1.3,round-mode=figures,round-precision=4}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $R_{\t{ap},\t{theor}}$ & \kilo\ohm  & ' + str(nominal_values(R_ap_theo) * 1e-3) + r' \\' + '\n'
table += r'  $\error{R_{\t{ap},\t{theor}}}$ & \kilo\ohm  & \multicolumn{1}{S[round-precision=1]}{' +str(std_devs(R_ap_theo) * 1e-3) + r'} \\' + '\n'
table += r'  $R_{\t{ap},\t{exp}}$ & \kilo\ohm & \multicolumn{1}{S[round-precision=3]}{' + str(R_ap.nominal_value) + r'} \\' + '\n'
table += r'  $\error{R_{\t{ap}, \t{exp}}}$ & \kilo\ohm & \multicolumn{1}{S[round-precision=1]}{' + str(R_ap.std_dev()) + r'}  \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/crit_damp.tex', 'w').write(table)

### loading data for amplitude, phase and frequency ###
nu, A, a = loadtxt('data/freq-amp-phase', unpack=True)
A = array([ufloat((v, 0.025)) for v in A]) #0.02 ablesefehler
nu_begr = nu[28:]
A_begr = A[28:]
a_begr = a[28:]

### table for measurement data ####
table = r'\sisetup{}' + '\n'
table += r'\begin{tabular}{S[table-format=2.1,round-mode=places,round-precision=1] S[table-format=3.0] S[table-format=1.3,round-mode=places,round-precision=3]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{\nu}{\kilo\hertz}} & \multicolumn{1}{c}{\SIlabel{U_C}{\milli\volt}} & \multicolumn{1}{c}{\SIlabel{a}{\micro\second}} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(nu_begr)):
  table += '  ' + str(int(nu_begr[i]) / 1e3) + ' & ' + str(int(A_begr[i].nominal_value * 1e3)) + ' & ' + str(a_begr[i]) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/freq_amp_phase.tex', 'w').write(table)

### saving plot data, experimental and theoretical values###
a *= 1e-6
omega = 2 * pi * nu
U_erreg = loadtxt('data/U_err')
savetxt('build/result/U_err.tex', (U_erreg,))
savetxt('build/plot/amplitude', (omega[28:], nominal_values(A[28:]) / U_erreg))

omega_1_amp = loadtxt('data/omega_amp1')
omega_2_amp = loadtxt('data/omega_amp2')
omega_1_amp = ufloat((omega_1_amp, 5e3)) 
omega_2_amp = ufloat((omega_2_amp, 3e3))
delta_omega_exp = omega_2_amp - omega_1_amp
q_fit = ufloat((amax(nominal_values(A) / U_erreg), A[45].std_dev() / U_erreg + 0.002))
q_fit = 1 / sqrt(L * C) / delta_omega_exp
q_theo = 1 / R_2 * sqrt(L / C)
delta_omega_theo = R_2 / L

omega_lin = omega[34:len(omega)- 4]
A_lin = A[34:len(omega) -4]
savetxt('build/plot/amplitude_lin_points', (omega_lin, nominal_values(A_lin) / U_erreg))

table = r'\sisetup{table-format=3.3,round-mode=figures,round-precision=3}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\omega_-$ & \kilo\hertz & \multicolumn{1}{S[round-precision=3]}{' + str(omega_1_amp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_-}$ & \kilo\hertz & \multicolumn{1}{S[round-precision=1]}{' + str(omega_1_amp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $\omega_+$ & \kilo\hertz & \multicolumn{1}{S[round-precision=3]}{' + str(omega_2_amp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_+}$ & \kilo\hertz & \multicolumn{1}{S[round-precision=1]}{' + str(omega_2_amp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $\Delta\omega$ & \kilo\hertz & \multicolumn{1}{S[round-precision=2]}{' + str(delta_omega_exp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\Delta\omega}$ & \kilo\hertz & \multicolumn{1}{S[round-precision=1]}{' + str(delta_omega_exp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $q_\t{exp}$ & & \multicolumn{1}{S[round-precision=2]}{' + str(q_fit.nominal_value) + r'} \\' + '\n'
table += r'  $\error{q_\t{exp}}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(q_fit.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\Delta\omega_\t{theor}$ & \kilo\hertz  & ' + str(delta_omega_theo.nominal_value * 1e-3) + r' \\' + '\n'
table += r'  $\error{\Delta\omega_\t{theor}}$ & \kilo\hertz & \multicolumn{1}{S[round-precision=1]}{' + str(delta_omega_theo.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $q_\t{theor}$ & & ' + str(q_theo.nominal_value) + r' \\' + '\n'
table += r'  $\error{q_\t{theor}}$ & & \multicolumn{1}{S[round-precision=1]}{' + str(q_theo.std_dev()) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/amplitude.tex', 'w').write(table)

###loading data creating table for phase shift ###
phi = a * omega
savetxt('build/plot/phase_log', (omega[28:], phi[28:]))
omega_lin = omega[37:len(omega)]
phi_lin = phi[37:len(omega)]
savetxt('build/plot/phase_log_lin', (omega_lin, phi_lin))

nu_res_theo = sqrt(1 / (L * C) - R_2**2 / (4 * L**2))
nu_1_theo = sqrt(R_2**2 / (4 * L**2) + 1 / (L * C)) - R_2 / (2 * L)
nu_2_theo = sqrt(R_2**2 / (4 * L**2) + 1 / (L * C)) + R_2 / (2 * L)
nu_res_exp = loadtxt('data/omega_res')
nu_1_exp = loadtxt('data/omega_1')
nu_2_exp = loadtxt('data/omega_2')
nu_res_exp = ufloat((nu_res_exp, 1e3))
nu_1_exp = ufloat((nu_1_exp, 2e3))
nu_2_exp = ufloat((nu_2_exp, 4e3))


table = r'\sisetup{table-format=3.1,round-mode=figures,round-precision=1}'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\omega_{\t{res},\t{exp}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=3]}{' + str(nu_res_exp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_{\t{res},\t{exp}}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=1]}{' + str(nu_res_exp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $\omega_{\t{1},\t{exp}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=3]}{' + str(nu_1_exp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_{\t{1},\t{exp}}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=1]}{' + str(nu_1_exp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  $\omega_{\t{2},\t{exp}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=3]}{' + str(nu_2_exp.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_{\t{2},\t{exp}}}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=1]}{' + str(nu_2_exp.std_dev() * 1e-3) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\omega_\t{res,theor}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=3]}{' + str(nominal_values(nu_res_theo) * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_\t{res,theor}}$ & \kilo\hertz  & ' + str(std_devs(nu_res_theo) * 1e-3) + r' \\' + '\n'
table += r'  $\omega_\t{1,theor}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=4]}{' + str(nu_1_theo.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_\t{1,theor}}$ & \kilo\hertz & ' + str(nu_1_theo.std_dev() * 1e-3) + r' \\' + '\n'
table += r'  $\omega_\t{2,theor}$ & \kilo\hertz  & \multicolumn{1}{S[round-precision=3]}{' + str(nu_2_theo.nominal_value * 1e-3) + r'} \\' + '\n'
table += r'  $\error{\omega_\t{2,theor}}$ & \kilo\hertz & ' + str(nu_2_theo.std_dev() * 1e-3) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/phase.tex', 'w').write(table)

### loading data, creating tables and graphs for impedance ###
omega, phi, Z_exp = loadtxt('data/impedanz', unpack=True)

table = r'\sisetup{round-mode=figures}' + '\n'
table += r'\begin{tabular}{S[table-format=2.1, round-mode=places, round-precision=1] S[table-format=3.0] S[table-format=-2.0]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{\nu}{\kilo\hertz}} & \multicolumn{1}{c}{\SIlabel{\abs{Z}}{\ohm}} & \multicolumn{1}{c}{\SIlabel{\varphi}{\degree}} \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(len(omega)):
  table += '  ' + str(omega[i]) + ' & ' + str(Z_exp[i]) + ' & ' + str(phi[i]) + r' \\' + '\n'

table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/impedance.tex', 'w').write(table)

phi = phi / 180 * pi
savetxt('build/plot/impedanz_theo', (R_1.nominal_value, L.nominal_value, C.nominal_value))
savetxt('build/plot/polar', (phi, Z_exp))
