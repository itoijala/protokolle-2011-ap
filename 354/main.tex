\input{header.tex}

\renewcommand{\ExperimentNumber}{354}

\begin{document}
  \maketitlepage{Gedämpfte und erzwungene Schwingungen}{19.01.2012}{09.02.2012}
  \section{Ziel}
    Ziel des Experiments ist die Untersuchung von gedämpften und erzwungenen Schwingungen eines $RCL$-Kreises.
  \section{Theorie}
    \subsection{Gedämpfte Schwingung}
      In einem Stromkreis bestehend aus einer Kapazität~$C$ und einer Induktivität~$L$ kann die Energie eine Schwingung zwischen dem Kondensator und der Spule durchführen.
      Durch den Widerstand~$R$ im $RCL$-Kreis (Abbildung \ref{fig:RCL}) wird diese Schwingung gedämpft.
      Nach dem zweiten Kirchhoff'schen Gesetz gilt
      \begin{eqn}
        U_R + U_C + U_L = 0 .
      \end{eqn}%
      Mit
      \begin{eqns}
        U_R &=& RI , \\
        U_C &=& \frac{Q}{C} , \\
        U_L &=& L \dot{I} \\
        \itext{und}
        I &=& \dot{Q}
      \end{eqns}%
      folgt für den Strom~$I$ die Differentialgleichung
      \begin{eqn}
        \ddot{I} + \frac{R}{L} \dot{I} + \frac{1}{LC} I = 0 . \eqlabel{eqn:DGL}
      \end{eqn}%
      \begin{figure}[b]
        \includegraphics{pstricks/1.pdf}
        \caption{$RCL$-Kreis}
        \label{fig:RCL}
      \end{figure}%

      Mit dem Ansatz
      \begin{eqn}
        \Fun{I}{t} = \E^{\lambda t}
      \end{eqn}%
      erhält man die charakteristische Gleichung
      \begin{eqn}
        \lambda^2 + \frac{R}{L} \lambda + \frac{1}{LC} = 0 ,
      \end{eqn}%
      die durch
      \begin{eqn}
        \lambda_{1,2} = - \frac{R}{2L} \pm \sqrt{\frac{R^2}{4L^2} - \frac{1}{LC}}
      \end{eqn}
      gelöst wird.
      Alle Lösungen der Gleichung \eqref{eqn:DGL} sind also
      \begin{eqn}
        \Fun{I}{t} =
        \begin{case}
          A_1 \E^{\lambda_1 t} + A_2 \E^{\lambda_2 t} , & \lambda_1 \neq \lambda_2 \\
          A_1 \E^{\lambda t} + A_2 t \E^{\lambda t} , & \lambda_1 = \lambda_2 \eqqcolon \lambda .
        \end{case}
      \end{eqn}%
      Mit den Definitionen
      \begin{eqns}
        \gamma &\coloneqq& \frac{R}{2L} \\
        \omega_0^2 &\coloneqq& \frac{1}{LC} \\
        \omega &\coloneqq& \sqrt{\gamma^2 - \omega_0^2}
      \end{eqns}%
      ergibt sich
      \begin{eqn}
        \Fun{I}{t} = \E^{-\gamma t} \cdot
        \begin{case}
          A_1 \E^{\omega t} + A_2 \E^{-\omega t} , & \gamma^2 > \omega_0^2 \\
          A_1 + A_2 t , & \gamma^2 = \omega_0^2 \\
          A_1 \E^{\I \omega t} + A_2 \E^{-\I \omega t} , & \gamma^2 < \omega_0^2 .
        \end{case}
      \end{eqn}%

      Der erste Fall heißt Kriechfall, der zweite aperiodischer Grenzfall und der dritte Schwingfall.
      Im Schwingfall ergibt sich die Umformung
      \begin{eqn}
        \Fun{I}{t} = A_0 \E^{-\gamma t} \Cos{\omega t + \phi} .
      \end{eqn}%
      Die Schwingungsdauer der Schwingung beträgt
      \begin{eqn}
        T = \frac{2\pi}{\omega} = \frac{2\pi}{\sqrt{\frac{R^2}{L^2} - \frac{4}{LC}}} .
      \end{eqn}%
      Für die Zeit $T_\t{ex}$, nach der die Amplitude auf ein $\E$-tel abgefallen ist, ergibt sich
      \begin{eqn}
        T_\t{ex} = \frac{1}{\gamma} = \frac{2L}{R} .
      \end{eqn}%

      Beim Kriechfall kehrt das System mit maximal einem Nulldurchgang in die Ruhelage zurück.
      Der aperiodische Grenzfall tritt bei dem Widerstand $R_\t{ap}$ auf.
      Es gilt
      \begin{eqns}
        \frac{R_\t{ap}^2}{4L^2} &=& \frac{1}{LC} \\
         R_\t{ap} &=& 2 \sqrt{\frac{L}{C}} . \eqlabel{eqn:R_ap}
      \end{eqns}%
    \subsection{Erzwungene Schwingung}
    In den $RCL$-Kreis wird nun ein Sinusgenerator eingebaut, der die Spannung $\Fun{U}{t} = U_0 \E^{\I \omega t}$ liefert und den Kreis zu Schwinungen anregt (Abbildung \ref{fig:RCL-U}).
      Statt der Gleichung \eqref{eqn:DGL} gilt jetzt
      \begin{eqn}
        LC \ddot{U}_C + RC \dot{U}_C + U_C = U_0 \E^{\I \omega t} .
      \end{eqn}%
      Mit dem Ansatz
      \begin{eqn}
        \Fun{U_C}{t} = \Fun{A}{\omega} \E^{\I \omega t}
      \end{eqn}%
      ergibt sich die Gleichung
      \begin{eqn}
        -LC \omega^2 A + \I RC \omega A + A = U_0 ,
      \end{eqn}%
      also folgt für die Amplitude $A$
      \begin{eqns}
        \Fun{A}{\omega} &=& \frac{U_0}{-LC \omega^2 + \I RC \omega + 1} \\
        &=& U_0 \frac{-LC \omega^2 - \I RC \omega + 1}{\del{LC \omega^2 - 1}^2 + R^2 C^2 \omega^2} \\
        \Abs{\Fun{A}{\omega}} &=& \frac{U_0}{\sqrt{\del{LC \omega^2 - 1}^2 + R^2 C^2 \omega^2}} \\
        \arg\Fun{A}{\omega} &=& \ArcTan{\frac{RC \omega}{LC \omega^2 - 1}} . \eqlabel{eqn:argA}
      \end{eqns}%
      Damit gilt für die Amplitude von $U_C$
      \begin{eqn}
        \Fun{U_C}{\omega} = \frac{U_0}{\sqrt{\del{LC \omega^2 - 1}^2 + R^2 C^2 \omega^2}} .
      \end{eqn}%
      Für $\omega \rightarrow 0$ strebt $U_C$ gegen die Erregeramplitude $U_0$.
      Für $\omega \rightarrow \infty$ geht $U_C$ gegen $0$.
      \begin{figure}[b]
        \includegraphics{pstricks/2.pdf}
        \caption{$RCL$-Kreis mit Sinusgenerator}
        \label{fig:RCL-U}
      \end{figure}%

      $U_C$ besitzt bei der Frequenz
      \begin{eqn}
        \omega_\t{res} = \sqrt{\frac{1}{LC} - \frac{R^2}{2L^2}}
      \end{eqn}%
      ein Maximum, das als Resonanz bezeichnet wird.
      Für kleines $R$ gilt
      \begin{eqns}
        \frac{R^2}{2L^2} &\ll& \frac{1}{LC} , \\
        \itext{und damit}
        \omega_\t{res} &\approx& \omega_0 = \frac{1}{\sqrt{LC}} .
      \end{eqns}%
      In diesem Fall gilt für die maximale Kondensatorspannung
      \begin{eqn}
        U_{C,\t{max}} = \frac{1}{RC \omega_0} U_0 = \frac{1}{R} \sqrt{\frac{L}{C}} U_0 .
      \end{eqn}%
      Die Resonanzüberhöhung oder Güte $q$ wird definiert als
      \begin{eqn}
        q \coloneqq \frac{1}{RC \omega_0} = \frac{1}{R} \sqrt{\frac{L}{C}} .
      \end{eqn}%
      Die Resonanz wird durch die Breite des Resonanzmaximums beschrieben.
      Sie ist definiert als $\omega_+ - \omega_-$, wobei $\omega_+$ und $\omega_-$ durch
      \begin{eqn}
        \frac{1}{\sqrt{2}} \frac{1}{R} \sqrt{\frac{L}{C}} U_0 = \frac{U_0}{\sqrt{\del{LC \omega_\pm^2 - 1}^2 + R^2 C^2 \omega_\pm^2}}
      \end{eqn}%
      gegeben sind.
      $\omega_\pm$ sind also die Frequenzen, bei denen die Spannung auf ein $\sqrt{2}$-tel gefallen ist.
      Es gilt
      \begin{eqns}
        \omega_+ - \omega_- &\approx& \frac{R}{L} , \eqlabel{eqn:omega+-} \\
        \itext{woraus}
        q = \frac{\omega_0}{\omega_+ - \omega_-} &=& \frac{1}{R} \sqrt{\frac{L}{C}} \eqlabel{eqn:q}
      \end{eqns}%
      folgt.
      Aus der Gleichung \eqref{eqn:argA} folgt, dass $\varphi \coloneqq \Arg{\Fun{A}{\omega}}$ für $\omega \rightarrow 0$ gegen $0$ und für $\omega \rightarrow \infty$ gegen $\pi$ geht.
      Bei der Frequenz $\omega = \omega_0$ beträgt die Phasendifferenz $\frac{\pi}{2}$.
      Für $\omega_1$ und $\omega_2$, die durch
      \begin{eqns}
        \Fun{\varphi}{\omega_{1,2}} &=& \del{\frac{1}{2} \mp \frac{1}{4}} \pi \\
        \itext{definiert sind, gilt}
        \omega_1 - \omega_2 &=& \frac{R}{L} ,
      \end{eqns}%
      was für den Fall der schwachen Dämpfung mit \eqref{eqn:omega+-} übereinstimmt.
    \subsection{Impedanz}
      Wenn der $RCL$-Kreis wie in Abbildung \ref{fig:Z} Dipol betrachtet wird, kann er als Baustein mit dem Frequenzabhängigen komplexen Widerstand $\Fun{Z}{\omega}$ behandelt werden.
      Es gilt
      \begin{eqn}
        Z = X + \I Y ,
      \end{eqn}%
      wobei $X$ als Wirkwiderstand und $Y$ als Blindwiderstand bezeichnet werden.
      Der Betrag von $Z$ wird als Scheinwiderstand oder Impedanz bezeichnet.
      Für den $RCL$-Kreis folgt aus
      \begin{eqns}
        Z_R &=& R \\
        Z_L &=& \I \omega L \\
        Z_C &=& - \I \frac{1}{\omega C} \\
        \itext{für $Z$}
        \Abs{\Fun{Z}{\omega}} &=& \sqrt{R^2 + \del{\omega L - \frac{1}{\omega C}}^2} .
      \end{eqns}%
      Der Scheiwiderstand besitzt ein Minimum an der Stelle $\omega = \omega_0$ und divergiert für $\omega \rightarrow 0$ und $\omega \rightarrow \infty$.
      \begin{figure}
        \includegraphics{pstricks/3.pdf}
        \caption{$RCL$-Kreis als Dipol}
        \label{fig:Z}
      \end{figure}%
  \section{Aufbau und Durchführung}
    Bei der ersten Messung wird der zeitliche Verlauf der gedämpften Schwingung mit einem Speicheroszillograph aufgenommen.
    Dazu wird der $RCL$-Kreis durch einen Nadelimpulsgenerator angetrieben und die Spannung am Kondensator mit einem Tastkopf abgegriffen (Abbildung \ref{fig:aufbau1}).
    Der Tastkopf dient dazu, den Einfluss der Eingangswiderstände am Millivoltmeter und Oszillographen auf den beobachteten Widerstand gering zu halten.
    Die Zeit zwischen zwei Impulsen sollte relativ groß und $R$ relativ klein gewählt werden (hier wurde $R_1$\footnote{Die Gerätedaten stehen in Tabelle \ref{tab:gerätedaten}} verwendet), um ein gutes Bild zu erhalten.
    \begin{figure}
      \includegraphics{pstricks/4.pdf}
      \caption{Aufbau der ersten und zweiten Messung}
      \label{fig:aufbau1}
    \end{figure}%

    Die zweite Messung ist zur Bestimmung von $R_\t{ap}$.
    Der variable Widerstand wird auf einen großen Wert eingestellt und langsam verringert, bis ein Minimum sichtbar wird.
    Dann wird er wieder erhöht, bis das Minimum verschwindet, um den aperiodischen Grenzfall zu erreichen.
    Ein Bild des Spannungsverlaufs (vergleiche Abbildung \ref{fig:R_ap}) wird aufgenommen.

    Bei der dritten Messung werden die Amplitude der Kondensatorspannung und ihre Phasendifferenz zur Erregerspannung in Abhängigkeit von der Frequenz gemessen.
    Dazu wird die Spannung am Kondensator von einem Millivoltmeter und einem Zwei-Kanal-Oszilloskop abgegriffen (Abbildung \ref{fig:aufbau2}).
    Die Erregerspannung wird am zweiten Kanal des Oszilloskops angeschlossen.
    Die Amplitude wird mit dem Millivoltmeter und die Phase mit dem Oszilloskop bestimmt.
    Hier sollte der Widerstand relativ groß gewählt werden, da bei einem größeren Widerstand die Resonanz besser zu beobachten ist; $R_2$ wurde verwendet.
    \begin{figure}
      \includegraphics{pstricks/5.pdf}
      \caption{Aufbau zur Messung der Amplitude und der Phase}
      \label{fig:aufbau2}
    \end{figure}%

    Als letztes wird die Impedanz des Kreises in Abhängigkeit von der Frequenz mit einem Impedanzmeter gemessen (Abbildung \ref{fig:aufbau3}).
    \begin{figure}
      \includegraphics{pstricks/6.pdf}
      \caption{Aufbau zur Messung der Impedanz}
      \label{fig:aufbau3}
    \end{figure}%
  \section{Messwerte}
    Gegebene Daten der verwendeten Geräte sind in Tabelle \ref{tab:gerätedaten} aufgeführt.
    Die Messwerte für die Amplitude und die Phase in Abhängigkeit von der Frequenz sind in Tabelle \ref{tab:freq_amp_phase} aufgeführt.
    Die Messwerte aus der Impedanzmessung sind in Tabelle \ref{tab:impedance} gegeben.
    \begin{table}
      \input{table/gerätedaten.tex}
      \caption{Gerätedaten von Gerät 3}
      \label{tab:gerätedaten}
    \end{table}%
    \begin{table}
      \input{table/freq_amp_phase.tex}
      \caption{Messwerte für frequenzabhängige Amplitude und Phase}
      \label{tab:freq_amp_phase}
    \end{table}%
    \begin{table}
      \input{table/impedance.tex}
      \caption{Messwerte der Impedanzmessung}
      \label{tab:impedance}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    \subsection{Gedämpfte Schwingung}
    	In Abbildung \ref{fig:tau} wird durch die blaue Kurve der gedämpfte Schwingvorgang aus der ersten Messung visualisiert.
    	Um die Abklingzeit zu ermitteln, werden zunächst die auftretenden Extrema ermittelt\input{peakfind.tex}; diese sind ebenfalls in Abbildung \ref{fig:tau} aufgetragen.
    	Die ermittelten Extrema $(t_i, U_i)$ werden durch
    	\begin{eqn}
      	U = A \E^{Bt} + C
    	\end{eqn}%
    	gefittet.
    	Die Ergebnisse der Ausgleichsrechnung samt Fehlern befinden sich in Tabelle \ref{tab:tau}.
    	Die Exponenten der beiden Einhüllenden werden gemittelt und zur Berechnung der Abklingzeit $T_\t{ex}$ und des effektiven Widerstands $R_\t{eff}$ verwendet.
    	Die berechneten Werte, sowie deren Fehler sind ebenfalls in Tabelle \ref{tab:tau} aufgeführt.
    	Der experimentell ermittelte Widerstand ist mehr als ein Faktor 2 höher als der theoretisch ins Gerät eingebaute.
    	Daraus folgt, dass Widerstände in den Drähten und Induktivitäten vernachlässigt wurden, woraus eine Abfallzeit folgt, die kleiner als die theoretisch berechnete ist, da der Widerstand in die Abfallzeit reziprok eingeht.
    	\begin{figure}
      	\includesgraphics{graph/tau.pdf}
      	\caption{Gedämpfte Schwingung und Einhüllende}
      	\label{fig:tau}
    	\end{figure}%
    	\begin{table}
      	\input{table/tau.tex}
      	\caption{Daten der Ausgleichsrechnung und daraus ermittelte sowie theoretische Werte}
      	\label{tab:tau}
    	\end{table}% 

    	In Abbildung \ref{fig:R_ap} ist der Verlauf der Spannung im Aperiodischen Grenzfall geplottet.
    	Der gemessene Grenzwiderstand $R_\text{ap}$, sowie der mit Formel \ref{eqn:R_ap} errechnete, stehen in Tabelle \ref{tab:R_ap}.
    	Dass der theoretische Wert größer ist als der experimentelle, ist auf die vorkommenden Induktivitäten im Draht, sowie vernachlässigte Widerstände im Draht und den Induktivitäten, die sich zu dem einstellbaren Widerstand hinzuaddieren, zurückzuführen.
    	Außerdem ist die Eichgenauigkeit des variablen Widerstands unbekannt, angenommen wird hier aber $\SI{3}{\percent}$, da der variable Widerstand einem Gerät aus einem früheren Versuch ähnelt.
      Die Messmethode ist ungenau, da die Dicke der Spannungskurve, hervorgerufen durch ein Rauschen, eine genaue Einstellung verhindert.
    	\begin{figure}
      	\includesgraphics{graph/R_ap.pdf}
      	\caption{Aperiodischer Grenzfall}
      	\label{fig:R_ap}
    	\end{figure}%
    	\begin{table}
      	\input{table/crit_damp.tex}
      	\caption{gemessener und theoretischer Grenzwiderstand $R_\t{ap}$} 
      	\label{tab:R_ap}
    	\end{table}%
			\FloatBarrier
		\subsection{Erzwungene Schwingung}
    	Das Verhältnis der Kondensatorspannung $U_\t{C}$ zur Erregerspannung \mbox{$U_\t{err} = \SI{9}{\volt}$} in Abhängigkeit von $\omega$ ist in Abbildung \ref{fig:amp_log} in einem doppelt-logarithmischen Diagramm geplottet.
    	Aus dem Graphen in Abbildung \ref{fig:amp_lin} liest man die Werte $\omega_{+}$ und $\omega_{-}$ ab und erhält $\Delta \omega$ durch Subtraktion.
    	Daraus wird mittels der Formel \eqref{eqn:q} die Resonanzüberhöhung berechnet.
    	Diese Werte, die angenommenen Mess- und Ablesefehler, sowie die theoretisch errechneten, sind in Tabelle \ref{tab:amplitude} aufgeführt. 
    	Vergleicht man die Messwerte mit der Theorie, so fällt auf, dass die Werte für die Resonanzbreite und Resonanzüberhöhung in der selben Größenordnung liegen, aber nicht ganz übereinstimmen.
    	Die Diskrepanzen sind wie oben auf vernachlässigte Induktivitäten und Widerstände zurückzuführen.
    	\begin{figure}
      	\includesgraphics{graph/amplitude.pdf}
      	\caption{Doppeltlogarithmischer Plot der frequenzabhängigen Amplitude}
      	\label{fig:amp_log}
    	\end{figure}%
    	\begin{figure}
      	\includesgraphics{graph/amplitude_lin.pdf}
      	\caption{Linearer Plot der frequenzabhängigen Amplitude im Bereich der Resonanzfrequenz}
      	\label{fig:amp_lin}
    	\end{figure}%
    	\begin{table}
      	\input{table/amplitude.tex}
      	\caption{Abgelesene und theoretische Werte zur frequenzabhängigen Amplitude}
      	\label{tab:amplitude}
    	\end{table}%

    	In Abbildung \ref{fig:phase} ist ein halblogarithmischer Plot der Phasenverschiebung zwischen Kondensatorspannung und Erregerspannung in Abhängigkeit von der Frequenz $\omega$.
    	Dies wird in Abbildung \ref{fig:phase_lin} im Bereich der Resonanzfrequenz  gesondert linear dargestellt.
    	Aus diesem Graphen liest man die Resonanzfrequenz $\omega_\t{res}$ bei einer Phasenverschiebung von $\varphi = \frac{\pi}{2}$, sowie die Frequenzen $\omega_1$ und $\omega_2$ bei $\varphi = \frac{\pi}{4}$ und $\varphi = \frac{3\pi}{4}$ ab.
    	Die abgelesenen Werte, angenommene Mess- und Ablesefehler, sowie die theoretisch errechneten stehen in Tabelle \ref{tab:phase}.
    	Die experimentellen Werte sind durchgehend größer als die theoretischen.
    	Auch $\omega_2 - \omega_1$ ist bei den experimentellen Werten größer.
    	Gründe für die Abweichung sind die bereits benannten unberücksichtigten Induktivitäten und Widerstände.
    	\begin{figure}
      	\includesgraphics{graph/phase.pdf}
      	\caption{Phasenverschiebung $\varphi$ in Abhängigkeit von der Frequenz $\omega$}
      	\label{fig:phase}
    	\end{figure}%
    	\begin{figure}
      	\includesgraphics{graph/phase_lin.pdf}
      	\caption{Phasenverschiebung $\varphi$ in Abhängigkeit von der Frequenz $\omega$}
      	\label{fig:phase_lin}
    	\end{figure}%
    	\begin{table}
      	\input{table/phase.tex}
      	\caption{Abgelesene und errechnete Werte zur Phasenverschiebung}
      	\label{tab:phase}
    	\end{table}%
			\FloatBarrier
		\subsection{Impedanz}
    	Ein halblogarithmischer Plot der Theoriekurve des frequenzabhängigen Scheinwiderstands ist in Abbildung \ref{fig:imp_log} gegeben.
    	Die ermittelten Messwerte sind dort ebenfalls geplottet.
    	Vergleicht man beide, so fällt auf, dass die theoretische Kurve relativ zu den Messwerten in der Frequenz nach rechts verschoben ist.
    	Dies kann man auf den vernachlässigten Innenwiderstand der Drähte und der Induktivität zurückführen.
    	Da die Impedanz mit einem kommerziellen Impedanzmeter ermittelt wurde, der zusätzlich die Phase anzeigt, ließ sich ebenfalls die Ortskurve ermitteln, also der Betrag des Scheinwiderstands in Abhängigkeit von seinem Argument.
    	Diese ist in Abbildung \ref{fig:impd} in einem Polarplot veranschaulicht.
    	Die Messwerte passen in diesem Plot gut zur Theorie.
    	\begin{figure}
      	\includesgraphics{graph/impedanz_log.pdf}
      	\caption{Impedanz in Abhängigkeit von der Frequenz}
      	\label{fig:imp_log}
    	\end{figure}%
  		\begin{figure}
    		\includesgraphics{graph/polar.pdf}
    		\caption{Ortskurve der Frequenzabhängige Impedanz}
    		\label{fig:impd}
  		\end{figure}%
  \makebibliography
\end{document}
