\input{header.tex}

\renewcommand{\ExperimentNumber}{406}

\begin{document}
  \maketitlepage{Beugung am Spalt}{15.11.2011}{22.11.2011}
  \section{Ziel}
    Ziel des Versuches ist es eine durch Beugung am Spalt erzeugte Beugungsfigur auszumessen und daraus die Spaltbreite und den Spaltabstand zu berechnen.
  \section{Theorie}
    Immer wenn Licht durch einen Spalt oder auf ein undurchsichtiges Objekt trifft, dessen Breite oder Abmessungen kleiner als die Breite des Lichtstrahls sind, verhält sich das Licht wider den Gesetzen der geometrischen Optik; es wird gebeugt.
    Da man hier bei der Lichtausbreitung über sehr viele Lichtquanten mitteln kann, reicht das klassische Wellenmodell als gute Näherung für die Beschreibung aus.
    Es gibt zwei grundlegene Versuchsanordnungen, die Fresnelsche und Fraunhofsche Beugung am Spalt.
    Weil bei der Fresnelschen Beugung im Aufpunkt Strahlen, die unter verschiedenen Winkeln gebeugt wurden, interferieren, wird dies mathematisch kompliziert.
    Im Folgenden wird daher die Fraunhoferbeugung behandelt.

    Bei dieser liegt die Lichtquelle im Unendlichen, die Strahlen fallen also alle unter dem selben Winkel auf den Spalt ein und werden dann gleich gebeugt.
    Der Aufpunkt wird durch eine Sammellinse auf die Brennebene abgebildet und somit ins Unendliche verlegt, da man davon ausgeht dass vor der Linse alle Strahlen mit dem selben Winkel $\varphi$ gebeugt wurden.

    Beim Beugungsobjekt handelt es sich um einen Spalt, dessen Länge im Verhältnis zur Breite $b$ groß ist, was zur Folge hat, dass das Lichtbündel nur in einer Dimension begrenzt wird.
    Aus der z-Richtung kommt ein Laserstrahl mit der Stärke
    \begin{eqn}
      A(z,t) = A_0 \exp \left( i \left( \omega t - \frac{2 \pi z}{\lambda} \right) \right) .
    \end{eqn}%
    Will man die Beugung erklären, so brauch man zwei Prinzipien.
    Das Huygensche Prinzip besagt, dass jeder Punkt einer Wellenfront Elementarkugelwellen aussendet.
    Die äußeren Einhüllenden der Elementarwellenfronten interferieren nach Fresnel zur neuen Wellenfront.

    \begin{figure}
      \includegraphics{pstricks/1.pdf}
      \caption{Schematische Darstellung des Einfachspaltes}
      \label{fig:einzelspalt}
    \end{figure}%
    Möchte man die Intensität des Lichts berechnen, die mit einem Winkel von $\varphi$ gebeugt wird, so muss man über alle Punkte integrieren, die in $\varphi$-Richtung strahlen (siehe Abbildung \ref{fig:einzelspalt}).
    Für die Strahlen, die von zwei Punkten mit dem Abstand $x$ ausgestrahlt werden, ergibt sich eine Phasendifferenz von
    \begin{eqn}
      \delta = \frac{2 \pi x \sin \varphi}{\lambda}
    \end{eqn}%
    mit der Wellenlänge $\lambda$ und dem Wegunterschied der Strahlen $s$.
    Für die Gesamtamplitude in $\varphi$-Richtung ergibt sich
    \begin{eqns}
      B(z, t, \varphi) & = & A_0 \int_0^b \exp \left( i \left( \omega t - \frac{2 \pi z}{\lambda} + \delta \right) \right) \dif{x} \eqlabel{eqn:B-einzel} \\
      & = & A_0 b \exp \left( i \left( \omega t - \frac{2 \pi z}{\lambda} \right) \right)
                \exp \left( i \frac{\pi b \sin \varphi}{\lambda} \right)
                \frac{\lambda}{\pi b \sin \varphi} \sin \left( \frac{\pi b \sin \varphi}{\lambda} \right) .
    \end{eqns}%
    Hier sind nur die beiden letzten Faktoren von Interesse, da der erste und zweite Faktor keinen Einfluss auf die Intensitätsmessung haben.
    Da nur die Intensität des Laserlichts messbar ist, verwendet man
    \begin{eqn}
      I(\varphi) \sim B^2(\varphi) = A_0^2 b^2 \left( \frac{\lambda}{\pi b \sin \varphi} \right)^2 \sin^2 \left( \frac{\pi b \sin \varphi}{\lambda} \right) . \eqlabel{eqn:intensität_einfach}
    \end{eqn}%

    \begin{figure}
      \includegraphics{pstricks/2.pdf}
      \caption{Schematische Darstellung des Doppelspaltes}
      \label{fig:doppelspalt}
    \end{figure}%
    Analog lässt sich die Intensitätsverteilung bei Beugung an einem Doppelspalt (Abbildung \ref{fig:doppelspalt}) berechnen.
    Es ergibt sich
    \begin{eqn}
      I(\varphi) \sim  B^2(\varphi) = A_0^2 b^2 \cos^2 \left( \frac{\pi s \sin \varphi}{\lambda} \right) \left( \frac{\lambda}{\pi b \sin \varphi} \right)^2 \sin^2 \left( \frac{\pi b \sin \varphi}{\lambda} \right) . \eqlabel{eqn:intensität_doppel}
    \end{eqn}%
  \section{Versuchsaufbau- und durchführung}
    In einem verdunkelten Raum werden auf einer geraden Schiene, auf der die angebrachten Elemente verschiebbar sind, nacheinander ein He-Ne-Laser mit Wellenlänge $\lambda$, die gewünschte Spaltkonfiguration und davon im Abstand $L$ ein Lichtdetektor auf einem verschiebbaren Reiter mit Messskala (vgl. Abbildung \ref{fig:versuchsaufbau}) angebracht.
    Bei der Spaltkonfiguration handelt es sich entweder um einen konstanten oder einen variablen Einzelspalt, oder um einen Doppelspalt.
    Hinter dem Detektor befindet sich eine weiße Wand, auf das die Beugungsfigur zur Kontrolle projiziert werden kann.

    Zu Begin des Versuches wird der thermische Dunkelstrom $I_\t{Du}$ des Detektors vemessen, um die tatsächlich eingestrahlte Amplitude nicht zu verfälschen.
    Anschließend wird der Laser so ausgerichtet, dass sein Strahl genau den Einzel- oder Doppelspalt trifft.
    Dieser wird wiederum so ausgerichtet, dass das 0. Maximum der Beugungsfigur genau auf den Einlassspalt des Detektors fällt, der mit Hilfe des Reiters zuvor in Mittelposition gefahren wurde, sodass die Beugungsfigur zu beiden Seiten symmetrisch vermessen werden kann.
    Für die Messung fährt man den Reiter auf Nullposition und und verschiebt ihn jeweils um $\SI{1}{\milli\meter}$ für den nächsten Stromwert.

    Bei der Bestimmung der Breite des Einzelspaltes mit einem Spiegel-Licht-Mikroskop muss man zunächst die im Mikroskop integrierte Messskala eichen.
    Dies geschieht mit einem Objektträger, der mit einer Messskala von $\SI{1}{\milli\meter}$ mit einer Auflösung von $\SI{10}{\micro\meter}$ bedruckt ist.
    Man bestimmt damit den Abstand zweier Linien im Mikroskop in $\SI{}{\milli\meter}$ und kann nach Auflegen des Spaltes auf den Objekttisch des Mikroskops die Spaltbreite bestimmen.
    \begin{figure}
      \includegraphics[width=\textwidth]{graphic/messaufbau.png}
      \caption{Versuchsaufbau zur Bestimmung der Spaltbreite \cite{anleitung406}}
      \label{fig:versuchsaufbau}
    \end{figure}%
  \section{Messwerte}
    In Tabelle \ref{tab:other-data} sind zunächst die Wellenlänge $\lambda$, der Abstand L, der Dunkelstrom $I_\t{Du}$, sowie die Herstellerdaten zur Breite des konstanten und variablen Einzelspaltes, sowie des Doppelspaltes aufgeführt.
    Außerdem ist dort die mit dem Mikroskop bestimmte Breite des Einzelspaltes, die Ungenauigkeit bei der Bestimmung und der relative Fehler im Verhältnis zu den Herstellerangaben eingetragen.

    In Tabelle \ref{tab:einfach-data}, \ref{tab:variabel-data} und \ref{tab:doppel-data} sind dann die ermittelten Messwerte für den konstanten und  variablen Einfachspalt, sowie dem Doppelspalt aufgeführt.
    In diesen Tabellen stehen dann jeweils der Beobachtungsort $\zeta$, der gemessene Strom des Detektors, als auch der durch Eichfehler des Ampèremeters gemachten Fehler bei der Bestimmung des Stroms.
    \begin{table}
      \input{table/other-data.tex}
      \caption{Daten zum Messaufbau und zu $b_\t{einfach, Mikroskop}$}
      \label{tab:other-data}
    \end{table}%
    \begin{table}
      \input{table/einfach-data.tex}
      \caption{Messdaten zum konstanten Einzelspalt}
      \label{tab:einfach-data}
    \end{table}%
    \begin{table}
      \input{table/variabel-data.tex}
      \caption{Messdaten zum variablen Einzelspalt}
      \label{tab:variabel-data}
    \end{table}%
    \begin{table}
      \input{table/doppel-data.tex}
      \caption{Messdaten zum Doppelspalt}
      \label{tab:doppel-data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%

    Zur Auswertung ist es hier und im Folgenden von Nöten mittels
    \begin{eqn}
      \tan(\varphi) \approx \varphi = \frac{\zeta - \zeta_0}{L}
    \end{eqn}%
    das Verhältnis des relativen Abstands vom 0. Maximum zum Abstand in den Beugungswinkel $\varphi$ zu überführen.
    Des Weiteren muss der Dunkelstrom von den abgelesenen Stromwerten subtrahiert werden.

    Bei der nicht-linearen Ausgleichrechnung mittels Formel \eqref{eqn:intensität_einfach} ergibt sich (vergleiche Tabelle \ref{tab:einfach-calc}) für die Breite des Spaltes ein Wert von $\input{result/b_einfach.tex}$.
    Der relative Fehler für die Breite beträgt $\input{result/b_einfach_rerror.tex}$.

    Beim Vergleich der Fitfunktion mit den Messwerten (Abbildung \ref{fig:graph-einfach}) fällt auf, dass die Messwerte insbesondere im Bereich des 0. Maximums gut passen.
    Im Bereich der Nebenmaxima gibt es geringe Abweichungen, die den Fehlern bei den Stromwerten sowie der niedrigen Messauflösung von 50 Messpunkten zuzuschreiben ist.

    Vergleicht man den relativen Fehler der Beugungsmessung mit dem Fehler bei der Bestimmung der Spaltbreite mit dem Mikroskop, dann ist der Fehler von $b_\t{einfach, Mikroskop}$ mit dem Betrag von $\SI{33.3}{\percent}$ verhältnismäßig groß.
    Diese antiquitierte Messmethode ist sehr ungenau im Verhältnis zur Bestimmung der Spaltbreite mittels Beugung.
    \begin{table}
      \input{table/einfach-calc.tex}
      \caption{Ort des 0.Maximums beim konstanten Einzelspalt, Ergebnisse der Ausgleichrechnung für $b$ und $A_0$}
      \label{tab:einfach-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/einfach.pdf}
      \caption{geplottete Messwerte des konstanten Einzelspalts und Fit für \mbox{Amplitude} und Spaltbreite}
      \label{fig:graph-einfach}
    \end{figure}%

    Für die Spaltbreite des variablen Einfachspaltes (Tabelle \ref{tab:variabel-calc}) ergibt sich ein Wert von $\input{result/b_variabel.tex}$
    Vergleicht man das mit den vom Hersteller angegeben Intervallgrenzen von $\input{result/b_variabel_Hersteller.tex}$ so fällt auf dass die ermittelte Spaltbreite unterhalb der Mitte des Intervalls, also bei $\input{result/b_variabel.tex}$ ist.
    Der variable Einzelspalt wurde nicht zwingend mittig eingestellt, sondern so, dass eine gut messbare Beugungsfigur entstand.
    Trotzdem liegt die Vermutung nahe, dass effektiv bei der Beugungsfigur mit dem Mittelwert der oberen und unteren Intervallgrenze des gerade noch mit Licht durchfluteten Spaltes gerechnet werden kann.

    Bei der Justierung der optischen Messvorrichtung unterlief der Fehler, dass das Hauptmaximum nicht exakt auf die Mitte der Messkala des Verschiebereiters ausgerichtet wurde, die Beugungsfigur konnte daher nicht vollständig und symmetrisch vermessen werden.
    Die Messwerte passen, wie aus Abbildung \ref{fig:graph-doppel} ersichtlich, lediglich im Bereich des Hauptmaximus gut an die gefittete Funktion.
    Im Bereich der Nebenmaxima passen die Werte nur noch näherungsweise, was zum einen auf die variable Spaltbreite zurückzuführen ist (die Theorie beschreibt konstante Spaltbreiten) und zum anderen am systematischen Messfehler liegen kann.
    Natürlich kommen die beim konstanten Einzelspalt beschriebenen Fehlerquellen dazu.
    Der relative Fehler bei der Spaltbreite beträgt $\input{result/b_variabel_rerror.tex}$.
    \begin{table}
      \input{table/variabel-calc.tex}
      \caption{Ort des 0. Maximums beim variablen Einzelspalt, Ergebnisse der Ausgleichrechung für $b$ und $A_0$}
      \label{tab:variabel-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/variabel.pdf}
      \caption{geplottete Messwerte des variablen Einzelspalts und Fit für \mbox{Amplitude} und Spaltbreite}
      \label{fig:graph-variabel}
    \end{figure}%
    \begin{table}
      \input{table/doppel-calc.tex}
      \caption{Ort des 0.Maximums beim Doppelspalt, Ergebnisse der Ausgleichrechnung für $b$ und $A_0$}
      \label{tab:doppel-calc}
    \end{table}%

    Vergleicht man die Messwerte des Doppelspaltversuchs mit der theoretischen Verteilung in Abbildung \ref{fig:graph-doppel} dann fällt auf, dass es einige Ausreißer gibt, diese können durch Ablesefehler entstanden sein.
    Die nicht-lineare Ausgleichsrechnung mit Hilfe der Funktion \eqref{eqn:intensität_doppel} ergibt für die Breite des Doppelspaltes $\input{result/b_doppel.tex}$, und für den Abstand der Spalte $\input{result/s_doppel.tex}$.
    Die relativen Fehler sind mit $\input{result/b_doppel_rerror.tex}$ für die Breite und $\input{result/s_doppel_rerror.tex}$ für die den Abstand der Spalte im Vergleich zu anderen Messwerten bei diesem Versuch etwas groß.
    Berücksichtigt man jedoch, dass die Theoriefunktion sehr stark und teils auch über große Niveaudifferenzen oszilliert, dann erscheinen die Messwerte verhältnismäßig gut.
    Der Unterschied zur Theoriefunktion des Einzelspaltes besteht darin, dass die Amplitude durch den $\cos^2$-Term stark oszilliert.
    \begin{figure}
      \includesgraphics{graph/doppel.pdf}
      \caption{Plot der Messwerte für den Doppelspalt und theoretische Kurve, sowie Vergleich mit Einfachspalt}
      \label{fig:graph-doppel}
    \end{figure}%
  \makebibliography
\end{document}
