#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

lambda_ = loadtxt('data/lambda') * 1e-9
L = loadtxt('data/L') * 1e-2

I_Du, I_Du_voll_rerror, I_Du_voll = loadtxt('data/I_Du', unpack=True)
I_Du *= 1e-9
I_Du_voll *= 1e-9
I_Du_error = rel2abs(I_Du_voll, I_Du_voll_rerror)
I_Du_rerror = abs2rel(I_Du, I_Du_error)

b_einfach = loadtxt('data/b_einfach') * 1e-3
b_Mikroskop, b_Mikroskop_error = loadtxt('data/b_Mikroskop', unpack=True) * 1e-6
b_Mikroskop_rerror = abs2rel(b_Mikroskop, b_Mikroskop_error)
b_variabel, b_variabel_min, b_variabel_max = loadtxt('data/b_variabel', unpack=True) * 1e-3
b_doppel = loadtxt('data/b_doppel') * 1e-3
s_doppel = loadtxt('data/s_doppel') * 1e-3

table = r'\sisetup{table-format=3.3,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\lambda$ & \nano\meter & ' + '{:.0f}'.format(lambda_ * 1e9) + r' \\' + '\n'
table += r'  $L$ & \centi\meter & ' + '{:.0f}'.format(L * 1e2) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $I_\t{Du}$ & \nano\ampere & \multicolumn{1}{S[round-mode=places]}{' + str(I_Du * 1e9) + r'} \\' + '\n'
table += r'  $\error{I_\t{Du}}$ & \nano\ampere & ' + str(I_Du_error * 1e9) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $b_\t{einfach, Hersteller}$ & \milli\meter & ' + str(b_einfach * 1e3) + r' \\' + '\n'
table += r'  $b_\t{einfach, Mikroskop}$ & \micro\meter & ' + '{:.0f}'.format(b_Mikroskop * 1e6) + r' \\' + '\n'
table += r'  $\error{b_\t{einfach, Mikroskop}}$ & \micro\meter & ' + '{:.0f}'.format(b_Mikroskop_error * 1e6) + r' \\' + '\n'
table += r'  $b_\t{variabel, Hersteller}$ & \milli\meter & \numrange{' + str(b_variabel_min * 1e3) + '}{' + str(b_variabel_max * 1e3) + r'} \\' + '\n'
table += r'  $b_\t{doppel, Hersteller}$ & \milli\meter & ' + str(b_doppel * 1e3) + r' \\' + '\n'
table += r'  $s_\t{doppel, Hersteller}$ & \milli\meter & ' + str(b_doppel * 1e3) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/other-data.tex', 'w').write(table)

open('build/result/b_variabel_Hersteller.tex', 'w').write(r'\SIrange{' + str(b_variabel_min * 1e3) + '}{' + str(b_variabel_max * 1e3) + r'}{\milli\meter}')

zeta, I_exp, I_voll_rerror, I_voll = loadtxt('data/I_einfach', unpack=True)
zeta *= 1e-3
I_exp *= 1e-9
I_voll *= 1e-9
I_exp_error = rel2abs(I_voll, I_voll_rerror)
I_exp_rerror = abs2rel(I_exp, I_exp_error)

I = I_exp - I_Du
I_error = sqrt(I_exp_error**2 + I_Du_error**2)
I_rerror = abs2rel(I, I_error)

zeta_0 = zeta[list(I).index(max(I))]
phi = (zeta - zeta_0) / L

def theory_einfach(phi, A_0, b):
  I = []
  for i in range(len(phi)):
    if phi[i] == 0:
      I.append(A_0**2 * b**2)
    else:
      I.append((A_0 * lambda_ / pi / sin(phi[i]) * sin(pi * b * sin(phi[i]) / lambda_))**2)
  return array(I)
popt, pcov = curve_fit(theory_einfach, phi, I, p0=[sqrt(max(I)) / b_einfach, b_einfach])
A_0 = popt[0]
A_0_error = sqrt(pcov[0][0])
A_0_rerror = abs2rel(A_0, A_0_error)
b = popt[1]
b_error = sqrt(pcov[1][1])
b_rerror = abs2rel(b, b_error)

savetxt('build/plot/einfach-points', (phi, I, I_error))
savetxt('build/plot/einfach-line',(A_0, b))

zeta_1 = zeta[:len(zeta) / 2 + 1]
zeta_2 = zeta[len(zeta) / 2 + 1:]
I_exp_1 =  I_exp[:len(I_exp) / 2 + 1]
I_exp_2 = I_exp[len(I_exp) / 2 + 1:]
I_exp_error_1 =  I_exp_error[:len(I_exp_error) / 2 + 1]
I_exp_error_2 = I_exp_error[len(I_exp_error) / 2 + 1:]

table = r'\sisetup{table-format=3.2}' + '\n'
table += r'\begin{tabular}{' + ' | ' .join(['S[table-format=2.0] S S[table-format=0.2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += '  ' + ' & '.join([r'\multicolumn{1}{c}{$\zeta / \si{\milli\meter}$} & \multicolumn{1}{c}{$I / \si{\nano\ampere}$} & \multicolumn{1}{c}{$\error{I} / \si{\nano\ampere}$}'] * 2) + r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(max_len([zeta_1, zeta_2])):
  if i < len(zeta_1): 
    table += '  ' + no_point(zeta_1[i] * 1e3) + ' & ' + no_point(I_exp_1[i] * 1e9) + ' & ' + no_point(I_exp_error_1[i] * 1e9)
  else:
    table += '  & & &'
  if i < len(zeta_2):
    table += ' & ' + no_point(zeta_2[i] * 1e3) + ' & ' + no_point(I_exp_2[i] * 1e9) + ' & ' + no_point(I_exp_error_2[i] * 1e9)
  else:
    table += ' & & &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/einfach-data.tex', 'w').write(table)

table = r'\sisetup{table-format=2.3,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\zeta_0$ & \milli\meter & \multicolumn{1}{S[round-mode=off]}{' + no_point(zeta_0 * 1e3) + r'} \\' + '\n'
table += r'  $A_0$ & \ampere\per\meter & \multicolumn{1}{S[round-precision=3]}{' + str(A_0) + r'} \\' + '\n'
table += r'  $\error{A_0}$ & \ampere\per\meter & ' + str(A_0_error) + r' \\' + '\n'
table += r'  $b$ & \milli\meter & \multicolumn{1}{S[round-precision=2]}{' + str(b * 1e3) + r'} \\' + '\n'
table += r'  $\error{b}$ & \milli\meter & ' + str(b_error * 1e3) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/einfach-calc.tex', 'w').write(table)

open('build/result/b_einfach.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(b * 1e3) + r'}{\milli\meter}')
open('build/result/b_einfach_rerror.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(b_rerror) + r'}{\percent}')

zeta, I_exp, I_voll_rerror, I_voll = loadtxt('data/I_variabel', unpack=True)
zeta *= 1e-3
I_exp *= 1e-9
I_voll *= 1e-9
I_exp_error = rel2abs(I_voll, I_voll_rerror)
I_exp_rerror = abs2rel(I_exp, I_exp_error)

I = I_exp - I_Du
I_error = sqrt(I_exp_error**2 + I_Du_error**2)
I_rerror = abs2rel(I, I_error)

zeta_0 = zeta[list(I).index(max(I))]
phi = (zeta - zeta_0) / L

def theory_variabel(phi, A_0, b):
  I = []
  for i in range(len(phi)):
    if phi[i] == 0:
      I.append(A_0**2 * b**2)
    else:
      I.append((A_0 * lambda_ / pi / sin(phi[i]) * sin(pi * b * sin(phi[i]) / lambda_))**2)
  return array(I)
popt, pcov = curve_fit(theory_variabel, phi, I, p0=[sqrt(max(I)) / b_variabel, b_variabel])
A_0 = popt[0]
A_0_error = sqrt(pcov[0][0])
A_0_rerror = abs2rel(A_0, A_0_error)
b = popt[1]
b_error = sqrt(pcov[1][1])
b_rerror = abs2rel(b, b_error)

savetxt('build/plot/variabel-points', (phi, I, I_error))
savetxt('build/plot/variabel-line',(A_0, b))

zeta_1 = zeta[:len(zeta) / 2 + 1]
zeta_2 = zeta[len(zeta) / 2 + 1:]
I_exp_1 =  I_exp[:len(I_exp) / 2 + 1]
I_exp_2 = I_exp[len(I_exp) / 2 + 1:]
I_exp_error_1 =  I_exp_error[:len(I_exp_error) / 2 + 1]
I_exp_error_2 = I_exp_error[len(I_exp_error) / 2 + 1:]

table = r'\sisetup{table-format=3.2}' + '\n'
table += r'\begin{tabular}{' + ' | ' .join(['S[table-format=2.0] S S[table-format=0.2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += '  ' + ' & '.join([r'\multicolumn{1}{c}{$\zeta / \si{\milli\meter}$} & \multicolumn{1}{c}{$I / \si{\nano\ampere}$} & \multicolumn{1}{c}{$\error{I} / \si{\nano\ampere}$}'] * 2) + r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(max_len([zeta_1, zeta_2])):
  if i < len(zeta_1): 
    table += '  ' + no_point(zeta_1[i] * 1e3) + ' & ' + no_point(I_exp_1[i] * 1e9) + ' & ' + no_point(I_exp_error_1[i] * 1e9)
  else:
    table += '  & & &'
  if i < len(zeta_2):
    table += ' & ' + no_point(zeta_2[i] * 1e3) + ' & ' + no_point(I_exp_2[i] * 1e9) + ' & ' + no_point(I_exp_error_2[i] * 1e9)
  else:
    table += ' & & &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/variabel-data.tex', 'w').write(table)

table = r'\sisetup{table-format=2.3,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\zeta_0$ & \milli\meter & \multicolumn{1}{S[round-mode=off]}{' + no_point(zeta_0 * 1e3) + r'} \\' + '\n'
table += r'  $A_0$ & \ampere\per\meter & \multicolumn{1}{S[round-precision=3]}{' + str(A_0) + r'} \\' + '\n'
table += r'  $\error{A_0}$ & \ampere\per\meter & ' + str(A_0_error) + r' \\' + '\n'
table += r'  $b$ & \milli\meter & \multicolumn{1}{S[round-precision=2]}{' + str(b * 1e3) + r'} \\' + '\n'
table += r'  $\error{b}$ & \milli\meter & \multicolumn{1}{S[round-mode=places,round-precision=3]}{' + '{:.6f}'.format(b_error * 1e3) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/variabel-calc.tex', 'w').write(table)

open('build/result/b_variabel.tex', 'w').write(r'\SI[round-mode=figures,round-precision=1]{' + str(b * 1e3) + r'}{\milli\meter}')
open('build/result/b_variabel_rerror.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(b_rerror) + r'}{\percent}')

zeta, I_exp, I_voll_rerror, I_voll = loadtxt('data/I_doppel', unpack=True)
zeta *= 1e-3
I_exp *= 1e-9
I_voll *= 1e-9
I_exp_error = rel2abs(I_voll, I_voll_rerror)
I_exp_rerror = abs2rel(I_exp, I_exp_error)

I = I_exp - I_Du
I_error = sqrt(I_exp_error**2 + I_Du_error**2)
I_rerror = abs2rel(I, I_error)

zeta_0 = zeta[list(I).index(max(I))]
phi = (zeta - zeta_0) / L

def theory_doppel(phi, A_0, b, s):
  I = []
  for i in range(len(phi)):
    if phi[i] == 0:
      I.append(A_0**2 * b**2)
    else:
      I.append((A_0 * cos(pi * s * sin(phi[i]) / lambda_) * lambda_ / pi / sin(phi[i]) * sin(pi * b * sin(phi[i]) / lambda_))**2)
  return array(I)
popt, pcov = curve_fit(theory_doppel, phi, I, p0=[sqrt(max(I)) / b_doppel, b_doppel, s_doppel])
A_0 = popt[0]
A_0_error = sqrt(pcov[0][0])
A_0_rerror = abs2rel(A_0, A_0_error)
b = popt[1]
b_error = sqrt(pcov[1][1])
b_rerror = abs2rel(b, b_error)
s = popt[2]
s_error = sqrt(pcov[2][2])
s_rerror = abs2rel(s, s_error)

savetxt('build/plot/doppel-points', (phi, I, I_error))
savetxt('build/plot/doppel-line',(A_0, b, s))

zeta_1 = zeta[:len(zeta) / 2 + 1]
zeta_2 = zeta[len(zeta) / 2 + 1:]
I_exp_1 =  I_exp[:len(I_exp) / 2 + 1]
I_exp_2 = I_exp[len(I_exp) / 2 + 1:]
I_exp_error_1 =  I_exp_error[:len(I_exp_error) / 2 + 1]
I_exp_error_2 = I_exp_error[len(I_exp_error) / 2 + 1:]

table = r'\sisetup{table-format=3.2}' + '\n'
table += r'\begin{tabular}{' + ' | ' .join(['S[table-format=2.0] S S[table-format=0.2]'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += '  ' + ' & '.join([r'\multicolumn{1}{c}{$\zeta / \si{\milli\meter}$} & \multicolumn{1}{c}{$I / \si{\nano\ampere}$} & \multicolumn{1}{c}{$\error{I} / \si{\nano\ampere}$}'] * 2) + r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(max_len([zeta_1, zeta_2])):
  if i < len(zeta_1): 
    table += '  ' + no_point(zeta_1[i] * 1e3) + ' & ' + no_point(I_exp_1[i] * 1e9) + ' & ' + no_point(I_exp_error_1[i] * 1e9)
  else:
    table += '  & & &'
  if i < len(zeta_2):
    table += ' & ' + no_point(zeta_2[i] * 1e3) + ' & ' + no_point(I_exp_2[i] * 1e9) + ' & ' + no_point(I_exp_error_2[i] * 1e9)
  else:
    table += ' & & &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/doppel-data.tex', 'w').write(table)

table = r'\sisetup{table-format=2.3,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\zeta_0$ & \milli\meter & \multicolumn{1}{S[round-mode=off]}{' + no_point(zeta_0 * 1e3) + r'} \\' + '\n'
table += r'  $A_0$ & \ampere\per\meter & \multicolumn{1}{S[round-precision=2]}{' + str(A_0) + r'} \\' + '\n'
table += r'  $\error{A_0}$ & \ampere\per\meter & ' + str(A_0_error) + r' \\' + '\n'
table += r'  $b$ & \milli\meter & \multicolumn{1}{S[round-precision=2]}{' + str(b * 1e3) + r'} \\' + '\n'
table += r'  $\error{b}$ & \milli\meter & ' + str(b_error * 1e3) + r' \\' + '\n'
table += r'  $s$ & \milli\meter & \multicolumn{1}{S[round-precision=3]}{' + str(s * 1e3) + r'} \\' + '\n'
table += r'  $\error{s}$ & \milli\meter & ' + str(s_error * 1e3) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/doppel-calc.tex', 'w').write(table)

open('build/result/b_doppel.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(b * 1e3) + r'}{\milli\meter}')
open('build/result/b_doppel_rerror.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(b_rerror) + r'}{\percent}')
open('build/result/s_doppel.tex', 'w').write(r'\SI[round-mode=figures,round-precision=3]{' + str(s * 1e3) + r'}{\milli\meter}')
open('build/result/s_doppel_rerror.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + str(s_rerror) + r'}{\percent}')
