SHELL=/bin/bash

this: dirs scripts pstricks
	export TEXINPUTS="./build:./:../:../texmf//:" ; lualatex --shell-escape --output-directory=build main.tex
	biber --logfile build/main.blg --outfile build/main.bbl build/main.bcf
	export TEXINPUTS="./build:./:../:../texmf//:" ; lualatex --shell-escape --output-directory=build main.tex
	export TEXINPUTS="./build:./:../:../texmf//:" ; lualatex --shell-escape --output-directory=build main.tex

fast: dirs
	export TEXINPUTS="./build:./:../:../texmf//:" ; lualatex --shell-escape --output-directory=build main.tex

dirs:
	mkdir -p build
	mkdir -p build/graph
	mkdir -p build/plot
	mkdir -p build/result
	mkdir -p build/table
	mkdir -p build/pstricks

scripts: dirs
	if [ -x main.py ] ; then \
		./main.py ; \
	fi
	if [ -x main.py2 ] ; then \
		./main.py2 ; \
	fi

pstricks: dirs
	shopt -s nullglob ; \
	for file in pstricks/*.tex ; do \
		FILE=`basename $$file` ; \
		FILE=$${FILE%.*} ; \
		export TEXINPUTS="./build:./:../:../texmf//:" ; latex --shell-escape --output-directory=build/pstricks $$file ; \
		if [ -f build/pstricks/$$FILE.dvi ] ; then \
			dvips -Ppdf -o build/pstricks/$$FILE.ps build/pstricks/$$FILE.dvi ; \
			ps2pdf -dAutoRotatePages=/None build/pstricks/$$FILE.ps build/pstricks/$$FILE.pdf ; \
			pdfcrop build/pstricks/$$FILE.pdf build/pstricks/$$FILE.pdf ; \
		fi ; \
	done

clean:
	rm -rf build __pycache__

all:
	for dir in ./[0-9]* ; do \
		make -C $$dir ; \
	done

clean-all:
	for dir in ./[0-9]* ; do \
		make -C $$dir clean ; \
	done
	rm -rf __pycache__
