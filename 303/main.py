#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

phi, U, U_noise = loadtxt('data/phi', unpack=True)
phi *= deg_rad
U *= 1e-3
U_noise *= 1e-3

def Uphi(phi, A, B):
  return nominal_values(abs(A) * cos(phi + B))

A, B = ucurve_fit(Uphi, phi, U)
A_noise, B_noise = ucurve_fit(Uphi, phi, U_noise)

savetxt('build/plot/phi-points', (phi, U))
savetxt('build/plot/phi-line', (A.nominal_value, B.nominal_value))
savetxt('build/plot/phi_noise-points', (phi, U_noise))
savetxt('build/plot/phi_noise-line', (A_noise.nominal_value, B_noise.nominal_value))

table = r'\sisetup{table-format=-1.3,round-mode=places,round-precision=3}' + '\n'
table += r'\begin{tabular}{S[table-format=3.0,round-mode=off] S S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$\SIlabel{\phi}{\degree}$} & \multicolumn{1}{c}{$\SIlabel{U_\t{out}}{\volt}$} & \multicolumn{1}{c}{$\SIlabel{U_\t{out,noise}}{\volt}$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(phi)):
  table += '  ' + '{:.0f}'.format(phi[i] / deg_rad) + ' & ' + str(U[i]) + ' & ' + str(U_noise[i]) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/phi-data.tex', 'w').write(table)

table = r'\sisetup{table-format=1.5,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \volt & \multicolumn{1}{S[round-precision=5]}{' + str(A.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \volt & ' + '{:f}'.format(A.std_dev()) + r' \\' + '\n'
table += r'  $B$ & \radian & \multicolumn{1}{S[round-precision=5]}{' + str(B.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B}$ & \radian & ' + '{:f}'.format(B.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $A_\t{noise}$ & \volt & \multicolumn{1}{S[round-precision=5]}{' + str(A_noise.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A_\t{noise}}$ & \volt & ' + '{:f}'.format(A_noise.std_dev()) + r' \\' + '\n'
table += r'  $B_\t{noise}$ & \radian & \multicolumn{1}{S[round-precision=4]}{' + str(B_noise.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B_\t{noise}}$ & \radian & ' + '{:f}'.format(B_noise.std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/phi-calc.tex', 'w').write(table)

r, U, PA, LI, LP = loadtxt('data/LED', unpack=True)
r *= 1e-2
U = abs(U)

def Ur(r, A, B, C):
  return nominal_values(A + B / (r + C)**2)

A, B, C = ucurve_fit(Ur, r, U / PA / LI / LP)

savetxt('build/plot/r-points', (r, U / PA / LI / LP))
savetxt('build/plot/r-line', (A.nominal_value, B.nominal_value, C.nominal_value))

table = r'\sisetup{}' + '\n'
table += r'\begin{tabular}{S[table-format=3.1] S[table-format=1.3,round-mode=places,round-precision=3] S[table-format=2.0] S[table-format=2.0] S[table-format=1.0]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$\SIlabel{r}{\centi\meter}$} & \multicolumn{1}{c}{$\SIlabel{U_\t{out}}{\volt}$} & \multicolumn{1}{c}{$G_\t{Preamp}$} & \multicolumn{1}{c}{$G_\t{Lock-In}$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(r)):
  table += '  ' + str(r[i] * 1e2) + ' & ' + str(U[i]) + ' & ' + str(int(PA[i])) + ' & ' + str(int(LI[i])) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/r-data.tex', 'w').write(table)

table = r'\sisetup{table-format=-1.6,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \milli\volt & \multicolumn{1}{S[round-precision=5]}{' + str(A.nominal_value * 1e3) + r'} \\' + '\n'
table += r'  $\error{A}$ & \milli\volt & ' + '{:f}'.format(A.std_dev() * 1e3) + r' \\' + '\n'
table += r'  $B$ & \volt\per\square\centi\meter & \multicolumn{1}{S[round-precision=7]}{' + str(B.nominal_value * 1e4) + r'} \\' + '\n'
table += r'  $\error{B}$ & \volt\per\square\centi\meter & ' + '{:f}'.format(B.std_dev() * 1e4) + r' \\' + '\n'
table += r'  $C$ & \centi\meter & \multicolumn{1}{S[round-precision=6]}{' + str(C.nominal_value * 1e2) + r'} \\' + '\n'
table += r'  $\error{C}$ & \centi\meter & ' + '{:f}'.format(C.std_dev() * 1e2) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/r-calc.tex', 'w').write(table)
