\input{header.tex}

\renewcommand{\ExperimentNumber}{303}

\begin{document}
  \maketitlepage{Der Lock-In-Verstärker}{10.01.2012}{27.01.2012}
  \section{Ziel}
    Ziel des Versuches ist es, sich mit der Funktionsweise eines Lock-In-Verstärkers vertraut zu machen.
  \section{Theorie}
    Beim Lock-In-Verstärker, der vornehmlich zum Messen stark verrauschter Signale eingesetzt wird, handelt es sich um einen speziellen Verstärker, der einen phasenempfindlichen Detektor enthält und deutlich höhere Güteziffern als ein Bandpass erreicht.
    Die Güte $Q$ eines Lock-In-Verstärkers ist definiert als
    \begin{eqn}
      Q = \frac{\omega}{\Delta \omega} ,
    \end{eqn}%
    wobei $\omega$ die gesuchte Frequenz und $\Delta\omega$ die Breite der Frequenzverteilungskurve, also die Anzahl anderer Frequenzen, ist.

    Bei einer schematischen Abstraktion reduziert sich der Aufbau des Lock-In-Verstärkers auf folgende Bauteile:
    Das Eingangssignal $U_\t{sig}$ wird mit einem phasenverschobenen Referenzsignal $U_\t{ref}$, hier zunächst ein Sinussignal, der Frequenz $\omega_0$ moduliert, nachdem es einen Bandpass durchlaufen hat, der alle Frequenzen außer denen, die in einer Umgebung von $\omega_0$ liegen, abschneidet.
    Die Modulation erfolgt im so genannten Mischer, in dem das Eingangs- und Referenzsignal der Frequenz $\omega$ miteinander multipliziert werden.
    \begin{eqns}
      U_\t{mix} &=& U_\t{sig} \cdot U_\t{ref} \\
                &=& U_{0,\t{sig}} \Sin{\omega t} \cdot U_{0,\t{ref}} \Sin{\omega t + \phi}
    \end{eqns}%
    Mit Hilfe des Additionstheorems
    \begin{eqn}
      \Sin{x \pm y} = \Sin{x} \Cos{y} \pm \Cos{x} \Sin{y}
    \end{eqn}%
    und der Vorraussetzung, dass alle Frequenzen gleich sind, weil nur eine Frequenz von $U_\t{sig}$ interessant ist, ergibt sich
    \begin{eqn}
      U_\t{mix} = U_{0,\t{sig}} U_{0,\t{ref}} \del{\Sin{\omega t}^2 + \Sin{\omega t} \Cos{\omega t}} \Cos{\phi} .
    \end{eqn}%
    Mit Hilfe des Additionstheorems
    \begin{eqn}
      \Sin{x}^2 = \frac{1}{2} \del{1 - \Cos{2x}}
    \end{eqn}%
    folgt
    \begin{eqn}
      U_\t{mix} = \frac{1}{2} U_{0,\t{sig}} U_{0,\t{ref}} \del{1 - \Cos{2\omega t} + 2 \Sin{\omega t} \Cos{\omega t}} \Cos{\phi} . \eqlabel{eqn:mischspannung_sin}
    \end{eqn}%
    In Gleichung \eqref{eqn:mischspannung_sin} können der erste Term als Gleichstromanteil und die restlichen Terme als Wechselstromanteil interpretiert werden.
    Abschließend wird das gemischte Signal durch einen Tiefpass mit der Zeitkonstante $\tau = RC \gg \frac{1}{\omega_0}$ geschickt, der das gemischte Signal über mehrere Perioden der Modulationsfrequenz integriert.
    \begin{eqns}
      U_\t{out} &=& \frac{1}{\tau} \Int{U_{\t{mix}}}{t,0,\tau} \\
                &=& \frac{1}{2} U_{0,\t{sig}} U_{0,\t{ref}} \Cos{\phi} \frac{1}{\tau} \Int{\del{1 - \Cos{2 \omega t} + 2 \Sin{\omega t} \Cos{\omega t}}}{t,0,\tau} \\
                &=& \frac{1}{2} U_{0,\t{sig}} U_{0,\t{ref}} \Cos{\phi} \eqlabel{eqn:U_out}
    \end{eqns}%
    Die nicht synchronisierten Beiträge werden überwiegend herausgemittelt, sodass am Ausgang eine Gleichspannung anliegt, die proportional zur Amplitude der Eingangsspannung ist.

    Bei der letzten Messung wird zur Anregung der LED eine Rechteckspannung als Referenzspannung verwendet.
    Die Fourier-Reihe der normierten Rechteckspannung ist
    \begin{eqns}
      U_\t{ref} &=& \frac{4}{\pi} \Sum{\frac{1}{2k-1} \Sin{\del{2k-1}x + \phi}}{k,1,\Infty} . \\
      \itext{Mit der Signalspannung $U_\t{sig} = U_0 \Sin{\omega t}$ multipliziert ergibt dies}
      U_\t{mix} &=& U_\t{ref} \cdot U_\t{sig} \\
      &=& U_0 \Abs{\Sin{\omega t}} \\
      &=& \frac{4}{\pi} U_0 \del{\frac{1}{2} - \Sum{\frac{1}{4k^2-1} \Cos{2k \omega t}}{k,1,\Infty}} .
    \end{eqns}%
    Die Konstante des bereits erwähnte Tiefpassfilter wird so gewählt, dass er die Oberwellen abschneidet und lediglich ein zur Eingangsspannung proportionaler Anteil
    \begin{eqns}
      U_\t{out} &=& \frac{2}{\pi} U_0 \\
      \itext{übrigbleibt. Sind $U_\t{sig}$ und $U_\t{ref}$ um $\phi$ phasenverschoben, so gilt}
      U_\t{out} &=& \frac{2}{\pi} U_0 \Cos{\phi} .\eqlabel{eqn:amp_phi}
    \end{eqns}%
    Verschwindet die Phasenverschiebung $\phi$, so wird $U_\t{out}$ maximal.
  \section{Aufbau und Durchführung}
    Der vorliegende Lock-In-Verstärker ist so aufgebaut, dass die einzelnen Komponenten seperat angesteuert und mit einem Speicheroszilloskop vermessen werden können.
    Zur Veranschaulichung ist Abbildung \ref{fig:versuchsaufbau} gegeben.

    Die Phasenlage des Referenzsignals relativ zum Eingangssignal lässt sich durch eine der Referenzsignalquelle nachgeschalteten Phasenverschiebung verändern, damit Phasendifferenz $\phi = 0$ beträgt.
    Des Weiteren kann das Signal mehrmals seperat verstärkt werden; Verstärker befinden sich im Preamplifier ($G_\t{Preamp}$), Lock-In-Detector ($G_\t{Lock-In}$) und Low-Pass-Filter ($G_\t{Low-Pass}$).
    \begin{figure}
      \includesgraphics{graphic/aufbau.png}
      \caption{Versuchsaufbau mit LED \cite{anleitung303}}
      \label{fig:versuchsaufbau}
    \end{figure}%

    Das Messprogramm sieht vor, dass zunächst das Signal des Funktionsgenerators(Ref./Osc.) untersucht und eroiert wird, welcher der beiden Ausgänge ein konstantes, respektive in der Amplitude veränderbares Signal ausgibt.
    Der Funktionsgenerator wird am variablen Ausgang mit einer ungefähren Frequenz von $\SI{1}{\kilo\hertz}$ und einer Amplitude von $\SI{10}{\milli\volt}$ betrieben.
    Bei der konstanten Spannung soll ein quantitativer Wert ermittelt werden.
    Von beiden Spannungen wird ein Bild des Oszilloskopen abgespeichert (Abbildungen \ref{fig:signal} und \ref{fig:referenz}).
    \begin{figure}
      \includegraphics{data/raw/ALL0000/F0000TEK.JPG}
      \caption{Ausgang des Signalgenerators}
      \label{fig:signal}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0001/F0001TEK.JPG}
      \caption{Referenzausgang des Generators}
      \label{fig:referenz}
    \end{figure}%

    Dann wird der Pre-Amplifier hinzugeschaltet und das Signal dort mit dem Oszillographen abgegriffen.
    Der Effekt des Verstärkers wird mit dem Oszillographen untersucht und das Bild im Oszillographen gespeichert (Abbildung \ref{fig:preamp}).
    \begin{figure}
      \includegraphics{data/raw/ALL0002/F0002TEK.JPG}
      \caption{Signal nach dem Pre-Amplifier}
      \label{fig:preamp}
    \end{figure}%

    Daraufhin wird das gemischte Signal hinter dem Lock-In-Detector für verschiedene Phasenunterschiede zwischen dem Referenzsignal und dem Eingangssignal vermessen und abgespeichert (Abbildungen \ref{fig:330} bis \ref{fig:30}).
    Weiterhin soll die Frequenz ermittelt werden.
    \begin{figure}
      \includegraphics{data/raw/ALL0003/F0003TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{330}{\degree}$}
      \label{fig:330}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0004/F0004TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{270}{\degree}$}
      \label{fig:270}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0006/F0006TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{210}{\degree}$}
      \label{fig:210}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0007/F0007TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{150}{\degree}$}
      \label{fig:150}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0009/F0009TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{90}{\degree}$}
      \label{fig:90}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0008/F0008TEK.JPG}
      \caption{Mixerausgang bei $\Delta \phi = \SI{30}{\degree}$}
      \label{fig:30}
    \end{figure}%

    Abschließend wird für die erste Messung der komplette Aufbau betrachtet und das Signal beim Low-Pass-Filter mit der Verstärkung 1 abgegriffen; der Preamplifier verstärkt mit dem Faktor 10, wobei der Gain der verwendeten Apparatur bei jeder Einstellung um \SI{10}{\percent} nach unten abweicht, und der Lock-In-Detector mit dem Faktor 2.
    Als Integrationskonstante wird $\tau = \SI{1}{\per\second}$ verwendet.
    Das Bild der Gleichspannung wird gespeichert (Abbildung \ref{fig:low-pass}).
    \begin{figure}
      \includegraphics{data/raw/ALL0011/F0011TEK.JPG}
      \caption{Signal nach dem Low-Pass}
      \label{fig:low-pass}
    \end{figure}%

    Bevor nun ein Noisegenerator zwischen dem Funktionengenerator und dem Preamplifier geschaltet wird, wird ein Bild des dort abgegriffenen verrauschten Signals abgespeichert (Abbildung \ref{fig:noise}).
    \begin{figure}
      \includegraphics{data/raw/ALL0012/F0012TEK.JPG}
      \caption{Signal mit Noise}
      \label{fig:noise}
    \end{figure}%

    Anschließend soll das Signal hinter dem Bandpass abgegriffen werden; es soll herausgefunden werden, wie sich die Güteeinstellung auf das Signal auswirkt.
    Hier wird ebenfalls ein Bild des Signals gespeichert (Abbildungen \ref{fig:bandpass-noise} und \ref{fig:mixer-noise}).
    Im Folgenden wird mit dem Gütefaktor 2 beim Bandpass-Filter gemessen.
    \begin{figure}
      \includegraphics{data/raw/ALL0013/F0013TEK.JPG}
      \caption{Signal mit Noise nach dem Bandpass}
      \label{fig:bandpass-noise}
    \end{figure}%
    \begin{figure}
      \includegraphics{data/raw/ALL0014/F0014TEK.JPG}
      \caption{Signal mit Noise nach dem Mixer}
      \label{fig:mixer-noise}
    \end{figure}%

    Die Phasenmessung hinter dem Lowpass-Filter wird mit hinzugeschalteten Noisegenerator wiederholt.

    Für die letzte Messung wird schließlich die Photodiodenschaltung entsprechend in Abbildung \ref{fig:versuchsaufbau} aufgebaut.
    Am Funktionengenerator wird nun eine Rechteckspannung eingestellt, deren Frequenz langsam gesteigert wird.
    Die Veränderung des Signals soll in Abhängigkeit zur Frequenz beschrieben werden.
    Mit Hilfe der Rechteckspannung (Abbildung \ref{fig:rechteck}) wird die LED mit einer Frequenz von \SI{336}{\hertz} zum Blinken gebracht.
    \begin{figure}
      \includegraphics{data/raw/ALL0015/F0015TEK.JPG}
      \caption{Rechtecksignal des Generators}
      \label{fig:rechteck}
    \end{figure}%
    Die Güte des Bandpasses wird auf 20 erhöht, die durchzulassende Frequenz am Bandpass muss der am Funktionengenerator angeglichen werden.
    Die Amplitudenspannung wird nun beim minimalen Abstand zwischen LED und Lichtdetektor durch Verstellen der Phase und Angleichen der Bandpassfrequez maximiert.
    Diese Einstellungen werden im Laufe der Messung aus Gründen der Vergleichbarkeit nicht mehr verändert.
    Die Lichtintensität soll in Abhängigkeit vom Abstand gemessen, sowie der Abstand ermittelt werden, bei dem das Licht der Diode gerade noch nachgewiesen werden kann.
    Da Licht sich, wenn man den hier nicht relevanten dualen Charakter ignoriert, als Kugelwelle ausbreitet, nimmt die Intensität mit dem Abstand $r$ auf Grund der Energieerhaltung mit $\frac{1}{r^2}$ ab.
    Deshalb wählt man für kleine $r$ eine größere Messdichte als für größere Abstände.
    Der Effekt der Intensitätsabnahme wird messtechnisch durch Erhöhen der einzelnen Verstärkungen im Lock-In-Vestärker ausgeglichen.
  \clearpage
  \section{Messwerte}
    Die gemessenen Werte stehen in den Tabellen \ref{tab:phi-data} und \ref{tab:r-data}.
    \begin{table}
      \input{table/phi-data.tex}
      \caption{Messwerte für den Zusammenhang zwischen Amplitude und Phasendifferenz, mit und ohne Noise}
      \label{tab:phi-data}
    \end{table}%
    \begin{table}
      \input{table/r-data.tex}
      \caption{Messwerte für die Abstandsabhängigkeit der Amplitude des Lichtsignals}
      \label{tab:r-data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%

    Die Amplitude des Signalausgangs kann variert werden, die Amplitude des Referenzausgangs bleibt konstant bei $\SI{3.20}{\volt}$.
    Der Effekt des Verstärkers liegt wie aus dem Vergleich von Abbildung \ref{fig:signal} und \ref{fig:preamp} ersichtlich wird, in der Verstärkung des Signals um den Faktor \num{9}.
    Die Frequenz, die sich hinter dem Lock-In-Detector einstellt, beträgt, wie aus den Abbildungen \ref{fig:330} bis \ref{fig:30} ersichtlich wird, $\omega \approx \SI{2}{\kilo\hertz}$, was die Summe aus der Referenzfrequenz, sowie der Signalfrequenz ist.
    Wird das Rauschen hinzugeschaltet, so fungiert der Bandpass als Vorentrauscher.
    Der Gütefaktor des Bandpasses ist proportional zur Redundanz des komplizierten Lock-In-Aufbaus.

    Die Messwerte aus Tabelle \ref{tab:phi-data} für die Amplitude ohne Noise sind in Abbildung \ref{fig:phi} aufgetragen, die für die Amplitude mit Noise in Abbildung \ref{fig:phi_noise}.
    Fittet man die Messwerte mit der Funktion
    \begin{eqn}
      U_\t{out} = A \Cos{\phi + B} ,
    \end{eqn}%
    so ergeben sich aus der Ausgleichsrechnung die Werte aus der Tabelle \ref{tab:phi-calc}.
    Die jeweiligen Fehler sind im Vergleich zu den Konstanten $A$ und $B$ klein.
    Die Plots der gefitteten Funktionen befinden sich in den Abbildungen \ref{fig:phi} und \ref{fig:phi_noise}.
    Aus ihnen wird ersichtlich, dass das Experiment die Theorie bestätigt.
    Vergleicht man Abbildung \ref{fig:mixer-noise} mit den unverrauschten Signalen nach dem Mixer, so fällt auf, dass der Bandpass auf Grund der Güteeinstellung das Signal noch nicht vollständig entrauscht; dies wird erst im Low-Pass-Filter finalisiert.
    Die Cosinus-Funktion aus der verrauschten Amplitude \ref{fig:phi_noise} ist im Vergleich zur nicht verrauschten Amplitude um $\pi$ verschoben.
    \begin{table}
      \input{table/phi-calc.tex}
      \caption{Ergebnisse der Ausgleichsrechnung für die Phasenabhängigkeit des gemischten Signals}
      \label{tab:phi-calc}
    \end{table}%
    \begin{table}
      \input{table/r-calc.tex}
      \caption{Ergebnisse der Ausgleichsrechnung für die Abstandsabhängigkeit des Lichtsignals}
      \label{tab:r-calc}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/phi.pdf}
      \caption{Plot der Messwerte und der Ausgleichskurve für die Phasenabhängigkeit des gemischten Signals, ohne Noise}
      \label{fig:phi}
    \end{figure}%
    \begin{figure}
      \includesgraphics{graph/phi_noise.pdf}
      \caption{Plot der Messwerte und der Ausgleichskurve für die Phasenabhängigkeit des gemischten Signals, mit Noise}
      \label{fig:phi_noise}
    \end{figure}%
    Die Messwerte für die Abstandsabhängigkeit der Amplitude sind in \ref{fig:r} aufgetragen.
    Die Ausgleichsrechnung mit Hilfe der Funktion
    \begin{eqn}
      \Fun{U_\t{out}}{r} = A + \frac{B}{\del{r + C}^2}
    \end{eqn}%
    ergibt für die Konstanten $A$, $B$ und $C$ und deren Fehler die Werte aus Tabelle \ref{tab:r-calc}.
    Die gefittete Funktion ist ebenfalls in Abbildung \ref{fig:r} aufgetragen.
    Die Fehler sind im Vergleich zu den Konstanten auch hier klein.
    Die experimentell ermittelten Werte passen gut zum theoretischen $r^{-2}$-Intensitätsabfall.
    Prinzipiell ließe sich das Lichtsignal noch weiter verstärken; ab $r \approx \SI{0.5}{\meter}$ wird das Signal sehr klein. 
    Die Schiene war zu kurz um den Grenzabstand $r_\t{max}$ zu ermitteln.
    \begin{figure}
      \includesgraphics{graph/r.pdf}
      \caption{Plot der Messwerte und der Ausgleichskurve für die Abstandsabhängigkeit des Lichtsignals}
      \label{fig:r}
    \end{figure}%
  \makebibliography
\end{document}
