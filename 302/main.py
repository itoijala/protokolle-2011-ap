#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

R_rerror = loadtxt('data/R_rerror')
R_pot_rerror = loadtxt('data/R_pot_rerror')
R_var_rerror = loadtxt('data/R_var_rerror')
C_rerror = loadtxt('data/C_rerror')
L_rerror = loadtxt('data/L_rerror')

table = r'\sisetup{}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \rerror{R} & \percent & ' + str(R_rerror) + r' \\' + '\n'
table += r'  \rerror{R_\t{pot}} & \percent & ' + str(R_pot_rerror) + r' \\' + '\n'
table += r'  \rerror{R_\t{var}} & \percent & ' + str(R_var_rerror) + r' \\' + '\n'
table += r'  \rerror{C} & \percent & ' + str(C_rerror) + r' \\' + '\n'
table += r'  \rerror{L} & \percent & ' + str(L_rerror) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/errors.tex', 'w').write(table)

R_value = [10, 12]
R_10_tmp = loadtxt('data/R_10', unpack=True)
R_12_tmp = loadtxt('data/R_12', unpack=True)
R_2 = [R_10_tmp[0], R_12_tmp[0]]
R_3 = [R_10_tmp[1], R_12_tmp[1]]

R_x = []
R_x_mean = []
R_x_std = []
R_x_error = []
R_x_rerror = []
for i in range(len(R_2)):
  R_x.append(R_2[i] * R_3[i] / (1000 - R_3[i]))
  R_x_mean.append(mean(R_x[i]))
  R_x_error.append(sqrt(error(R_x[i])**2 + rel2abs(R_x_mean[i], sqrt(R_rerror**2 + R_pot_rerror**2))**2))
  R_x_rerror.append(abs2rel(R_x_mean[i], R_x_error[i]))

table = r'\sisetup{table-format=4.0,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(('S S[table-format=3.0]' for i in R_x)) + '}' + '\n'
table += r'  \toprule' + '\n'
for i in range(len(R_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{2}{c'
  if i < len(R_x) - 1:
    table += '|'
  table += '}{Wert ' + str(R_value[i]) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(len(R_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$R_2 / \si{\ohm}$} & \multicolumn{1}{c'
  if i < len(R_x) - 1:
    table += '|'
  table += r'}{$R_3 / \si{\ohm}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(max_len(R_x)):
  for j in range(len(R_x)):
    if j > 0:
      table += ' & '
    table += str(R_2[j][i]) + ' & ' + str(R_3[j][i])
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/R-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.1,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s' + (' S' * len(R_x)) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{Wert ' + str(value) + '}' for value in R_value)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $R_\t{x}$ & \ohm'
for i in range(max_len(R_x)):
  if i > 0:
    table += '  &'
  for j in range(len(R_x)):
    table += ' & ' + str(R_x[j][i])
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{R_\t{x}}$ & \ohm & ' + ' & '.join((str(value) for value in R_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{R_\t{x}}}$ & \ohm & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value) + '}' for value in R_x_error)) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/R-calc.tex', 'w').write(table)

open('build/result/R-R_10.tex', 'w').write(r'\SI{' + '{:.0f}'.format(R_x_mean[0]) + r'}{\ohm}')
open('build/result/R-R_10_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(R_x_rerror[0]) + r'}{\percent}')
open('build/result/R-R_12.tex', 'w').write(r'\SI{' + '{:.0f}'.format(R_x_mean[1]) + r'}{\ohm}')
open('build/result/R-R_12_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(R_x_rerror[1]) + r'}{\percent}')

C_value = [1, 3, 9]
C_1_tmp = loadtxt('data/C_1', unpack=True)
C_3_tmp = loadtxt('data/C_3', unpack=True)
C_9_tmp = loadtxt('data/C_9', unpack=True)
C_2 = [C_1_tmp[0] * 1e-9, C_3_tmp[0] * 1e-9, C_9_tmp[0] * 1e-9]
R_3 = [C_1_tmp[1], C_3_tmp[1], C_9_tmp[1]]
R_2 = [C_9_tmp[2]]

C_x = []
C_x_mean = []
C_x_std = []
C_x_error = []
C_x_rerror = []
for i in range(len(C_2)):
  C_x.append(C_2[i] * (1000 - R_3[i]) / R_3[i])
  C_x_mean.append(mean(C_x[i]))
  C_x_error.append(sqrt(error(C_x[i])**2 + rel2abs(C_x_mean[i], sqrt(C_rerror**2 + R_pot_rerror**2))**2))
  C_x_rerror.append(abs2rel(C_x_mean[i], C_x_error[i]))
R_x = []
R_x_mean = []
R_x_std = []
R_x_error = []
R_x_rerror = []
for i in range(len(R_2)):
  R_x.append(R_2[i] * R_3[len(C_2) - len(R_2) + i] / (1000 - R_3[len(C_2) - len(R_2) + i]))
  R_x_mean.append(mean(R_x[i]))
  R_x_error.append(sqrt(error(R_x[i])**2 + rel2abs(R_x_mean[i], sqrt(R_rerror**2 + R_pot_rerror**2))**2))
  R_x_rerror.append(abs2rel(R_x_mean[i], R_x_error[i]))

C_wien = C_x_mean[0]

table = r'\sisetup{table-format=3.0,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{'
for i in range(len(C_x)):
  if i >= len(C_x) - len(R_x):
    n = 3
  else:
    n = 2
  if i > 0:
    table += ' | '
  table += ' '.join(['S'] * n)
table += '}\n'
table += r'  \toprule' + '\n'
for i in range(len(C_x)):
  if i >= len(C_x) - len(R_x):
    n = 3
  else:
    n = 2
  if i > 0:
    table += ' & '
  table += r'\multicolumn{' + str(n) + '}{c'
  if i < len(C_x) - 1:
    table += '|'
  table += '}{Wert ' + str(C_value[i]) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(len(C_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$C_2 / \si{\nano\farad}$}'
  if i >= len(C_x) - len(R_x):
    table += r' & \multicolumn{1}{c}{$R_2 / \si{\ohm}$}'
  table += r' & \multicolumn{1}{c'
  if i < len(C_x) - 1:
    table += '|'
  table += r'}{$R_3 / \si{\ohm}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(max_len(C_x)):
  for j in range(len(C_x)):
    if j > 0:
      table += ' & '
    table += str(C_2[j][i] * 1e9) + ' & '
    if j >= len(C_x) - len(R_x):
      table += str(R_2[j - len(C_x) + len(R_x)][i]) + ' & '
    table += str(R_3[j][i])
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/C-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.1,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s' + (' S' * len(C_x)) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{Wert ' + str(value) + '}' for value in C_value)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $C_\t{x}$ & \nano\farad'
for i in range(max_len(C_x)):
  if i > 0:
    table += '  &'
  for j in range(len(C_x)):
    table += ' & ' + str(C_x[j][i] * 1e9)
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'

table += r'  $R_\t{x}$ & \ohm'
for i in range(max_len(R_x)):
  if i > 0:
    table += '  &'
  table += ' & ' * (len(C_x) - len(R_x))
  for j in range(len(R_x)):
    table += ' & ' + str(R_x[j][i])
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{C_\t{x}}$ & \nano\farad & ' + ' & '.join((str(value * 1e9) for value in C_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{C_\t{x}}}$ & \nano\farad & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value * 1e9) + '}' for value in C_x_error)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{R_\t{x}}$ & \ohm & ' + (' & ' * (len(C_x) - len(R_x))) + ' & '.join((str(value) for value in R_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{R_\t{x}}}$ & \ohm & ' + (' & ' * (len(C_x) - len(R_x))) + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value) + '}' for value in R_x_error)) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/C-calc.tex', 'w').write(table)

open('build/result/C-C_1.tex', 'w').write(r'\SI{' + '{:.0f}'.format(C_x_mean[0] * 1e9) + r'}{\nano\farad}')
open('build/result/C-C_1_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(C_x_rerror[0]) + r'}{\percent}')
open('build/result/C-C_3.tex', 'w').write(r'\SI{' + '{:.0f}'.format(C_x_mean[1] * 1e9) + r'}{\nano\farad}')
open('build/result/C-C_3_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(C_x_rerror[1]) + r'}{\percent}')
open('build/result/C-C_9.tex', 'w').write(r'\SI{' + '{:.0f}'.format(C_x_mean[2] * 1e9) + r'}{\nano\farad}')
open('build/result/C-C_9_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(C_x_rerror[2]) + r'}{\percent}')
open('build/result/C-R_9.tex', 'w').write(r'\SI{' + '{:.0f}'.format(R_x_mean[0]) + r'}{\ohm}')
open('build/result/C-R_9_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(R_x_rerror[0]) + r'}{\percent}')

L_value = [18]
L_2, R_2, R_3 = loadtxt('data/L_18_ind', unpack=True)
L_2 = [L_2 * 1e-3]
R_2 = [R_2]
R_3 = [R_3]

L_x = []
L_x_mean = []
L_x_std = []
L_x_error = []
L_x_rerror = []
for i in range(len(L_2)):
  L_x.append(L_2[i] * R_3[i] / (1000 - R_3[i]))
  L_x_mean.append(mean(L_x[i]))
  L_x_error.append(sqrt(error(L_x[i])**2 + rel2abs(L_x_mean[i], sqrt(L_rerror**2 + R_pot_rerror**2))**2))
  L_x_rerror.append(abs2rel(L_x_mean[i], L_x_error[i]))
R_x = []
R_x_mean = []
R_x_std = []
R_x_error = []
R_x_rerror = []
for i in range(len(L_2)):
  R_x.append(R_2[i] * R_3[i] / (1000 - R_3[i]))
  R_x_mean.append(mean(R_x[i]))
  R_x_error.append(sqrt(error(R_x[i])**2 + rel2abs(R_x_mean[i], sqrt(R_rerror**2 + R_pot_rerror**2))**2))
  R_x_rerror.append(abs2rel(R_x_mean[i], R_x_error[i]))

table = r'\sisetup{table-format=3.0,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{'
for i in range(len(L_x)):
  if i < len(L_x) - 1:
    table += ' | '
  table += 'S[table-figures-integer=2,table-figures-decimal=1] S S'
table += '}\n'
table += r'  \toprule' + '\n'
for i in range(len(L_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{3}{c'
  if i < len(L_x) - 1:
    table += '|'
  table += '}{Wert ' + str(L_value[i]) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(len(L_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$L_2 / \si{\milli\henry}$} & \multicolumn{1}{c}{$R_2 / \si{\ohm}$} & \multicolumn{1}{c'
  if i < len(L_x) - 1:
    table += '|'
  table += r'}{$R_3 / \si{\ohm}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(max_len(L_x)):
  for j in range(len(L_x)):
    if j > 0:
      table += ' & '
    table += str(L_2[j][i] * 1e3) + ' & ' + str(R_2[j][i]) + ' & ' + str(R_3[j][i])
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L-ind-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.1,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s' + (' S' * len(L_x)) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{Wert ' + str(value) + '}' for value in L_value)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $L_\t{x}$ & \milli\henry'
for i in range(max_len(L_x)):
  if i > 0:
    table += '  &'
  for j in range(len(L_x)):
    table += ' & ' + str(L_x[j][i] * 1e3)
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'

table += r'  $R_\t{x}$ & \ohm'
for i in range(max_len(R_x)):
  if i > 0:
    table += '  &'
  for j in range(len(R_x)):
    table += ' & ' + str(R_x[j][i])
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{L_\t{x}}$ & \milli\henry & ' + ' & '.join((str(value * 1e3) for value in L_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{L_\t{x}}}$ & \milli\henry & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value * 1e3) + '}' for value in L_x_error)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{R_\t{x}}$ & \ohm & ' + ' & '.join((r'\multicolumn{1}{S[round-precision=2]}{' + str(value) + '}' for value in R_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{R_\t{x}}}$ & \ohm & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value) + '}' for value in R_x_error)) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L-ind-calc.tex', 'w').write(table)

open('build/result/L-ind-L_18.tex', 'w').write(r'\SI{' + '{:.1f}'.format(L_x_mean[0] * 1e3) + r'}{\milli\henry}')
open('build/result/L-ind-L_18_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(L_x_rerror[0]) + r'}{\percent}')
open('build/result/L-ind-R_18.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + '{:.0f}'.format(R_x_mean[0]) + r'}{\ohm}')
open('build/result/L-ind-R_18_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(R_x_rerror[0]) + r'}{\percent}')

L_value = [18]
C_4, R_2, R_3, R_4 = loadtxt('data/L_18_max', unpack=True)
C_4 = [C_4 * 1e-9]
R_2 = [R_2]
R_3 = [R_3]
R_4 = [R_4]

L_x = []
L_x_mean = []
L_x_std = []
L_x_error = []
L_x_rerror = []
for i in range(len(L_2)):
  L_x.append(R_2[i] * R_3[i] * C_4[i])
  L_x_mean.append(mean(L_x[i]))
  L_x_error.append(sqrt(error(L_x[i])**2 + rel2abs(L_x_mean[i], sqrt(R_rerror**2 + R_var_rerror**2 + C_rerror**2))**2))
  L_x_rerror.append(abs2rel(L_x_mean[i], L_x_error[i]))
R_x = []
R_x_mean = []
R_x_std = []
R_x_error = []
R_x_rerror = []
for i in range(len(L_2)):
  R_x.append(R_2[i] * R_3[i] / R_4[i])
  R_x_mean.append(mean(R_x[i]))
  R_x_error.append(sqrt(error(R_x[i])**2 + rel2abs(R_x_mean[i], sqrt(R_rerror**2 + 2 * R_var_rerror**2))**2))
  R_x_rerror.append(abs2rel(R_x_mean[i], R_x_error[i]))

table = r'\sisetup{table-format=3.0,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{'
for i in range(len(L_x)):
  if i < len(L_x) - 1:
    table += ' | '
  table += 'S S[table-format=4.0,round-precision=3] ' + ' '.join(['S'] * 2)
table += '}\n'
table += r'  \toprule' + '\n'
for i in range(len(L_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{4}{c'
  if i < len(L_x) - 1:
    table += '|'
  table += '}{Wert ' + str(L_value[i]) + '}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(len(L_x)):
  if i > 0:
    table += ' & '
  table += r'\multicolumn{1}{c}{$C_4 / \si{\nano\farad}$} & \multicolumn{1}{c}{$R_2 / \si{\ohm}$} & \multicolumn{1}{c}{$R_3 / \si{\ohm}$} & \multicolumn{1}{c'
  if i < len(L_x) - 1:
    table += '|'
  table += r'}{$R_4 / \si{\ohm}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'

for i in range(max_len(L_x)):
  for j in range(len(L_x)):
    if j > 0:
      table += ' & '
    table += str(C_4[j][i] * 1e9) + ' & ' + str(R_2[j][i]) + ' & ' + str(R_3[j][i]) + ' & ' + str(R_4[j][i])
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L-max-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.1,round-mode=figures,round-precision=3}' + '\n'
table += r'\begin{tabular}{c s' + (' S' * len(L_x)) + '}\n'
table += r'  \toprule' + '\n'
table += '  & & ' + ' & '.join((r'\multicolumn{1}{c}{Wert ' + str(value) + '}' for value in L_value)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $L_\t{x}$ & \milli\henry'
for i in range(max_len(L_x)):
  if i > 0:
    table += '  &'
  for j in range(len(L_x)):
    table += ' & ' + str(L_x[j][i] * 1e3)
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'

table += r'  $R_\t{x}$ & \ohm'
for i in range(max_len(R_x)):
  if i > 0:
    table += '  &'
  for j in range(len(R_x)):
    table += ' & \multicolumn{1}{S[round-precision=3]}{' + str(R_x[j][i]) + '}'
  table += r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{L_\t{x}}$ & \milli\henry & ' + ' & '.join((r'\multicolumn{1}{S[round-precision=2]}{' + str(value * 1e3) + '}' for value in L_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{L_\t{x}}}$ & \milli\henry & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value * 1e3) + '}' for value in L_x_error)) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\mean{R_\t{x}}$ & \ohm & ' + ' & '.join((r'\multicolumn{1}{S[round-precision=2]}{' + str(value) + '}' for value in R_x_mean)) + r' \\' + '\n'
table += r'  $\error{\mean{R_\t{x}}}$ & \ohm & ' + ' & '.join(('\multicolumn{1}{S[round-mode=figures,round-precision=1]}{' + str(value) + '}' for value in R_x_error)) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L-max-calc.tex', 'w').write(table)

open('build/result/L-max-L_18.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + '{:.3f}'.format(L_x_mean[0] * 1e3) + r'}{\milli\henry}')
open('build/result/L-max-L_18_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(L_x_rerror[0]) + r'}{\percent}')
open('build/result/L-max-R_18.tex', 'w').write(r'\SI[round-mode=figures,round-precision=2]{' + '{:.3f}'.format(R_x_mean[0]) + r'}{\ohm}')
open('build/result/L-max-R_18_rerror.tex', 'w').write(r'\SI{' + '{:.1f}'.format(R_x_rerror[0]) + r'}{\percent}')

U_S = loadtxt('data/U_S')
R_wien = loadtxt('data/R_wien')
omega_0 = 1 / (2 * pi * R_wien * C_wien)

omega, U_Br = loadtxt('data/U_Br', unpack=True)

savetxt('build/plot/graph-points', (omega / omega_0, U_Br / U_S))

omega_1 = omega[:len(omega) / 2]
U_Br_1 = U_Br[:len(U_Br) / 2]
omega_2 = omega[len(omega) / 2:]
U_Br_2 = U_Br[len(U_Br) / 2:]

def V(Omega):
  return sqrt(abs((Omega**2 - 1)**2 / ((1 - Omega**2)**2 + 9 * Omega**2) / 9))

k = U_Br[list(omega).index(240)] / V(2) / U_S

table = r'\sisetup{}' + '\n'
table += r'\begin{tabular}{' + (' | '.join(['S[table-format=5.0] S[table-format=1.4]'] * 2)) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$U_\t{S}$} & \multicolumn{1}{s}{\volt} & \multicolumn{2}{S[table-figures-integer=4,table-figures-decimal=0,round-mode=figures,round-precision=1]}{' + str(U_S) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$R$} & \multicolumn{1}{s}{\ohm} & \multicolumn{2}{S[table-figures-integer=4,table-figures-decimal=0]}{' + '{:.0f}'.format(float(R_wien)) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$C$} & \multicolumn{1}{s}{\nano\farad} & \multicolumn{2}{S[table-figures-integer=4,table-figures-decimal=0]}{' + '{:.0f}'.format(C_wien * 1e9) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += '  ' + ' & '.join([r'$\omega / \si{\hertz}$ & $U_\t{Br} / \si{\volt}$'] * 2) + r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(omega_1)):
  table += '  ' + '{:.0f}'.format(omega_1[i]) + ' & ' + str(U_Br_1[i])
  if i < len(omega_2):
    table += ' & ' + '{:.0f}'.format(omega_2[i]) + ' & ' + str(U_Br_2[i])
  else:
    table += ' & &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/U_Br-data.tex', 'w').write(table)

table = r'\sisetup{table-format=3.4}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $\omega_0$ & \hertz & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(omega_0) + r'} \\' + '\n'
table += r'  $U_\t{Br}(\omega_0) \approx U_\t{Br}(240)$ & \volt & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(U_Br[list(omega).index(240)]) + r'} \\' + '\n'
table += r'  $V(2)$ & & \multicolumn{1}{S[round-mode=figures,round-precision=3]}{' + str(V(2)) + r'} \\' + '\n'
table += r'  $k$ & & \multicolumn{1}{S[round-mode=figures,round-precision=2]}{' + str(k) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/U_Br-calc.tex', 'w').write(table)

open('build/result/U_S.tex', 'w').write(r'\SI{' + '{:.0}'.format(U_S) + r'}{\volt}')
open('build/result/U_Br_480.tex', 'w').write(r'\num{' + '{:.2f}'.format(U_Br[list(omega).index(480)]) + r'}')
open('build/result/V_2.tex', 'w').write(r'\num{' + '{:.2f}'.format(float(V(2))) + '}')
open('build/result/k.tex', 'w').write(r'\num{' + '{:.2}'.format(k) + '}')
