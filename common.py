from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

def error(array):
  return std(array) / sqrt(len(array))

def rerror(array):
  m = mean(array)
  if m == 0:
    return float('NaN')
  return error(array) / m * 100

def abs2rel(value, error):
  rerror = []
  a = True
  try:
    len(value)
  except TypeError:
    value = [value]
    error = [error]
    a = False
  for i in range(len(value)):
    if value[i] == 0:
      rerror.append(float('NaN'))
    else:
      rerror.append(error[i] / abs(value[i]) * 100)
  if not a:
    return rerror[0]
  return array(rerror)

def rel2abs(value, rerror):
  return value * rerror / 100

def max_len(array):
  l = len(array[0])
  for i in range(1, len(array)):
    if len(array[i]) > l:
      l = len(array[i])
  return l

def linregress_error(x, y):
  N = len(y)
  Delta = N * sum(x**2) - (sum(x))**2

  A = (N * sum(x * y) - sum(x) * sum(y)) / Delta
  B = (sum(x**2) * sum(y) - sum(x) * sum(x * y)) / Delta

  sigma_y = sqrt(sum((y - A * x - B)**2) / (N - 2))

  A_error = sigma_y * sqrt(N / Delta)
  B_error = sigma_y * sqrt(sum(x**2) / Delta)

  return (A, B, A_error, B_error)

def ulinregress(x, y):
  A, B, A_error, B_error = linregress_error(nominal_values(x), nominal_values(y))
  return (ufloat((A, A_error)), ufloat((B, B_error)))

def ucurve_fit(f, x, y, p0=None, sigma=None, **kw):
  popt, pcov = curve_fit(f, x, y, p0=p0, sigma=sigma, **kw)
  r = []
  for i in range(len(popt)):
    r.append(ufloat((popt[i], sqrt(pcov[i][i]))))
  return r

def no_point(f):
  s = str(f)
  if s.endswith('.0'):
    return s[:-2]
  return s

def vals(v):
  return nominal_values(v)

def stds(v):
  return std_devs(v)

deg_rad = pi / 180
m_u = 1.660538921e-27
kelvin_celsius = 273.15
bar_pascal = 1e5

c = 299792458
mu_0 = 4e-7 * pi
epsilon_0 = 1 / mu_0 / c**2
h = 6.62606957e-34
h_bar = h / 2 / pi
e_0 = 1.602176565e-19
m_e = 9.10938291e-31
k_B = 1.3806488e-23
N_A = 6.02214129e23

g = 9.807
