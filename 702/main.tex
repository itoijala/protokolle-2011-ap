\input{header.tex}

\addbibresource{lit.bib}

\renewcommand{\ExperimentNumber}{702}

\begin{document}
  \maketitlepage{Aktivierung mit Neutronen}{06.12.2011}{13.12.2011}
  \section{Ziel}
    Ziel des Versuchs ist die Bestimmung der Halbwertszeiten verschiedener Isotope.
  \section{Theorie}\label{theorie}
    Isotope werden instabil, wenn die Zahl der Neutronen $m - z$, wobei $m$ die Massenzahl ist, relativ zu der Zahl der Protonen $z$ zu groß oder zu klein wird.
    Die Wahrscheinlichkeit, dass dieser Kern in einen anderen zerfällt, variert zwischen verschiedenen Isotopen stark und kann durch die Halbwertszeit $T$ beschrieben werden.
    Die Wahrscheinlichkeit, dass ein Kern innerhalb der Halbwertszeit zerfällt, beträgt \SI{50}{\percent}.

    Eine Möglichkeit, instabile Kerne zu erzeugen, besteht darin, stabile Isotope mit Neutronen zu beschießen.
    Es entsteht ein Compoundkern, der nach einer kurzen Zeit von $\approx \SI{e-16}{\second}$ unter Aussendung der Energie des Neutrons in Form eines $\gamma$-Quants in seinen Grundzustand wechselt
    \begin{eqn}
      \ce{^{$m$}_{$z$} A + ^1_0 n -> ^{$m + 1$}_{$z$} A^* -> ^{$m + 1$}_{$z$} A + \gamma} .
    \end{eqn}%
    In vielen Fällen ist dieser Kern instabil und zerfällt nach einer längeren Zeit unter Aussendung eines Elektrons und eines Antineutrinos in einen stabilen Kern
    \begin{eqn}
      \ce{^{$m + 1$}_{$z$} A -> ^{$m + 1$}_{$z + 1$} B + \beta^- + \bar{\nu}_{\t{e}} + E_{\t{kin}}} .
    \end{eqn}%
    Die aus dem Massenverlust resultierende kinetische Energie wird zufällig zwischen dem Elektron und dem Antineutrino aufgeteilt.

    Für die hier untersuchten Isotope gelten
    \begin{eqns}[lCcClClCl]
      \ce{^{115}_{49} In} & + & \ce{n} & \ce{->} & \ce{^{116}_{49} In} & \ce{->} & \ce{^{116}_{50} Sn} & + & \ce{\beta^- + \bar{\nu}_{\t{e}}} \\
      \ce{^{107}_{47} Ag} & + & \ce{n} & \ce{->} & \ce{^{108}_{47} Ag} & \ce{->} & \ce{^{108}_{48} Cd} & + & \ce{\beta^- + \bar{\nu}_{\t{e}}} \\
      \ce{^{109}_{47} Ag} & + & \ce{n} & \ce{->} & \ce{^{110}_{47} Ag} & \ce{->} & \ce{^{110}_{48} Cd} & + & \ce{\beta^- + \bar{\nu}_{\t{e}}} .
    \end{eqns}%
    In der Natur vorkommendes Silber beinhaltet zu \SI{52}{\percent} \ce{^{107} Ag} und zu \SI{48}{\percent} \ce{^{109} Ag}, also laufen beide Zerfälle gleichzeitig und überlagert ab.

    Die Zerfälle radioaktiver Isotope folgen dem Gesetz
    \begin{eqn}
      N(t) = N_0 e^{- \lambda t} \eqlabel{eqn:zerfallskurve} .
    \end{eqn}%
    Hieraus folgt unmittelbar durch
    \begin{eqn}
      \frac{1}{2} N_0 = N_0 e^{- \lambda T} ,
    \end{eqn}%
    dass die Halbwertszeit $T$
    \begin{eqn}
      T = \frac{\ln 2}{\lambda}
    \end{eqn}%
    beträgt.
    Die experimentelle Bestimmung von $N(t)$ ist sehr schwierig, stattdessen kann nur die Anzahl der Zerfälle $N_{\Delta t}(t)$ in der Zeit $\Delta t$ gemessen werden.
    Es gilt
    \begin{eqn}
      N_{\Delta t}(t) = N(t) - N(t + \Delta t) .
    \end{eqn}%
    Es folgt
    \begin{eqns}
      N_{\Delta t}(t) & = & N_0 \left( 1 - u^{- \lambda \Delta t} \right) u^{- \lambda t} \\
      \ln N_{\Delta t}(t) & = & \ln \left( N_0 \left( 1 - u^{- \lambda \Delta t} \right) \right) - \lambda t . \eqlabel{eqn:zerfall_lin-regress}
    \end{eqns}%
    $\lambda$, und damit $T$, kann also durch eine lineare Regression bestimmt werden.

    Bei Silber gibt es die Komplikation, dass zwei radioaktive Isotope gleichzeitig vorhanden sind.
    Damit die Bestimmung der Halbwertszeiten möglich ist, muss eine der beiden Halbwertszeiten viel größer als die andere sein.
    Dann kann mittels der oben beschriebenen linearen Regression die Halbwertszeit des langlebigen Isotops bestimmt werden, in dem man nur die Werte ab $t^*$ einbezieht.
    Das kurzlebige Isotop sollte bei $t = t^*$ fast vollständig zerfallen sein.
    Dann kann man für $t < t^*$ die Ergebnisse der linearen Regression abziehen und erneut eine lineare Regression durchführen, um die kurze Halbwertszeit zu bestimmen.
  \section{Versuchsaufbau- und durchführung}
    Der Versuchsaufbau besteht aus einem Bleiquader mit einem zylindrischen Hohlraum, in den die Probe eingeführt werden kann.
    Anschließend wird der Hohlraum mit einem Geiger-Müller-Zählrohr ausgefüllt, an dem eine Zählelektronik angeschlossen ist, die die Impulse des Zählrohrs innerhalb einer einstallbaren Zeit zählt.
    
    Das Zweck der Bleiabschirmung besteht darin, dass der Nulleffekt, also die aus äußerlichen Quellen resultierende Strahlung, reduziert wird.
    Der Nulleffekt $N_{\Delta t_\t{u},\t{u}}$ muss vor der Messung bestimmt werden, damit durch Subtraktion die korrekte Zerfallsrate ohne Störeinflüsse ermittelt werden kann.
    Um den statistischen Fehler klein zu halten, wird die Nulleffektmessung mit großem $\Delta t_\t{u}$ durchgeführt.
  \section{Messwerte}
    Die gemessenen Werte stehen in den Tabellen \ref{tab:In-data} und \ref{tab:Ag-data}.
    \begin{table}
      \input{table/In-data.tex}
      \caption{Messwerte für Indium}
      \label{tab:In-data}
    \end{table}%
    \begin{table}
      \input{table/Ag-data.tex}
      \caption{Messwerte für Silber}
      \label{tab:Ag-data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    Da die Messwerte poissonverteilt sind, gilt
    \begin{eqn}
      \error{x} = \sqrt{x} .
    \end{eqn}%
    \input{linregress.tex}%

    In Abbildung \ref{fig:In} sind die Messwerte geplottet, sowie die Regressionsgerade aufgetragen, die sich aus den ausgerechneten Parametern aus Tabelle \ref{tab:In-calc} ergibt.
    $\lambda$ ergibt sich durch die Beziehung $\lambda = -A$ und steht ebenfalls in Tabelle \ref{tab:In-calc}.
    \begin{figure}
      \includesgraphics{graph/In.pdf}
      \caption{Messwerte und Regressionsgerade für Indium}
      \label{fig:In}
    \end{figure}%
    Vor der Auswertung werden die Messwerte um den Nulleffekt korrigiert.
    Der Fehler des Nulleffekts ist vernachlässigbar.
    In Abbildung \ref{fig:In} sind die Messwerte geplottet, sowie die Regressionsgerade aufgetragen, die sich auch den ausgerechneten Paramter aus Tabelle \ref{tab:In-calc} ergibt.
    $\lambda$ ergibt sich durch die Beziehung $\lambda = -A$ und steht ebenfalls in Tabelle \ref{tab:In-calc}.
    Mit Hilfe der Formel \eqref{eqn:zerfall_lin-regress} wird gefolgert, dass der t-unabhängige Teil konstant und gleich dem y-Achsenabschnitt $B$ der Regressionsgeraden ist.
    Dadurch errechnet sich der  in \ref{tab:In-calc} angegebenen Wert für $N_0$.
    
    Die Zerfallskurve erhält man, in dem man $\lambda$ und $N_0$ in \eqref{eqn:zerfallskurve} einsetzt.
    Für die Halbwertszeit ergibt sich
    \begin{eqn}
      T_\ce{^{116} In} = \SI{53 +- 2}{\minute} .
    \end{eqn}%
    
    Die statistischen Fehler für $T$ und $\lambda$ sind klein. 
    Beim Vergleichen mit dem Literaturwert für $T$ aus Tabelle \ref{tab:In-calc} fällt auf, dass der gemessene Wert dem sehr nahe kommt.
    Es handelt sich daher hierbei um ein gutes Messverfahren.
    \begin{table}
      \input{table/In-calc.tex}
      \caption{Ergebnisse der linearen Regression für Indium}
      \label{tab:In-calc}
    \end{table}%
   
   In Abbildung \ref{fig:Ag} sind die Messwerte für das langlebige Isotop, die Regressionsgerade, sowie die extrahierten Messwerte für das kurzlebige Isotop und dessen Regressionsgerade geplottet.
    Die für die lineare Regression verwendeten Messintervalle sind in Tabelle \ref{tab:Ag_lang-calc} angegeben.
    Die Werte für die Zerfallskonstante und die Halbwertszeit stehen ebenfalls in Tabelle \ref{tab:Ag_lang-calc} und werden analog zu Indium berechnet. 
    Beim kurzlebigen Silberisotop muss die in Kapitel \ref{theorie} beschriebene Subtraktion berücksichtigt werden.
    \begin{figure}
      \includesgraphics{graph/Ag.pdf}
      \caption{Messwerte und Regressionsgerade für Silber}
      \label{fig:Ag}
    \end{figure}%
    
    Beim langlebigen Silberisotop ergibt sich ein größerer Fehler, da der Zerfall des kurzlebigen Isotops noch eine Rolle spielt.
    Die Wahl von $t^*$ ist auf Grund der großen Schwankungen in den Messwerten schwierig.
    Damit ist die Abweichung vom Literaturwert zu erklären, bei dessen Ermittlung solche Fehler minimiert wurden.
    Das Messverfahren, das hier durchgeführt wurde, zieht auf Grund der zwei parallel verlaufenden Zerfallsvorgänge Fehler mit sich.
    Für das schneller zerfallende Silberisotop ist der Fehler geringer.
    \begin{table}
      \input{table/Ag-calc.tex}
      \caption{Ergebnisse der linearen Regression für Silber}
      \label{tab:Ag_lang-calc}
    \end{table}%
  \makebibliography
\end{document}
