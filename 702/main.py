#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

Delta_t_u = loadtxt('data/Delta_t_In_u')
N_u = loadtxt('data/N_In_u')
Delta_t = loadtxt('data/Delta_t_In')
N_g = loadtxt('data/N_In', unpack=True)
T_116m1_lit = loadtxt('data/T_In_116m1_lit') * 60

N_u = ufloat((N_u, sqrt(N_u)))
t = array([Delta_t * (x + 1) for x in range(len(N_g))])
N_g = array([ufloat((N_g[x], sqrt(N_g[x]))) for x in range(len(N_g))])

N = N_g - N_u * Delta_t / Delta_t_u

A, B = ulinregress(t, log(N))
lambda_ = - A
T = log(2) / lambda_
N_0 = B / (1 - exp(- lambda_ * Delta_t))

savetxt('build/plot/In-points', (t, nominal_values(N), std_devs(N)))
savetxt('build/plot/In-line', (A.nominal_value, B.nominal_value))

t_1 = t[0:len(t) / 2]
t_2 = t[len(t) / 2:]
N_g_1 = N_g[0:len(t) / 2]
N_g_2 = N_g[len(t) / 2:]

table = r'\sisetup{table-format=4.0}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S'] * 2) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{$\Delta t_\t{u}$} & \multicolumn{1}{s}{\second} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(Delta_t_u)) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$N_{\Delta t_\t{u},\t{u}}$} & \multicolumn{1}{s}{} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(N_u.nominal_value)) + r'} \\' + '\n'
table += r'  \multicolumn{1}{c}{$\Delta t$} & \multicolumn{1}{s}{\second} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(Delta_t)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += ' '
for i in range(2):
  if i > 0:
    table += ' &'
  table += r' \multicolumn{1}{c}{$t / \si{\second}$} & \multicolumn{1}{c'
  if i < 1:
    table += '|'
  table += r'}{$N_{\Delta t}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(t_1)):
  table += '  ' + str(int(t_1[i])) + ' & ' + str(int(N_g_1[i].nominal_value)) + ' &'
  if i < len(t_2):
    table += str(int(t_2[i])) + ' & ' + str(int(N_g_2[i].nominal_value))
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/In-data.tex', 'w').write(table)

table = r'\sisetup{table-format=+4.2e+1,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A$ & \per\second & \multicolumn{1}{S[round-precision=3]}{' + '{:.6e}'.format(A.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-4,round-precision=1]}{' + '{:.6e}'.format(A.std_dev()) + r'} \\' + '\n'
table += r'  $B$ & &\multicolumn{1}{S[round-precision=3]}{ ' + str(B.nominal_value) + r'}\\' + '\n'
table += r'  $\error{B}$ & &\multicolumn{1}{S}{' + str(B.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\lambda$ & \per\second & \multicolumn{1}{S[round-precision=3]}{' + '{:.6e}'.format(lambda_.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\lambda}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-4,round-precision=1]}{' + '{:.6e}'.format(lambda_.std_dev()) + r'} \\' '\n'
table += r'  $T_\ce{^{116} In}$ & \minute & \multicolumn{1}{S[round-precision=2]}{' + str(T.nominal_value / 60) + r'}\\' + '\n'
table += r'  $\error{T_\ce{^{116} In}}$ & \minute & \multicolumn{1}{S[round-precision=1]}{' + str(T.std_dev() / 60) + r'}\\' + '\n'
table += r'  $T_{\ce{^{116m1} In},\t{lit}}$ \cite{nuclide-table} & \minute & \multicolumn{1}{S[round-precision=3]}{' + str(T_116m1_lit / 60) + r'}\\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/In-calc.tex', 'w').write(table)

Delta_t_u = loadtxt('data/Delta_t_Ag_u')
N_u = loadtxt('data/N_Ag_u')
Delta_t = loadtxt('data/Delta_t_Ag')
N_g = loadtxt('data/N_Ag', unpack=True)
i_a = int(loadtxt('data/i_Ag_a'))
T_110_lit = loadtxt('data/T_Ag_110_lit')
T_108_lit = loadtxt('data/T_Ag_108_lit') * 60

N_u = ufloat((N_u, sqrt(N_u)))
t = array([Delta_t * (x + 1) for x in range(len(N_g))])
N_g = array([ufloat((N_g[x], sqrt(N_g[x]))) for x in range(len(N_g))])

N = N_g - N_u * Delta_t / Delta_t_u

t_t = []
N_t = []
for i in range(len(t)):
  if N[i] > 0:
    t_t.append(t[i])
    N_t.append(N[i])
t = array(t_t)
N = array(N_t)

A_l, B_l = ulinregress(t[i_a:], log(N[i_a:]))
lambda_l = - A_l
T_l = log(2) / lambda_l
N_0_l = B_l / (1 - exp(- lambda_l * Delta_t))

t_k = t[0:i_a - 8]
N_k = N[0:i_a - 8] - N_0_l * exp(- lambda_l * t[0:i_a - 8])

A_k, B_k = ulinregress(t_k, log(N_k))
lambda_k = - A_k
T_k = log(2) / lambda_k
N_0_k = B_k / (1 - exp(- lambda_k * Delta_t))

savetxt('build/plot/Ag-points', (t, nominal_values(N), std_devs(N)))
savetxt('build/plot/Ag-points_k', (t_k, nominal_values(N_k), std_devs(N_k)))
savetxt('build/plot/Ag-line', (A_l.nominal_value, B_l.nominal_value, A_k.nominal_value, B_k.nominal_value))

t_1 = t[0:len(t) / 3]
t_2 = t[len(t) / 3:len(t) * 2 / 3]
t_3 = t[len(t) * 2 / 3:]
N_g_1 = N_g[0:len(t) / 3]
N_g_2 = N_g[len(t) / 3:len(t) * 2 / 3]
N_g_3 = N_g[len(t) * 2 / 3:]

table = r'\sisetup{table-format=4.0}' + '\n'
table += r'\begin{tabular}{' + ' | '.join(['S S'] * 3) + '}\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{2}{c}{$\Delta t_\t{u}$} & \multicolumn{2}{s}{\second} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(Delta_t_u)) + r'} \\' + '\n'
table += r'  \multicolumn{2}{c}{$N_{\Delta t_\t{u},\t{u}}$} & \multicolumn{2}{s}{} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(N_u.nominal_value)) + r'} \\' + '\n'
table += r'  \multicolumn{2}{c}{$\Delta t$} & \multicolumn{2}{s}{\second} & \multicolumn{2}{S[table-format=3.0]}{' + str(int(Delta_t)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += ' '
for i in range(3):
  if i > 0:
    table += ' &'
  table += r' \multicolumn{1}{c}{$t / \si{\second}$} & \multicolumn{1}{c'
  if i < 1:
    table += '|'
  table += r'}{$N_{\Delta t}$}'
table += r' \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(t_1)):
  table += '  ' + str(int(t_1[i])) + ' & ' + str(int(N_g_1[i].nominal_value)) + ' &'
  if i < len(t_2):
    table += str(int(t_2[i])) + ' & ' + str(int(N_g_2[i].nominal_value)) + ' &'
  else:
    table += ' & &'
  if i < len(t_3):
    table += str(int(t_3[i])) + ' & ' + str(int(N_g_3[i].nominal_value))
  else:
    table += ' &'
  table += r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Ag-data.tex', 'w').write(table)

table = r'\sisetup{table-format=+3.1e+1,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $t$ & \second & \multicolumn{1}{S[round-mode=off]}{\numrange{' + str(int(t[i_a])) + '}{' + str(int(t[-1])) + r'}} \\' + '\n'
table += r'  $A$ & \per\second  & \multicolumn{1}{S[round-precision=1]}{' + '{:.6e}'.format(A_l.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-3,round-precision=1]}{' + '{:.6e}'.format(A_l.std_dev()) + r'} \\' + '\n'
table += r'  $B$ & &\multicolumn{1}{S[round-precision=2]}{ ' + str(B_l.nominal_value) + r'}\\' + '\n'
table += r'  $\error{B}$ & &\multicolumn{1}{S}{' + str(B_l.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\lambda$ & \per\second & \multicolumn{1}{S[round-precision=1]}{' + '{:.6e}'.format(lambda_l.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\lambda}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-3,round-precision=1]}{' + '{:.6e}'.format(lambda_l.std_dev()) + r'} \\' + '\n'
table += r'  $T_\ce{^{108} Ag}$ & \second & \multicolumn{1}{S[round-precision=1]}{' + str(T_l.nominal_value) + r'}\\' + '\n'
table += r'  $\error{T_\ce{^{108} Ag}}$ & \second & \multicolumn{1}{S[round-precision=1]}{' + str(T_l.std_dev()) + r'}\\' + '\n'
table += r'  $T_{\ce{^{108} Ag},\t{lit}}$ \cite{nuclide-table} & \second & \multicolumn{1}{S[round-precision=4]}{' + str(T_108_lit) + r'}\\' + '\n'
table += r'  \midrule' + '\n'
table += r'  \midrule' + '\n'
table += r'  $t$ & \second & \multicolumn{1}{S[round-mode=off]}{\numrange{' + str(int(t[0])) + '}{' + str(int(t[i_a - 8])) + r'}} \\' + '\n'
table += r'  $A$ & \per\second  & \multicolumn{1}{S[round-precision=2]}{' + '{:.6e}'.format(A_k.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-2,round-precision=1]}{' + '{:.6e}'.format(A_k.std_dev()) + r'} \\' + '\n'
table += r'  $B$ & &\multicolumn{1}{S[round-precision=2]}{ ' + str(B_k.nominal_value) + r'}\\' + '\n'
table += r'  $\error{B}$ & &\multicolumn{1}{S}{' + str(B_k.std_dev()) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $\lambda$ & \per\second & \multicolumn{1}{S[round-precision=2]}{' + '{:.6e}'.format(lambda_k.nominal_value) + r'} \\' + '\n'
table += r'  $\error{\lambda}$ & \per\second & \multicolumn{1}{S[scientific-notation=fixed,fixed-exponent=-2,round-precision=1]}{' + '{:.6e}'.format(lambda_k.std_dev()) + r'} \\' + '\n'
table += r'  $T_\ce{^{110} Ag}$ & \second & \multicolumn{1}{S[round-precision=1]}{' + str(T_k.nominal_value) + r'}\\' + '\n'
table += r'  $\error{T_\ce{^{110} Ag}}$ & \second & \multicolumn{1}{S[round-precision=1]}{' + str(T_k.std_dev()) + r'}\\' + '\n'
table += r'  $T_{\ce{^{110} Ag},\t{lit}}$ \cite{nuclide-table} & \second & \multicolumn{1}{S[round-precision=3]}{' + str(T_110_lit) + r'}\\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/Ag-calc.tex', 'w').write(table)
