#!/usr/bin/env python

import sys
sys.path.insert(1, '..')
from common import *

from pylab import *
from scipy.stats import linregress
from scipy.optimize import curve_fit
from uncertainties import *
from uncertainties.umath import *
from uncertainties.unumpy import *

R = N_A * k_B

#L = loadtxt('data/L') * 1e3
kappa = loadtxt('data/kappa')
M = loadtxt('data/M') * 1e-3
C_App = loadtxt('data/C_App')
V_H2O = loadtxt('data/V_H2O') * 1e-3
c_H2O = loadtxt('data/c_H2O') * 1e3
M_H2O = loadtxt('data/M_H2O')
C_H2O = V_H2O * 1e3 * c_H2O / M_H2O
C = C_H2O + C_App

t, P, T_k, T_w, p_k, p_w = loadtxt('data/t', skiprows=2, unpack=True)
t *= 60
T_k += kelvin_celsius
T_w += kelvin_celsius
p_k += 1
p_w += 1
p_k *= bar_pascal
p_w *= bar_pascal

A_L, B_L = ulinregress(1 / T_k, log(p_k))
savetxt('build/plot/L-points', (T_k, p_k))
savetxt('build/plot/L-line', (A_L.nominal_value, B_L.nominal_value))
L = -A_L * R

table = r'\sisetup{table-format=-5.1,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A_L$ & \second & \multicolumn{1}{S[round-precision=3]}{' + str(A_L.nominal_value) + r'} \\' + '\n'
table += r'  $\error{A_L}$ & \second & ' + str(A_L.std_dev()) + r' \\' + '\n'
table += r'  $B_L$ & & \multicolumn{1}{S[round-precision=3]}{' + str(B_L.nominal_value) + r'} \\' + '\n'
table += r'  $\error{B_L}$ & & ' + str(B_L.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $L$ & \joule\per\kilogram & \multicolumn{1}{S[round-precision=3]}{' + str(L.nominal_value) + r'} \\' + '\n'
table += r'  $\error{L}$ & \joule\per\kilogram & ' + str(L.std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/L.tex', 'w').write(table)

def quad(x, A, B, C):
  return A * x**2 + B * x + C

def dquad(x, A, B, C):
  return 2 * A * x + B

A_k, B_k, C_k = ucurve_fit(quad, t, T_k)
A_w, B_w, C_w = ucurve_fit(quad, t, T_w)
 
dT_k = abs(dquad(t, A_k, B_k, C_k))
dT_w = abs(dquad(t, A_w, B_w, C_w))

nu_real = C / P * dT_w
nu_ideal = T_w / (T_w - T_k)

dm = C / L * dT_k

P_m = R * C / M / L / (kappa - 1) * (p_w * pow(p_k / p_w, 1 / kappa) - p_k) * T_k / p_k * dT_k

savetxt('build/plot/T_k-points', (t / 60, T_k - kelvin_celsius))
savetxt('build/plot/T_k-line', (A_k.nominal_value * 60 * 60, B_k.nominal_value * 60, C_k.nominal_value - kelvin_celsius))
savetxt('build/plot/T_w-points', (t / 60, T_w - kelvin_celsius))
savetxt('build/plot/T_w-line', (A_w.nominal_value * 60 * 60, B_w.nominal_value * 60, C_w.nominal_value - kelvin_celsius))
savetxt('build/plot/p-points', (t / 60, p_k / bar_pascal, p_w / bar_pascal))
savetxt('build/plot/P-points', (t / 60, P))

table = r'\sisetup{table-format=3.2,round-mode=places,round-precision=2}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $R$ & \joule\per\mole\per\kelvin & ' + '{:.6f}'.format(R) + r' \\' + '\n'
table += r'  $C_\t{App}$ & \joule\per\kelvin & \multicolumn{1}{S[round-mode=off]}{' + str(int(C_App)) + r'} \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $M$ & \gram\per\mole & ' + str(M * 1e3) + r' \\' + '\n'
table += r'  $\kappa$ & & ' + str(kappa) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $M_\ce{H2O}$ & \gram\per\mole & ' + str(M_H2O) + r' \\' + '\n'
table += r'  $c_\ce{H2O}$ & \kilo\joule\per\mole & \multicolumn{1}{S[round-precision=1]}{' + str(c_H2O * 1e-3) + r'} \\' + '\n'
table += r'  $V_\ce{H2O}$ & \liter & \multicolumn{1}{S[round-mode=off]}{' + str(int(V_H2O * 1e3)) + r'} \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/other.tex', 'w').write(table)

table = r'\sisetup{table-format=2.1}' + '\n'
table += r'\begin{tabular}{S[table-format=2.0] S[table-format=3.0] S S S[table-format=1.2] S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{t}{\minute}} & \multicolumn{1}{c}{\SIlabel{P}{\watt}} & \multicolumn{1}{c}{\SIlabel{T_\t{k}}{\degreeCelsius}} & \multicolumn{1}{c}{\SIlabel{T_\t{w}}{\degreeCelsius}} & \multicolumn{1}{c}{\SIlabel{p_\t{k}}{\bar}} & \multicolumn{1}{c}{\SIlabel{p_\t{w}}{\bar}} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(t)):
  table += '  ' + str(int(t[i] / 60)) + ' & ' + str(int(P[i])) + ' & ' + str(T_k[i] - kelvin_celsius) + ' & ' + str(T_w[i] - kelvin_celsius) + ' & ' + str(p_k[i] / bar_pascal) + ' & ' + str(p_w[i] / bar_pascal) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/data.tex', 'w').write(table)

table = r'\sisetup{table-format=-3.4e-2,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{c s S}' + '\n'
table += r'  \toprule' + '\n'
table += r'  $A_\t{k}$ & \kelvin\per\square\minute & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A_k.nominal_value * 60 * 60) + r'} \\' + '\n'
table += r'  $\error{A_\t{k}}$ & \kelvin\per\square\minute & ' + str(A_k.std_dev() * 60 * 60) + r' \\' + '\n'
table += r'  $B_\t{k}$ & \kelvin\per\minute & \multicolumn{1}{S[round-precision=3]}{' + str(B_k.nominal_value * 60) + r'} \\' + '\n'
table += r'  $\error{B_\t{k}}$ & \kelvin\per\minute & ' + str(B_k.std_dev() * 60) + r' \\' + '\n'
table += r'  $C_\t{k}$ & \kelvin & \multicolumn{1}{S[round-precision=5]}{' + str(C_k.nominal_value) + r'} \\' + '\n'
table += r'  $\error{C_\t{k}}$ & \kelvin & ' + str(C_k.std_dev()) + r' \\' + '\n'
table += r'  \midrule' + '\n'
table += r'  $A_\t{w}$ & \kelvin\per\square\minute & \multicolumn{1}{S[round-precision=3]}{' + '{:.6f}'.format(A_w.nominal_value * 60 * 60) + r'} \\' + '\n'
table += r'  $\error{A_\t{w}}$ & \kelvin\per\square\minute & ' + str(A_w.std_dev() * 60 * 60) + r' \\' + '\n'
table += r'  $B_\t{w}$ & \kelvin\per\minute & \multicolumn{1}{S[round-precision=3]}{' + str(B_w.nominal_value * 60) + r'} \\' + '\n'
table += r'  $\error{B_\t{w}}$ & \kelvin\per\minute & ' + str(B_w.std_dev() * 60) + r' \\' + '\n'
table += r'  $C_\t{w}$ & \kelvin & \multicolumn{1}{S[round-precision=5]}{' + str(C_w.nominal_value) + r'} \\' + '\n'
table += r'  $\error{C_\t{w}}$ & \kelvin & ' + str(C_w.std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}' + '\n'
open('build/table/regress.tex', 'w').write(table)

rounds = [3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2]

table = r'\sisetup{per-mode=fraction,fraction-function=\tfrac,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{S[table-format=2.0,round-mode=off] S[table-format=1.3] S[table-format=1.3] S[table-format=1.3,round-mode=places,round-precision=2] S[table-format=1.2] S[table-format=1.4,round-mode=places,round-precision=3] S[table-format=1.3]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{t}{\minute}} & \multicolumn{1}{c}{\SIlabel{\Abs{\tod{T_\t{k}}{t}}}{\kelvin\per\minute}} & \multicolumn{1}{c}{\SIlabel{\error{\Abs{\tod{T_\t{k}}{t}}}}{\kelvin\per\minute}} & \multicolumn{1}{c}{\SIlabel{\Abs{\tod{T_\t{w}}{t}}}{\kelvin\per\minute}} & \multicolumn{1}{c}{\SIlabel{\error{\Abs{\tod{T_\t{w}}{t}}}}{\kelvin\per\minute}} & \multicolumn{1}{c}{$\nu_\t{real}$} & \multicolumn{1}{c}{$\error{\nu_\t{real}}$} \\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(t)):
  table += '  ' + str(int(t[i] / 60)) + r' & \multicolumn{1}{S[round-mode=places,round-precision=' + str(rounds[i]) + ']}{' + '{:.6f}'.format(dT_k[i].nominal_value * 60) + '} & ' + str(dT_k[i].std_dev() * 60) + ' & ' + '{:.6f}'.format(dT_w[i].nominal_value * 60) + ' & ' + str(dT_w[i].std_dev() * 60) + ' & ' + '{:.6f}'.format(nu_real[i].nominal_value) + ' & ' + str(nu_real[i].std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/calc.tex', 'w').write(table)

table = r'\sisetup{per-mode=fraction,fraction-function=\tfrac,round-mode=figures,round-precision=1}' + '\n'
table += r'\begin{tabular}{S[table-format=2.0,round-mode=off] S[table-format=3.2,round-precision=3] S[table-format=3.0] S[table-format=1.0] S[table-format=2.1] S[table-format=1.1]}' + '\n'
table += r'  \toprule' + '\n'
table += r'  \multicolumn{1}{c}{\SIlabel{t}{\minute}} & \multicolumn{1}{c}{$\nu_\t{ideal}$} & \multicolumn{1}{c}{\SIlabel{\Abs{\tod{m}{t}}}{\gram\per\minute}} & \multicolumn{1}{c}{\SIlabel{\error{\Abs{\tod{m}{t}}}}{\gram\per\minute}} & \multicolumn{1}{c}{\SIlabel{\Abs{P_\t{m}}}{\watt}} & \multicolumn{1}{c}{\SIlabel{\error{\Abs{P_\t{m}}}}{\watt}}\\' + '\n'
table += r'  \midrule' + '\n'
for i in range(len(t)):
  table += '  ' + str(int(t[i] / 60)) + ' & ' + '{:.6f}'.format(nu_ideal[i]) + ' & ' + r'\multicolumn{1}{S[round-mode=off]}{' + '{:.0f}'.format(dm[i].nominal_value * 60 * 1e3) + '} & ' + str(dm[i].std_dev() * 60 * 1e3) + ' & ' + r'\multicolumn{1}{S[round-mode=places,round-precision=1]}{' + '{:.6f}'.format(P_m[i].nominal_value) + '} & ' + str(P_m[i].std_dev()) + r' \\' + '\n'
table += r'  \bottomrule' + '\n'
table += r'\end{tabular}'
open('build/table/calc2.tex', 'w').write(table)
