\input{header.tex}

\addbibresource{lit.bib}

\renewcommand{\ExperimentNumber}{206}

\begin{document}
  \maketitlepage{Die Wärmepumpe}{20.12.2011}{27.01.2012}
  \section{Ziel}
    Ziel des Versuches ist es charakteristische Werte einer Wärmepumpe zu ermitteln.
  \section{Theorie}
    \subsection{Funktionsweise} \label{funktionsweise}
      Bei einer Wärmepumpe handelt es sich um einen Kreislauf, in dem ein Transportmedium in einem Rohrsystem durch zwei Reservoire unterschiedlicher Temperatur zirkuliert.
      Durch einen Kompressor und ein Drosselventil entstehen die Drücke $p_\t{k}$ im kalten und $p_\t{w}$ im warmen Reservoir.
      Der Kompressor verringert das Volumen des Gases adiabatisch und erhöht so den Druck und die Temperatur; das Drosselventil stellt einen Strömungswiderstand dar.
      Der Kompressor ist so abgestimmt, dass das Transportmedium einerseits im kälteren Reservoir verdampft und ihm dadurch Kondensationswärme $L$ pro Masseneinheit entzieht und andererseits im wärmeren Reservoir kondensiert und ihm Kondensationswärme abgibt.
      Um die Stabilität des Kreislaufs zu gewährleisten sind zusätzlich ein Reiniger, der die Flüssigkeit von Gasresten säubert und eine Steuerung des Drosselventils, die sich an der Temperaturdifferenz zwischen Eingang und Ausgang des zweiten Reservoirs orientiert, installiert.
      Wird nicht genug des Mediums verdampft, so verringert die Steuerung die Durchlassrate des Drosselventils.
    \subsection{Herleitung der charakteristischen Größen}
      Normalerweise betrachtet man in der Natur eine Wärmeverschiebung vom wärmeren ins kältere Reservoir.
      Der Wärmetransport von einem kälteren Reservoir in ein wärmeres ist nach dem ersten Hauptsatz der Thermodynamik nur unter Aufwendung von Arbeit möglich.
      Das Verhältnis $\nu$ zwischen der transportierten Wärme und der dazu aufgewendeten Arbeit $A$ bezeichnet man als Gütefaktor der Wärmepumpe
      \begin{eqns}
        \nu &=& \frac{Q_\t{w}}{A} , \eqlabel{eqn:gütefaktor} \\
        \itext{wobei}
        Q_\t{w} &=& Q_\t{k} + A .
      \end{eqns}%
      $Q_\t{w}$ ist die dem wärmeren Reservoir zugeführte, $Q_\t{k}$ die dem kälteren Reservoir entzogene Wärmemenge.

      Ändert sich die Temperatur während des Wärmetransports nicht, so folgt aus dem zweiten Hauptsatz der Thermodynamik, unter der Vorraussetzung, dass der Vorgang reversibel ist, also die in das Transportmedium hineingesteckte Energie zu jeder Zeit wieder daraus gewonnen werden kann,
      \begin{eqns}
        \frac{Q_\t{w}}{T_\t{w}} - \frac{Q_\t{k}}{T_\t{k}} &=& 0 , \eqlabel{eqn:verh-Q_T} \\
        \itext{wobei $T_\t{w}$ die Temperatur im warmen Reservoir und $T_\t{k}$ die im kalten Reservoir ist. Die Annahme ist realitätsfern, es gilt daher eigentlich}
        \frac{Q_\t{w}}{T_\t{w}} - \frac{Q_\t{k}}{T_\t{k}} &>& 0 . \\
        \itext{Aus \eqref{eqn:gütefaktor} und \eqref{eqn:verh-Q_T} folgt für die Güteziffer}
        \nu_\t{ideal} &=& \frac{T_\t{w}}{T_\t{w} - T_\t{k}} , \\
        \itext{also eigentlich}
        \nu_\t{real} &<& \frac{T_\t{w}}{T_\t{w} - T_\t{k}} .
      \end{eqns}%
      Die Effizienz einer Wärmepumpe steigt mit kleiner werdender Temperaturdifferenz der beiden Reservoire; die Effizienz übersteigt im Übrigen die anderer Wärmegewinnungsverfahren.

      Für die Ermittlung des Gütefaktors aus einer Messung erhält man mit dem Zusammenhang
      \begin{eqns}
        \od{Q_\t{w}}{t} &=& C \od{T_\t{w}}{t} , \eqlabel{eqn:wärmekapazität} \\
        \itext{wobei $C$ die Wärmekapazität der gesamten Apparatur ist, und mit Hilfe von \eqref{eqn:gütefaktor}}
        \nu &=& \frac{C}{P} \od{T_\t{w}}{t} . \eqlabel{eqn:gütefaktor_mess}
      \end{eqns}%
      Hierbei ist $P$ die Leistung des Kompressors.

      Eine weitere Kennziffer ist der Massendurchsatz der Wärmepumpe.
      Mit der zu \eqref{eqn:wärmekapazität} analogen Gleichung
      \begin{eqns}
        \od{Q_\t{k}}{t} &=& C \od{T_\t{k}}{t} \\
        \itext{und}
        L \od{m}{t} &=& \od{Q_\t{k}}{t} \\
        \itext{ergibt sich der Massendurchsatz}
        \od{m}{t} &=& \frac{C}{L} \od{T_\t{k}}{t} \eqlabel{eqn:massendurchsatz} .
      \end{eqns}%

      Schließlich soll noch die mechanische Kompressorleistung $P_\t{m}$ bestimmt werden.
      Mit der mechanischen Arbeit, die ein Kompressor bei einer Volumenveränderung verrichtet
      \begin{eqns}
        A_\t{m} &=& - \int_{V_\t{k}}^{V_\t{w}} p \dif{V} \\
        \itext{und der Poisson'schen Gleichung}
        p V^\kappa &=& \t{const} , \\
        \itext{wobei $\kappa$ der Adiabatenexponent ist, ergibt sich insgesamt}
        A_\t{m} &=& - p_\t{k}^{} V_\t{k}^\kappa \int_{V_\t{k}}^{V_\t{w}} V^{-\kappa} \dif{V} \\
        &=& \frac{1}{\kappa - 1} \del{p_\t{w} \sqrt[\kappa]{\frac{p_\t{k}}{p_\t{w}}} - p_\t{k}} V_\t{k} .
      \end{eqns}%
      Daraus folgt für die Kompressorleistung
      \begin{eqns}
        \od{A_\t{m}}{t} &=& \frac{1}{\kappa - 1} \del{p_\t{w} \sqrt[\kappa]{\frac{p_\t{k}}{p_\t{w}}} - p_\t{k}} \od{V_\t{k}}{t} \\
        &=& \frac{1}{\kappa - 1} \del{p_\t{w} \sqrt[\kappa]{\frac{p_\t{k}}{p_\t{w}}} - p_\t{k}} \frac{1}{\rho} \od{m}{t} . \\
        \itext{Mit Hilfe der idealen Gasgleichung wird $\rho$ durch}
        \rho &=& \frac{M p}{R T} \\
        \itext{ersetzt, wobei $M$ die molare Masse des Transportmediums und $R$ die universelle Gaskonstante ist, folgt}
        P_\t{m} = \od{A_\t{m}}{t} &=& \frac{1}{\kappa - 1} \del{p_\t{w} \sqrt[\kappa]{\frac{p_\t{k}}{p_\t{w}}} - p_\t{k}} \frac{R T_\t{k}}{M p_\t{k}} \frac{C}{L} \od{T_\t{k}}{t} . \eqlabel{eqn:kompressorleistung}
      \end{eqns}%
  \section{Versuchsaufbau und -durchführung}
    Der Versuchsaufbau entspricht im Wesentlichen dem beschriebenen Prinzip einer Wärmepumpe aus \ref{funktionsweise} und ist in Abbildung \ref{fig:aufbau} veranschaulicht.
    Die Reservoire bestehen aus wärmeisolierten Eimern, die vor Beginn mit Wasser gefüllt werden.
    Um sich einer homogenen Wärmeverteilung im Reservoir anzunähern, sind im Wasser Rührpropeller angebracht, die durch einen Elektromotor angetrieben werden.
    Die gleichmäßige Wärmeverteilung ist insbesondere relevant bei der Ermittlung der Temperatur, da die Temperatur im gesamten Reservoir und nicht nur um den Sensor gemessen werden soll.
    Am Kompressor sind noch ein Wattmeter und eine elektrische Uhr angebracht.
    \begin{figure}
      \includegraphics[scale=0.6]{graphic/aufbau.png}
      \caption{Der Versuchsaufbau \cite{anleitung206}}
      \label{fig:aufbau}
    \end{figure}%
    Als Transportmedium wird hier Dichlordifluormethan (\ce{Cl2F2C}) verwendet, das sich durch eine hohe Kondensationswärme auszeichnet, was der Effizienz einer Wärmepumpe dienlich ist.
    Für die Messung nimmt man den Kompressor in Betrieb und aktiviert so den Kreislauf.
    Es werden in Abständen von $\Delta t = \SI{1}{\minute}$ die Drücke $p_\t{k}$ und $p_\t{w}$, die Temperaturen $T_\t{k}$ und $T_\t{w}$, sowie die Leistungsaufnahme des Kompressors $P$ aufgenommen.
  \section{Messwerte}
    Verwendete Konstanten und einzelne Messwerte stehen in Tabelle \ref{tab:other}.
    Die zeitabhängigen Messwerte stehen in Tabelle \ref{tab:data}.
    \begin{table}
      \input{table/other.tex}
      \caption{Konstanten und einzelne Messwerte}
      \label{tab:other}
    \end{table}%
    \begin{table}
      \input{table/data.tex}
      \caption{Messwerte}
      \label{tab:data}
    \end{table}%
  \section{Auswertung}
    \input{statistics.tex}%
    \input{linregress.tex}%

    Die Verdampfungswärme~$L$ kann durch eine lineare Regression bestimmt werden.
    Es gilt
    \begin{eqns}
      \Fun{p}{T} &=& p_0 \Exp{-\frac{L}{R} \frac{1}{T}} , \\
      \itext{also}
      \Log{p} &=& -A_L \frac{1}{T} + B_L .
    \end{eqns}%
    Dann gilt für $L$
    \begin{eqn}
      L = -A_L R .
    \end{eqn}%
    Die Ergebnisse der linearen Regression stehen in Tabelle \ref{tab:L} und die Messwerte und die Ausgleichsgerade sind in Abbildung \ref{fig:L} geplottet.
    \begin{table}
      \input{table/L.tex}
      \caption{Ergebnisse der Ausgleichsrechnung und berechneter Wert für $L$}
      \label{tab:L}
    \end{table}%
    \begin{figure}
      \includesgraphics{graph/L.pdf}
      \caption{Plot der Messwerte und der Ausgleichskurve zur Bestimmung von~$L$}
      \label{fig:L}
    \end{figure}%

    In Tabelle \ref{tab:regress} sind die Parameter der Ausgleichsrechnung für die Temperaturkurve, sowie deren Fehler aufgeführt. Als Fitfunktion wurde
    \begin{eqn}
      T(t) = At^2 + Bt + C \eqlabel{eqn:fitfunktion}
    \end{eqn}%
    verwendet.

    Die Messwerte sowie die gefittete Funktion sind in Abbildung \ref{fig:T_k} und \ref{fig:T_w} geplottet.
    Es fällt auf, dass die Funktion \eqref{eqn:fitfunktion} die Messwerte gut approximiert.
    \begin{table}
      \input{table/regress.tex}
      \caption{Ergebnisse der Regression}
      \label{tab:regress}
    \end{table}%
    Die berechneten Werte für die zeitlichen Ableitungen der Temperaturen, die Güteziffern, den Massendurchsatz, sowie die mechanische Kompressorleistung stehen in Tabelle \ref{tab:calc}.

    Die realen Güteziffern sind deutlich niedriger als die theoretisch erreichbaren; es scheint sich um eine schlechte Wärmepumpe zu handeln, da sich die reale und die ideale Güteziffer um zwei Größenordnungen unterscheiden.
    Der Massendurchsatz sowie die Güteziffer sinken mit steigender Temperaturdifferenz der Reservoire, es wird also weniger Wärme transportiert, das heißt die Wärmepumpe funktioniert schlechter.
    Durch die schlechte Isolierung der Wärmepumpe kommt es zu Wärmeverlusten innerhalb des Transportvorgangs, sowie in den einzelnen Reservoiren.
    Trotz der Rührmotoren konnte man keine stets homogene Wärmeverteilung in den Reservoiren realisieren.
    Durch die hohe Anzahl der abzulesenden Messwerten konnte beim Ablesen keine Gleichzeitigkeit garantiert werden.
    \begin{table}
      \OverfullCenter{\input{table/calc.tex}}
      \caption{Berechnete Ergebnisse}
      \label{tab:calc}
    \end{table}%
    \begin{table}
      \input{table/calc2.tex}
      \caption{Berechnete Ergebnisse}
      \label{tab:calc2}
    \end{table}%

    \begin{figure}
      \includesgraphics{graph/T_k.pdf}
      \caption{Temperatur des kalten Reservoirs $T_\t{k}$ gegen die Zeit $t$ aufgetragen}
      \label{fig:T_k}
    \end{figure}%
    \begin{figure}
      \includesgraphics{graph/T_w.pdf}
      \caption{Temperatur des warmen Reservoirs $T_\t{w}$ gegen die Zeit $t$ aufgetragen}
      \label{fig:T_w}
    \end{figure}%
    In Abbildung \ref{fig:p} sind die zeitlichen Druckverläufe aufgetragen.
    \begin{figure}
      \includesgraphics{graph/p.pdf}
      \caption{Die Drücke $p_\t{k}$ und $p_\t{w}$ gegen die Zeit $t$ aufgetragen}
      \label{fig:p}
    \end{figure}%
    In Abbildung \ref{fig:P} ist der zeitliche Verlauf der Kompressorleistung gegeben.
    \begin{figure}
      \includesgraphics{graph/P.pdf}
    \caption{Die Kompressorleistung $P$ gegen die zeit $t$ aufgetragen}
      \label{fig:P}
    \end{figure}%
    
  \makebibliography
\end{document}
